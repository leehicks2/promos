using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers{
    
    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class TemplateController : ControllerBase
    {
        private readonly ILogger<TemplateController> _logger;
        private readonly ITemplate _templateProvider;

         public TemplateController(ILogger<TemplateController> logger,  ITemplate templateProvider)
        {
            _logger = logger;
            _templateProvider = templateProvider;
        }

        [HttpPost]
        public async Task<Template> Post()
        {
            Template response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var requestTemplate = JsonConvert.DeserializeObject<Template>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _templateProvider.Insert(requestTemplate);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

        [HttpPost("update")]
        public async Task<Template> Put()
        {
            Template response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var requestTemplate = JsonConvert.DeserializeObject<Template>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _templateProvider.Update(requestTemplate);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

        [HttpGet("{id}")]
        public Template GetById(int id)
        {
            return _templateProvider.GetTemplateById(id);
        }

         [HttpGet("bandid/{bandid}/sizeid/{sizeid}")]
        public List<Template> GetByBandAndSizeId(string bandid, int sizeid)
        {
            return _templateProvider.GetTemplateByBandAndSize(bandid, sizeid);
        }

        [HttpGet]
        public List<Template> Get()
        {
            return _templateProvider.GetAllTemplates();
        }

        [HttpGet("band/{id}")]
        public List<Template> GetByBandId(int id)
        {
            return _templateProvider.GetTemplatesByBand(id);
        }

         [HttpGet("packs/")]
        public List<TemplatePack> GetTenplatePacks()
        {
            return _templateProvider.GetTemplatePacks();
        }

        [HttpPost("packs/")]
        public async Task<TemplatePack> InsertTemplatePack()
        {
            TemplatePack response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var requestTemplate = JsonConvert.DeserializeObject<TemplatePack>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _templateProvider.InsertTemplatePack(requestTemplate);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

        [HttpGet("store/{storeid}/band/{bandid}/templateId/{templateid}")]
        public List<TemplateDisplay> GetTenplateDisplay(int storeid, int bandid, int templateid)
        {
            return _templateProvider.GettemplateDisplay(storeid, bandid, templateid);
        }

         [HttpGet("display/buyeroffer/{buyerofferId}/template/{templateid}")]
        public List<TemplateDisplay> GetTenplateDisplay(int buyerofferId, int templateId)
        {
            return _templateProvider.GettemplateDisplayByBuyerAndTemplate(buyerofferId, templateId);
        }
    }
}