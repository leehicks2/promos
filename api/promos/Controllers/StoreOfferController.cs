using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class StoreOfferController : ControllerBase
    {
        private readonly IStoreOffer _storeOfferProvider;

        public StoreOfferController(IStoreOffer storeOfferprovider){
            _storeOfferProvider = storeOfferprovider;
        }

        [HttpGet]
        public List<StoreOffer> Get()
        {
            return _storeOfferProvider.GetAll();
        }

        [HttpGet("bandid/{bandId}")]
        public List<StoreOffer> GetByBandId(int bandId)
        {
            return _storeOfferProvider.GetStoreOffersByBandId(bandId);
        }

        [HttpGet("buyerofferid/delete/{buyerOfferId}")]
        public int DeleteStoreOffer(int buyerOfferId)
        {
            return _storeOfferProvider.DeleteStoreOffer(buyerOfferId);
        }

         [HttpGet("delete/offerid/{id}")]
        public int DeleteStoreOfferById(int id)
        {
            return _storeOfferProvider.DeleteStoreOfferById(id);
        }


        [HttpPost("buyeroffer/{buyerOfferId}/user/{userId}")]
        public int InsertStoreOffer(int buyerOfferId, int userId)
        {
            int response = 0;
            response = _storeOfferProvider.Insert(buyerOfferId, userId);
            Console.WriteLine(JsonConvert.SerializeObject(response));
            return response;
        }

          [HttpPost]
        public async Task<List<StoreOffer>> Upsert()
        {
            List<StoreOffer> response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var storeOffers = JsonConvert.DeserializeObject<List<StoreOffer>>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeOfferProvider.UpsertMultiple(storeOffers);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

        [HttpGet("status")]
        public List<StoreOfferStatus> GetStoreOfferStatus()
        {
           return _storeOfferProvider.GetStoreOfferStatusAll();
        }

        [HttpGet("promodisplaytype")]
        public List<PromoDisplayType> GetPromoDisplayType()
        {
           return _storeOfferProvider.GetPromoDisplayType();
        }

         [HttpGet("offerType")]
        public List<OfferType> GetOfferType()
        {
           return _storeOfferProvider.GetOfferType();
        }

         [HttpGet("offerType/band/{id}")]
        public List<OfferType> GetOfferTypeByBandId(int id)
        {
           return _storeOfferProvider.GetOfferTypeByBandId(id);
        }

          [HttpPost("offerType")]
        public async Task<OfferType> InsertOfferType()
        {
            OfferType response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var storeOffers = JsonConvert.DeserializeObject<OfferType>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeOfferProvider.InsertOfferType(storeOffers);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

         [HttpGet("offerdisplayType/offerid/{id}")]
        public List<OfferDisplayType> GetOfferDisplayTypeByOfferId(int id)
        {
           return _storeOfferProvider.GetOfferDisplayTypesByOfferId(id);
        }

         [HttpGet("offerdisplayTypeoverride/offerid/{id}")]
        public List<StoreOfferDisplayOverride> GetOfferDisplayTypeByOfferIdForStore(int id)
        {
           return _storeOfferProvider.GetOfferDisplayTypesByOfferIdForStore(id);
        }

         [HttpPost("offerdisplayType")]
        public async Task<OfferDisplayTypeDb> InsetOfferDisplayType( )
        {
            OfferDisplayTypeDb response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var storeOffers = JsonConvert.DeserializeObject<OfferDisplayTypeDb>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeOfferProvider.InsertOfferDisplayType(storeOffers);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

         [HttpPost("offerdisplayType/update")]
        public async Task<OfferDisplayType> UpdateOfferDisplayType( )
        {
            OfferDisplayType response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var storeOffers = JsonConvert.DeserializeObject<OfferDisplayType>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeOfferProvider.UpdateOfferDisplayType(storeOffers);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

         [HttpPost("offerdisplayType/delete")]
        public async Task<OfferDisplayType> DeleteOfferDisplayType( )
        {
            OfferDisplayType response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var storeOffers = JsonConvert.DeserializeObject<OfferDisplayType>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeOfferProvider.DeleteOfferDisplayType(storeOffers);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }


        [HttpGet("storeOfferDisplaytypes/offerid/{offerid}/storeid/{storeid}")]
        public List<StoreOfferTypeDisplayTypesDb> GetStoreOfferDisplaytypes(int offerId, int storeId)
        {
           return _storeOfferProvider.GetOfferTypeDisplayTypesDb(offerId, storeId);
        }

         [HttpGet("storeOfferDisplaytypesstores/offerid/{offerid}")]
        public List<StoreOfferTypeDisplayTypesDb> GetStoreOfferDisplaytypesStores(int offerId)
        {
           return _storeOfferProvider.GetOfferTypeDisplayTypeStoreNameByOfferId(offerId);
        }

        [HttpPost("storeOfferDisplaytypesstores")]
        public async Task<List<StoreOfferTypeDisplayTypesDb>> InsetOfferTypeDisplayType( )
        {
            List<StoreOfferTypeDisplayTypesDb> response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var storeOffers = JsonConvert.DeserializeObject<List<StoreOfferTypeDisplayTypesDb>>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeOfferProvider.InsertOfferTypeDisplayType(storeOffers);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

         [HttpPost("storeOfferDisplaytypesstores/update")]
        public async Task<StoreOfferTypeDisplayTypesDb> UpdateOfferTypeDisplayType( )
        {
            StoreOfferTypeDisplayTypesDb response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var storeOffers = JsonConvert.DeserializeObject<StoreOfferTypeDisplayTypesDb>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeOfferProvider.UpdateOfferTypeDisplayType(storeOffers);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

        [HttpGet("order")]
        public List<Order> GetAllOrders()
        {
            return _storeOfferProvider.GetOrders();
        }

        [HttpGet("order/completed")]
        public List<Order> GetAllOrdersCompleted()
        {
            return _storeOfferProvider.GetOrdersComplated();
        }

          [HttpGet("order/orderid/{orderId}")]
        public List<OrderDto> GetAllOrdersById(int orderId)
        {
            return _storeOfferProvider.GetOrdersById(orderId);
        }

         [HttpPost("order/bandid/{bandId}/offertypeid/{offerTypeId}/userid/{userId}")]
        public int InsertOrder(int bandId, int offerTypeId, int userid)
        {
            return _storeOfferProvider.InsertOrder(bandId,userid,offerTypeId);
        }

         [HttpGet("printer")]
        public List<PrinterDto> GetPrinters()
        {
            return _storeOfferProvider.GetPrinters();
        }

         [HttpGet("reports")]
        public List<ReportDto> GetReports()
        {
            return _storeOfferProvider.GetReports();
        }


    }
}