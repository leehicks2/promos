using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class StoreController : ControllerBase
    {
        private readonly IStore _storeProvider;

        public StoreController(IStore bandprovider){
            _storeProvider = bandprovider;
        }

        [HttpGet]
        public List<Store> Get()
        {
            return _storeProvider.GetAllStores();
        }

        [HttpGet("band/{id}")]
         public List<Store> GetByBandId(int id)
        {
            return _storeProvider.GetStoreBandIdId(id);
        }

         [HttpGet("band/{id}/island/{islandId}")]
         public List<Store> GetByBandId(int id, int islandId)
        {
            return _storeProvider.GetStoreBandAndIsland(id, islandId);
        }
    }
}