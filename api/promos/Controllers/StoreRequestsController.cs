using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class StoreRequestsController : ControllerBase
    {
         private readonly IStoreRequests _storeRequestsProvider;

          public StoreRequestsController(IStoreRequests storeOfferprovider){
            _storeRequestsProvider = storeOfferprovider;
            }

        [HttpGet]
        public List<StoreRequests> Get()
        {
            return _storeRequestsProvider.GetAllStoreRequests();
        }

        [HttpGet("band/{band}")]
        public List<StoreRequestAction> GetByBandLetter(string band)
        {
            return _storeRequestsProvider.GetAllStoreRequestsByBand(band);
        }

         [HttpGet("order/{orderid}")]
        public List<StoreRequestAction> GetRequestsByOrderId(int orderid)
        {
            return _storeRequestsProvider.GetAllStoreRequestsByOrderId(orderid);
        }

         [HttpGet("storepending/store/{id}")]
        public List<StoreRequestAction> GetActiveByStore(int id)
        {
            return _storeRequestsProvider.GetAllActiveRequestsByStore(id);
        }

         [HttpGet("updatemap/mapId/{id}/amount/{amount}")]
        public bool updateMapping(int id, int amount)
        {
            return _storeRequestsProvider.UpdateTemplateMap(id, amount);
        }

        [HttpGet("confirmstorerequest/{id}")]
        public bool updateMapping(int id)
        {
            return _storeRequestsProvider.ConfirmStoreRequestByUser(id);
        }

         [HttpGet("activerequestorders/{bandletter}")]
        public List<StoreRequestOrder> GetActiveStoreRequestOrders(string bandletter)
        {
            return _storeRequestsProvider.GetActiveStoreRequestOrdersByBand(bandletter);
        }

         [HttpGet("confirmrequestorder/{orderid}")]
        public StoreRequestAction GetActiveStoreRequestOrders(int orderid)
        {
            return _storeRequestsProvider.ConfirmStoreRequest(orderid);
        }



        [HttpPost]
       public async Task<StoreRequests> Post(){

          var response = new StoreRequests();
           using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var requestTemplate = JsonConvert.DeserializeObject<StoreRequests>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeRequestsProvider.InsertStoreRequest(requestTemplate);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }
            return response;
        }

         [HttpPost("confirm")]
       public async Task<StoreRequestAction> PostConfirm(){

          var response = new StoreRequestAction();
           using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var requestTemplate = JsonConvert.DeserializeObject<StoreRequestAction>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _storeRequestsProvider.ConfirmStoreRequest(requestTemplate);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }
            return response;
        }

         [HttpGet("tutorials")]
        public List<Tutorial> GetTutorials()
        {
            return _storeRequestsProvider.GetTutorials();
        }

         [HttpGet("tutorialtypes/{orderid}")]
        public List<TutorialType> GetTutorialTypes(int orderid)
        {
            return _storeRequestsProvider.GetTutorialTypes(orderid);
        }

    }
}