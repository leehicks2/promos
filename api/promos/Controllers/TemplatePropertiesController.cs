using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers{
    
    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class TemplatePropertiesController : ControllerBase
    {
        private readonly ILogger<TemplatePropertiesController> _logger;
        private readonly ITemplateProperties _templatePropertiesProvider;

        public TemplatePropertiesController(ILogger<TemplatePropertiesController> logger, ITemplateProperties templatePropertiesProvider){
            _logger = logger;
            _templatePropertiesProvider = templatePropertiesProvider;
        }

        [HttpPost]
        public async Task<TemplateProperties> Post()
        {
            TemplateProperties response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var requestTemplate = JsonConvert.DeserializeObject<TemplateProperties>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _templatePropertiesProvider.Insert(requestTemplate);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

        [HttpPost("update")]
        public async Task<TemplateProperties> Put()
        {
            TemplateProperties response;

            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var requestTemplate = JsonConvert.DeserializeObject<TemplateProperties>(body);
                //Do sql check
                //var resp = _loginProvider.login(loginModel);
                response = _templatePropertiesProvider.Update(requestTemplate);
                Console.WriteLine(JsonConvert.SerializeObject(response));
            }

            return response;
        }

        [HttpGet("template/{id}")]
        public List<TemplateProperties> GetByTemplateId(int id)
        {
            return _templatePropertiesProvider.GetByTemplateId(id);
        }

         [HttpGet("{id}")]
        public TemplateProperties GetById(int id)
        {
            return _templatePropertiesProvider.GetById(id);
        }

         [HttpGet("field")]
        public List<Field> GetFields()
        {
            return _templatePropertiesProvider.GetFields();
        }

        [HttpGet("templateproperty/delete/{id}")]
        public bool DeleteTemplateProperty(int id)
        {
            _templatePropertiesProvider.DeleteTemplateProperties(id);

            return true;
        }

        [HttpGet("fonts/band/{id}")]
        public List<Fonts> GetFontsByBand(int id)
        {
            return _templatePropertiesProvider.GetFontsByBand(id);
        }
    }
}