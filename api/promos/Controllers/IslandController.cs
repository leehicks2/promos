using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class IslandController : ControllerBase
    {
        private readonly IStore _storeProvider;

        public IslandController(IStore bandprovider){
            _storeProvider = bandprovider;
        }

        [HttpGet]
        public List<Island> Get()
        {
            return _storeProvider.GetAllIslands();
        }

        [HttpGet("band/{id}")]
         public List<Island> GetByBandId(int id)
        {
            return _storeProvider.GetIslandsByBandId(id);
        }
    }
}