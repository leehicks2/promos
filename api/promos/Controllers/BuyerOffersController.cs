using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class BuyerOffersController : ControllerBase
    {
        private readonly ILogger<BuyerOffersController> _logger;
        private readonly IBuyerOffers _buyerOffers;

        private readonly IImportPromo _importPromos;

        public BuyerOffersController(ILogger<BuyerOffersController> logger, IBuyerOffers buyerOffers, IImportPromo importPromos)
        {
            _logger = logger;
            _buyerOffers = buyerOffers;
            _importPromos = importPromos;
        }


        [HttpPost("update")]
        public async Task<List<BuyerOffer>> Put()
        {
            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var buyerOfferList = JsonConvert.DeserializeObject<List<BuyerOffer>>(body);
                
                _importPromos.UpdatePromos(buyerOfferList);

                Console.WriteLine(JsonConvert.SerializeObject(buyerOfferList));
            }
           // Console.WriteLine(JsonConvert.SerializeObject(authResponse));
            return new List<BuyerOffer>();
        }

        [HttpGet]
        public List<BuyerOffer> Get()
        {
            return _buyerOffers.GetBuyerOffers();
        }

        [HttpGet("bandid/{bandId}")]
        public List<BuyerOffer> Get(string bandId)
        {
            return _buyerOffers.GetBuyerOffersByBand(bandId, 1);
        }

        [HttpGet("bandid/{bandId}/orderby/{orderby}")]
        public List<BuyerOffer> GetWithOrder(string bandId, string orderby)
        {
            return _buyerOffers.GetBuyerOffersByBandWithOrder(bandId, 1, orderby);
        }

         [HttpGet("bandid/{bandId}/orderby/{orderby}/filter/{filterBy}")]
        public List<BuyerOffer> GetWithOrder(string bandId, string orderby, string filterBy)
        {
            return _buyerOffers.GetBuyerOffersByBandWithOrderFilter(bandId, 1, orderby, filterBy);
        }

         [HttpGet("bandid/{bandId}/island/{island}")]
        public List<BuyerOffer> GetByBandAndIsland(string bandId, string island)
        {
            return _buyerOffers.GetBuyerOffersByBandAndIsland(bandId, island);
        }

         [HttpGet("bandid/{bandId}/island/{island}/orderby/{orderby}")]
        public List<BuyerOffer> GetByBandAndIslandAndOrder(string bandId, string island, string orderby)
        {
            return _buyerOffers.GetBuyerOffersByBandAndIslandAndOrder(bandId, island, orderby);
        }

         [HttpGet("bandid/{bandId}/island/{island}/orderby/{orderby}/filter/{filter}")]
        public List<BuyerOffer> GetByBandAndIslandAndOrderFilter(string bandId, string island, string orderby, string filter)
        {
            return _buyerOffers.GetBuyerOffersByBandAndIslandAndOrderFilter(bandId, island, orderby, filter);
        }

        [HttpGet("userid/{userid}")]
        public List<BuyerOffer> GetByUser(int userid)
        {
            return _buyerOffers.GetBuyerOffersByUser(userid);
        }

        [HttpGet("bandid/{bandId}/storeid/{storeid}")]
        public List<BuyerOffer> GetByBandAndStore(string bandId, string storeid)
        {
            return _buyerOffers.GetBuyerOffersByBandAndStore(bandId, storeid);
        }


        [HttpPost("Insertmultiplestoreorders")]
        public async Task<List<BuyerOffer>> InsertMultipleStoreOrders()
        {
            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var buyerOfferList = JsonConvert.DeserializeObject<List<int>>(body);
                
                _buyerOffers.InsertMultipeSoreOrders(buyerOfferList);

                Console.WriteLine(JsonConvert.SerializeObject(buyerOfferList));
            }
           // Console.WriteLine(JsonConvert.SerializeObject(authResponse));
            return new List<BuyerOffer>();
        }

         [HttpPost("deletemultiplestoreorders")]
        public async Task<List<BuyerOffer>> DeleteMultipleStoreOrders()
        {
            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var buyerOfferList = JsonConvert.DeserializeObject<List<int>>(body);
                
                _buyerOffers.DeleteMultipeSoreOrders(buyerOfferList);

                Console.WriteLine(JsonConvert.SerializeObject(buyerOfferList));
            }
           // Console.WriteLine(JsonConvert.SerializeObject(authResponse));
            return new List<BuyerOffer>();
        }
    }
    
}