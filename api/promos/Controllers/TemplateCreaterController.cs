using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers{
    
    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class TemplateCreaterController : ControllerBase
    {

        private readonly ILogger<TemplateCreaterController> _logger;
        private readonly ITemplateCreater _templateCreaterProvider;

         public TemplateCreaterController(ILogger<TemplateCreaterController> logger,  ITemplateCreater templateProvider)
        {
            _logger = logger;
            _templateCreaterProvider = templateProvider;
        }


         [HttpGet("{id}/printer/{printer}")]
        public CreatedModel GetById(int id, int printer)
        {
            return _templateCreaterProvider.ProcessOrder(id, printer);
        }

         [HttpGet("preview/buyeroffer/{buyeroffer}/template/{template}")]
        public CreatedModel Preview(int buyeroffer, int template)
        {
             _templateCreaterProvider.CreatePreview(buyeroffer, template);
            return new CreatedModel();
        }

    }

}