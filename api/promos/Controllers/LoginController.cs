﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class LoginController : ControllerBase
    {
        private readonly ILogger<LoginController> _logger;
        private readonly ILogin _loginProvider;

        public LoginController(ILogger<LoginController> logger, ILogin login)
        {
            _logger = logger;
            _loginProvider = login;
        }

        [HttpPost]
        public async Task<AuthResponseModel> Post()
        {
            //_loginProvider.SendEmail();
            var authResponse = new AuthResponseModel();
            using (var reader = new StreamReader(Request.Body))
            {
                var body = await reader.ReadToEndAsync();
                var loginModel = JsonConvert.DeserializeObject<LoginModel>(body);
                //Do sql check
                var resp = _loginProvider.login(loginModel);
                if(true){
                     authResponse = resp;
                }
            }
            Console.WriteLine(JsonConvert.SerializeObject(authResponse));
            return authResponse;
        }

        [HttpGet]
        public LoginModel Get()
        {
            return new LoginModel();
        }
    }
}
