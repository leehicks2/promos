using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [EnableCors("AllowOrigin")]  
    public class BandController : ControllerBase
    {
        private readonly IBand _bandProvider;

        public BandController(IBand bandprovider){
            _bandProvider = bandprovider;
        }

        [HttpGet]
        public List<Band> Get()
        {
            return _bandProvider.GetAllBands();
        }
    }
}