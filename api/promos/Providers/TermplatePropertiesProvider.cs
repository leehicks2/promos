using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers.Providers{
    public class TemplatePropertiesProvider: ITemplateProperties{

        private readonly IConfiguration _config;
        DataSourceConnection dataSourceConnection;

        public TemplatePropertiesProvider(IConfiguration config){
            _config = config;
           dataSourceConnection = new DataSourceConnection(_config);
        }

        public List<TemplateProperties> GetTemplatePropertyByTemplateId(int id){
            return new List<TemplateProperties>();
        }

        public TemplateProperties Insert(TemplateProperties templateProperties){
            Console.WriteLine(JsonConvert.SerializeObject(templateProperties));
             try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {

                    connection.Open();      
                   // string sql = $"INSERT INTO dbo.TemplateProperties (templateId,width, height, topProperty, leftProperty, fontSize, color, spacing, fontWeight, active, textProperty, FieldId, fontstyle) OUTPUT INSERTED.ID values ('{templateProperties.templateId}', '{templateProperties.width}', '{templateProperties.height}', '{templateProperties.topProperty}', '{templateProperties.leftProperty}', '{templateProperties.fontSize}', '{templateProperties.color}', '{templateProperties.spacing}', '{templateProperties.fontWeight}','{templateProperties.active}', '{templateProperties.textProperty}', '{templateProperties.FieldId}', '{templateProperties.fontstyle}')"; 
                   // string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                   string sql = $"[dbo].[UpsertTemplateProperties]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        var fieldId = templateProperties.FieldId == 0 ? 1: templateProperties.FieldId;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@Id", templateProperties.id));
                        command.Parameters.Add(new SqlParameter("@templateID", templateProperties.templateId));
                        command.Parameters.Add(new SqlParameter("@width", templateProperties.width));
                        command.Parameters.Add(new SqlParameter("@height", templateProperties.height));
                        command.Parameters.Add(new SqlParameter("@topProperty", templateProperties.topProperty));
                        command.Parameters.Add(new SqlParameter("@leftProperty", templateProperties.leftProperty));
                        command.Parameters.Add(new SqlParameter("@fontSize", templateProperties.fontSize));
                        command.Parameters.Add(new SqlParameter("@color", templateProperties.color));
                        command.Parameters.Add(new SqlParameter("@spacing", templateProperties.spacing));
                        command.Parameters.Add(new SqlParameter("@fontWeight", templateProperties.fontWeight));
                        command.Parameters.Add(new SqlParameter("@active", "false"));
                        command.Parameters.Add(new SqlParameter("@textProperty", templateProperties.textProperty));
                        command.Parameters.Add(new SqlParameter("@Fieldid", fieldId));
                        command.Parameters.Add(new SqlParameter("@FontStyle", templateProperties.fontstyle));
                        command.Parameters.Add(new SqlParameter("@Strikethrough", templateProperties.Strikethrough));
                        command.Parameters.Add(new SqlParameter("@TextAlign", templateProperties.TextAlign));
                        command.Parameters.Add(new SqlParameter("@DateFormat", templateProperties.DateFormat));
                        command.Parameters.Add(new SqlParameter("@Rotate", templateProperties.Rotate));
                        command.Parameters.Add(new SqlParameter("@TextBefore", templateProperties.TextBefore));
                        command.Parameters.Add(new SqlParameter("@TextAfter", templateProperties.TextAfter));
                        command.Parameters.Add(new SqlParameter("@VerticalAlign", templateProperties.VerticalAlign));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                               // Console.WriteLine(Int32.Parse(reader["Id"].ToString()));
                                templateProperties.id = Int32.Parse(reader["Id"].ToString());
                            }
                        }
                    }         

                    // using (SqlCommand command = new SqlCommand(sql, connection))
                    // {
                    //     //command.ExecuteNonQuery();
                    //     int id = (int)command.ExecuteScalar();
                    //     templateProperties.id = id;
                    // }                 
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

             return templateProperties;
        }

        public TemplateProperties Update(TemplateProperties templateProperties){
             Console.WriteLine(JsonConvert.SerializeObject(templateProperties));
             try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {

                    connection.Open();  
                   // string sql = $"UPDATE dbo.TemplateProperties SET templateId = '{templateProperties.templateId}', width = '{templateProperties.width}' , height = '{templateProperties.height}', topProperty = '{templateProperties.topProperty}', leftProperty = '{templateProperties.leftProperty}', fontSize = '{templateProperties.fontSize}', color = '{templateProperties.color}', spacing = '{templateProperties.spacing}', fontWeight = '{templateProperties.fontWeight}', active = 'false', textProperty = '{templateProperties.textProperty}', Fieldid = '{templateProperties.FieldId}', fontstyle = '{templateProperties.fontstyle}' WHERE id = '{templateProperties.id}' ";      
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                   string sql = $"[dbo].[UpsertTemplateProperties]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        var fieldId = templateProperties.FieldId == 0 ? 1: templateProperties.FieldId;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@Id", templateProperties.id));
                        command.Parameters.Add(new SqlParameter("@templateID", templateProperties.templateId));
                        command.Parameters.Add(new SqlParameter("@width", templateProperties.width));
                        command.Parameters.Add(new SqlParameter("@height", templateProperties.height));
                        command.Parameters.Add(new SqlParameter("@topProperty", templateProperties.topProperty));
                        command.Parameters.Add(new SqlParameter("@leftProperty", templateProperties.leftProperty));
                        command.Parameters.Add(new SqlParameter("@fontSize", templateProperties.fontSize));
                        command.Parameters.Add(new SqlParameter("@color", templateProperties.color));
                        command.Parameters.Add(new SqlParameter("@spacing", templateProperties.spacing));
                        command.Parameters.Add(new SqlParameter("@fontWeight", templateProperties.fontWeight));
                        command.Parameters.Add(new SqlParameter("@active", "false"));
                        command.Parameters.Add(new SqlParameter("@textProperty", templateProperties.textProperty));
                        command.Parameters.Add(new SqlParameter("@Fieldid", fieldId));
                        command.Parameters.Add(new SqlParameter("@FontStyle", templateProperties.fontstyle));
                        command.Parameters.Add(new SqlParameter("@Strikethrough", templateProperties.Strikethrough));
                        command.Parameters.Add(new SqlParameter("@TextAlign", templateProperties.TextAlign));
                        command.Parameters.Add(new SqlParameter("@DateFormat", templateProperties.DateFormat));
                        command.Parameters.Add(new SqlParameter("@Rotate", templateProperties.Rotate));
                        command.Parameters.Add(new SqlParameter("@TextBefore", templateProperties.TextBefore));
                        command.Parameters.Add(new SqlParameter("@TextAfter", templateProperties.TextAfter));
                         command.Parameters.Add(new SqlParameter("@VerticalAlign", templateProperties.VerticalAlign));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                               // templateProperties.id = reader.GetInt32(0);
                            }
                        }
                    }            

                    //  using (SqlCommand command = new SqlCommand(sql, connection))
                    // {
                    //     command.ExecuteNonQuery();
                    //     // int id = (int)command.ExecuteScalar();
                    //     // templateProperties.id = id;
                    // }            
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

             return templateProperties;
        }

        public TemplateProperties GetById(int id){
            var responseTemplate = new TemplateProperties();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"select * from dbo.TemplateProperties where id='{id}'";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new TemplateProperties();
                                template.id = Int32.Parse(reader["id"].ToString()); 
                                template.templateId = Int32.Parse(reader["templateId"].ToString());
                                template.width = reader["width"].ToString();
                                template.height = reader["height"].ToString();
                                template.topProperty = reader["topProperty"].ToString();
                                template.leftProperty = reader["leftProperty"].ToString();
                                template.fontSize = reader["fontSize"].ToString();
                                template.color = reader["color"].ToString();
                                template.spacing = reader["spacing"].ToString();
                                template.fontWeight = reader["fontWeight"].ToString();
                                template.active =  bool.Parse(reader["active"].ToString());
                                template.textProperty = reader["textProperty"].ToString();
                                template.FieldId = Int32.Parse(reader["FieldId"].ToString());
                                template.fontstyle = reader["fontstyle"].ToString();
                                template.TextAlign = reader["textalign"].ToString();
                                template.Strikethrough = Int32.Parse(reader["strikethrough"].ToString());
                                template.DateFormat = Int32.Parse(reader["dateFormat"].ToString());
                                template.Rotate = Int32.Parse(reader["rotate"].ToString());
                                template.TextBefore = reader["textBefore"].ToString();
                                template.TextAfter = reader["textAfter"].ToString();
                                template.VerticalAlign = reader["verticalAlign"].ToString();
                                responseTemplate = template;
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return responseTemplate;
        }

        public  List<TemplateProperties> GetByTemplateId(int id){
            var responseTemplate = new List<TemplateProperties>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"[dbo].[GETTemplateProperties]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@templateID", id));
                        command.Parameters.Add(new SqlParameter("@Fieldid", -1));
                        command.Parameters.Add(new SqlParameter("@id", -1));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new TemplateProperties();
                                template.id = Int32.Parse(reader["id"].ToString()); 
                                template.templateId = Int32.Parse(reader["templateId"].ToString());
                                template.width = reader["width"].ToString();
                                template.height = reader["height"].ToString();
                                template.topProperty = reader["topProperty"].ToString();
                                template.leftProperty = reader["leftProperty"].ToString();
                                template.fontSize = reader["fontSize"].ToString();
                                template.color = reader["color"].ToString();
                                template.spacing = reader["spacing"].ToString();
                                template.fontWeight = reader["fontWeight"].ToString();
                                template.active =  bool.Parse(reader["active"].ToString());
                                template.textProperty = reader["textProperty"].ToString();
                                template.FieldId = Int32.Parse(reader["FieldId"].ToString());
                                template.FieldName = reader["FieldName"].ToString();
                                template.fontstyle = reader["fontstyle"].ToString();
                                template.Strikethrough = Int32.Parse(reader["strikethrough"].ToString()); 
                                template.TextAlign = reader["textalign"].ToString();
                                template.DateFormat = Int32.Parse(reader["dateFormat"].ToString());
                                template.Rotate = Int32.Parse(reader["rotate"].ToString());
                                template.TextBefore = reader["textBefore"].ToString();
                                template.TextAfter = reader["textAfter"].ToString();
                                 template.VerticalAlign = reader["verticalAlign"].ToString();
                                 template.Description = reader["Description"].ToString();
                                responseTemplate.Add(template);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return responseTemplate;
        }

        public List<Field> GetFields(){
            var responseTemplate = new List<Field>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"[dbo].[GETField]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@FieldID", -1));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var field = new Field();
                                field.id = reader.GetInt32(0);
                                field.FieldName = reader.GetString(1);
                                field.Description = reader.GetString(2);

                                responseTemplate.Add(field);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return responseTemplate;
        }

        public bool DeleteTemplateProperties(int id){
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"[dbo].[DeleteTemplateProperties]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@id", id));
                        command.ExecuteNonQuery();
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return true;
        }

        public List<Fonts> GetFontsByBand(int id){
            var responseTemplate = new List<Fonts>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"select * from dbo.Font where BandId='{id}'";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new Fonts();
                                template.FontID = Int32.Parse(reader["FontId"].ToString()); 
                                template.BandId = Int32.Parse(reader["BandId"].ToString());
                                template.Font = reader["Font"].ToString();
                                responseTemplate.Add(template);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return responseTemplate;
        }
    }
}