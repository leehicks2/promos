using System.Collections.Generic;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

using System.Data.SqlClient;
using System;
using Newtonsoft.Json;
using System.Data;
using Microsoft.Extensions.Configuration;

namespace promos.Controllers.Providers{
    public class BuyerOffers: IBuyerOffers{

        private readonly IConfiguration _config;
    
        public BuyerOffers(IConfiguration config){
            _config = config;
        }

        public List<BuyerOffer> GetBuyerOffers(){

            var dataSourceConnection = new DataSourceConnection(_config);
            var buyerOffersList = new List<BuyerOffer>();

            try{
               SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();    
                    
                    string sql = $"[dbo].[GetBuyerOffers]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                               //c user.Userid = reader.GetInt32(0);
                               
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> GetBuyerOffersByBand(string BandId, int userId){
             var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();    
                    
                    string sql = $"[dbo].[GetBuyerOffers]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@BandId", BandId));
                        

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                               //c user.Userid = reader.GetInt32(0);
                               
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> GetBuyerOffersByBandWithOrder(string BandId, int userId, string orderBy){
             var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();    
                    
                    string sql = $"[dbo].[GetBuyerOffers]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@BandId", BandId));
                        command.Parameters.Add(new SqlParameter("@Field", orderBy));
                        

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                               //c user.Userid = reader.GetInt32(0);
                               
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

         public List<BuyerOffer> GetBuyerOffersByBandWithOrderFilter(string BandId, int userId, string orderBy, string filterBy){
             var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();    
                    
                    string sql = $"[dbo].[GetBuyerOffers]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@BandId", BandId));
                        command.Parameters.Add(new SqlParameter("@Field", orderBy));
                        command.Parameters.Add(new SqlParameter("@FieldFilter", filterBy));
                        

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                               //c user.Userid = reader.GetInt32(0);
                               
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> GetBuyerOffersByBandAndIsland(string BandId, string islandName){
             var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();    
                    
                    string sql = $"[dbo].[GetBuyerOffers]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@BandId", BandId));
                        command.Parameters.Add(new SqlParameter("@Island", islandName));
                        

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                               //c user.Userid = reader.GetInt32(0);
                               
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> GetBuyerOffersByBandAndIslandAndOrder(string BandId, string islandName, string orderby){
             var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();    
                    
                    string sql = $"[dbo].[GetBuyerOffers]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@BandId", BandId));
                        command.Parameters.Add(new SqlParameter("@Island", islandName));
                        command.Parameters.Add(new SqlParameter("@Field", orderby));
                         
                        
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                               //c user.Userid = reader.GetInt32(0);
                               
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> GetBuyerOffersByBandAndIslandAndOrderFilter(string BandId, string islandName, string orderby, string filter)
        {
            var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource;
                builder.UserID = dataSourceConnection.UserId;
                builder.Password = dataSourceConnection.Password;
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = $"[dbo].[GetBuyerOffers]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@BandId", BandId));
                        command.Parameters.Add(new SqlParameter("@Island", islandName));
                        command.Parameters.Add(new SqlParameter("@Field", orderby));
                        command.Parameters.Add(new SqlParameter("@FieldFilter", filter));


                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                                //c user.Userid = reader.GetInt32(0);

                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> InsertMultipeSoreOrders(List<int> buyerOffers)
        {
            var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource;
                builder.UserID = dataSourceConnection.UserId;
                builder.Password = dataSourceConnection.Password;
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = $"[dbo].[AddRemoveStoreOffer]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@BuyerOfferIdList", String.Join(",", buyerOffers)));
                        command.Parameters.Add(new SqlParameter("@AllocatorUserID", 1));

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> DeleteMultipeSoreOrders(List<int> buyerOffers)
        {
            var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource;
                builder.UserID = dataSourceConnection.UserId;
                builder.Password = dataSourceConnection.Password;
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    string sql = $"[dbo].[AddRemoveStoreOffer]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@BuyerOfferIdList", String.Join(",", buyerOffers)));
                        command.Parameters.Add(new SqlParameter("@AllocatorUserID", 1));
                        command.Parameters.Add(new SqlParameter("@Id", 1));

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> GetBuyerOffersByUser(int userId){
             var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();    
                    
                    string sql = $"[dbo].[GetBuyerOffers]";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@UserId", userId));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                               //c user.Userid = reader.GetInt32(0);
                               
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }

        public List<BuyerOffer> GetBuyerOffersByBandAndStore(string BandId, string StoreId){
             var dataSourceConnection = new DataSourceConnection(_config);

            var buyerOffersList = new List<BuyerOffer>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();    
                    
                    String sql = $"SELECT * from dbo.BuyerOffer where PrBand = '{BandId}' and PromIsland = '{StoreId}'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var buyerOffer = new BuyerOffer();
                                buyerOffer.BuyersOfferID = reader.GetInt32(0);
                                buyerOffer.BuyerUserID = reader.GetInt32(1);
                                buyerOffer.CreatedDate = reader.GetDateTime(2);
                                buyerOffer.PromRecordId = reader.GetString(3);
                                buyerOffer.PromProdCode = reader.GetString(4);
                                buyerOffer.PromIsland = reader.GetString(5);
                                buyerOffer.PrBand = reader.GetString(6);
                                buyerOffer.PromCode = reader.IsDBNull(7) ? "" : reader.GetString(7);
                                buyerOffer.PromDescription = reader.GetString(8);
                                buyerOffer.PromSupp = reader.GetString(9);
                                buyerOffer.PromStartRetDate = reader.GetString(10);
                                buyerOffer.PromEndRetDate = reader.GetString(11);
                                buyerOffer.PromStartCostDate = reader.GetString(12);
                                buyerOffer.PromEndCostDate = reader.GetString(13);
                                buyerOffer.CurrentCost = reader.GetString(14);
                                buyerOffer.PromCost = reader.GetString(15);
                                buyerOffer.PromMessage = reader.GetString(16);
                                buyerOffer.CurrentRetail = reader.GetString(17);
                                buyerOffer.PromRetail = reader.GetString(18);
                                buyerOffer.SuppProdCode = reader.GetString(19);
                                buyerOffer.HostedInd = reader.IsDBNull(20) ? "" : reader.GetString(20);
                                buyerOffer.PromDeptDesc = reader.GetString(21);
                                buyerOffer.proddescrip = reader.GetString(22);
                                buyerOffer.Size = reader.GetString(23);
                                buyerOffer.Savings = reader.GetString(24);
                                buyerOffer.Slogan = reader.GetString(25);
                                buyerOffer.MultibuyPrice = reader.GetString(27);
                                buyerOffer.MultibuyQty = reader.GetString(28);
                                buyerOffer.GenericDescription = reader.GetString(29);
                                buyerOffer.BandName = reader.GetString(31);
                                buyerOffersList.Add(buyerOffer);
                               //c user.Userid = reader.GetInt32(0);
                               
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return buyerOffersList;
        }
    }
    }