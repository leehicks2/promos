using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Extensions.Configuration;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers.Providers{
    public class StoreRequestsProvider: IStoreRequests{
         private readonly IConfiguration _config;
        public StoreRequestsProvider(IConfiguration config){
            _config = config;
        }

        public List<StoreRequests> GetAllStoreRequests(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequests>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select * from StoreRequests where StatusId = '1004'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new StoreRequests();
                                store.StoreRequestsId = Int32.Parse(reader["StoreRequestsID"].ToString());
                                store.BuyersOfferId = Int32.Parse(reader["BuyersOfferID"].ToString());
                                store.UserId =  Int32.Parse(reader["UserID"].ToString());
                                store.SizeId =  Int32.Parse(reader["SizeID"].ToString());
                                store.Quantity =  Int32.Parse(reader["Quantity"].ToString());
                                store.StatusId =  Int32.Parse(reader["StatusId"].ToString());
                                store.Orientation =  reader["Orientation"].ToString();
                                store.storeRequestTemplates = new List<StoreRequestTemplates>();
                                store.StoreId = Int32.Parse(reader["StoreId"].ToString());
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<StoreRequests> GetAllStoreRequestsForStore(int storeId){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequests>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select * from StoreRequests where StatusId = '1006' and StoreId={storeId}";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new StoreRequests();
                                store.StoreRequestsId = Int32.Parse(reader["StoreRequestsID"].ToString());
                                store.BuyersOfferId = Int32.Parse(reader["BuyersOfferID"].ToString());
                                store.UserId =  Int32.Parse(reader["UserID"].ToString());
                                store.SizeId =  Int32.Parse(reader["SizeID"].ToString());
                                store.Quantity =  Int32.Parse(reader["Quantity"].ToString());
                                // store.TemplateId =  Int32.Parse(reader["TemplateID"].ToString());
                                store.StatusId =  Int32.Parse(reader["StatusId"].ToString());
                                store.Orientation =  reader["Orientation"].ToString();
                                store.storeRequestTemplates = new List<StoreRequestTemplates>();
                                store.StoreId = Int32.Parse(reader["StoreId"].ToString());
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<StoreRequestAction> GetAllStoreRequestsByBand(string bandLetter){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequestAction>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select a.BuyersOfferID, a.StoreRequestsID,a.StatusID,
                                b.PromProdCode, b.PromDescription, c.templateName, c.id AS templateId, d.UserName, e.Amount,
                                f.StoreID, f.StoreName, z.Description as SizeDescription, y.id as BandId, y.BandLetter, e.Id as TemplateMapId
                                from StoreRequests a
                                inner join BuyerOffer b on b.BuyersOfferID = a.BuyersOfferID
                                --inner join PromoDisplayType c on c.id = a.SizeID
                                inner join SystemUser d on d.Userid = a.UserID
                                inner join StoreRequestsTemplateMapping e on e.StoreRequestsId = a.StoreRequestsID
                                inner join Template c on c.id = e.TemplateId
                                inner join PromoDisplayType z on z.id = c.templateType                    
                                inner join Store f on f.StoreID = a.StoreID
                                Inner JOIN IslandBandMap ce on  ce.ID=f.IslandBandMapID
                                inner join Band y on y.id =ce.BandID
                                WHERE b.PrBand = '{bandLetter}' AND
                                a.StatusID = '1004'
                                ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new StoreRequestAction();
                                store.StoreRequestsId = Int32.Parse(reader["StoreRequestsID"].ToString());
                                store.BuyersOfferId = Int32.Parse(reader["BuyersOfferID"].ToString());
                                store.UserName =  reader["UserName"].ToString();
                                store.PromDescription =  reader["PromDescription"].ToString();
                                store.SizeDescription =  reader["SizeDescription"].ToString();
                                store.TemplateId =  Int32.Parse(reader["TemplateID"].ToString());
                                store.TemplateName =  reader["TemplateName"].ToString();
                                store.StatusId =  Int32.Parse(reader["StatusId"].ToString());
                                store.StoreName =  reader["StoreName"].ToString();
                                store.StoreID = Int32.Parse(reader["StoreID"].ToString());
                                store.Amount = Int32.Parse(reader["Amount"].ToString());
                                store.BandLetter = reader["BandLetter"].ToString();
                                store.BandId = Int32.Parse(reader["BandId"].ToString());
                                store.PromProdCode =  reader["PromProdCode"].ToString();
                                store.TemplateMapId = Int32.Parse(reader["TemplateMapId"].ToString());
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<StoreRequestAction> GetAllStoreRequestsByOrderId(int orderId){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequestAction>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select a.BuyersOfferID, a.StoreRequestsID,a.StatusID,
                                    b.PromProdCode, b.PromDescription, c.templateName, c.id AS templateId, d.UserName, e.Amount,
                                    a.StoreID, f.StoreName,a.StoreRequestOrderId, z.Description as SizeDescription, y.id as BandId, y.BandLetter, e.Id as TemplateMapId
                                    ,zz.RequestOrderID
                                    from StoreRequests a
                                    inner join BuyerOffer b on b.BuyersOfferID = a.BuyersOfferID
                                    --inner join PromoDisplayType c on c.id = a.SizeID
                                    inner join SystemUser d on d.Userid = a.UserID
                                    inner join StoreRequestsTemplateMapping e on e.StoreRequestsId = a.StoreRequestsID
                                    inner join Template c on c.id = e.TemplateId
                                    inner join Store f on f.StoreID = a.StoreID
                                    inner join PromoDisplayType z on z.id = c.templateType
                                    inner join RequestOrders zz on zz.RequestOrderID = a.StoreRequestOrderId
                                    Inner JOIN IslandBandMap ce on  ce.ID=f.IslandBandMapID
                                    inner join Band y on y.id =ce.BandID
                                    WHERE zz.RequestOrderID = '{orderId}'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new StoreRequestAction();
                                store.StoreRequestsId = Int32.Parse(reader["StoreRequestsID"].ToString());
                                store.BuyersOfferId = Int32.Parse(reader["BuyersOfferID"].ToString());
                                store.UserName =  reader["UserName"].ToString();
                                store.PromDescription =  reader["PromDescription"].ToString();
                                store.SizeDescription =  reader["SizeDescription"].ToString();
                                store.TemplateId =  Int32.Parse(reader["TemplateID"].ToString());
                                store.TemplateName =  reader["TemplateName"].ToString();
                                store.StatusId =  Int32.Parse(reader["StatusId"].ToString());
                                store.StoreName =  reader["StoreName"].ToString();
                                store.StoreID = Int32.Parse(reader["StoreID"].ToString());
                                store.Amount = Int32.Parse(reader["Amount"].ToString());
                                store.BandLetter = reader["BandLetter"].ToString();
                                store.BandId = Int32.Parse(reader["BandId"].ToString());
                                store.PromProdCode =  reader["PromProdCode"].ToString();
                                store.TemplateMapId = Int32.Parse(reader["TemplateMapId"].ToString());
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

         public List<StoreRequestAction> GetAllActiveRequestsByStore(int storeId){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequestAction>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select a.BuyersOfferID, a.StoreRequestsID,a.StatusID,
                                b.PromProdCode, b.PromDescription, c.templateName, c.id AS templateId, d.UserName, e.Amount,
                                D.StoreID, f.StoreName, z.Description as SizeDescription, y.id as BandId, y.BandLetter, e.Id as TemplateMapId
                                -- Select *
                                from StoreRequests a
                                inner join BuyerOffer b on b.BuyersOfferID = a.BuyersOfferID
                                --inner join PromoDisplayType c on c.id = a.SizeID
                                inner join SystemUser d on d.Userid = a.UserID
                                inner join StoreRequestsTemplateMapping e on e.StoreRequestsId = a.StoreRequestsID
                                inner join Template c on c.id = e.TemplateId
                                inner join Store f on f.StoreID = a.StoreID
                                inner join PromoDisplayType z on z.id = c.templateType
                                Inner JOIN IslandBandMap ce on  ce.ID=f.IslandBandMapID
                                inner join Band y on y.id =ce.BandID
                            WHERE f.StoreId = '{storeId}' AND a.StatusID = '1006' ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new StoreRequestAction();
                                store.StoreRequestsId = Int32.Parse(reader["StoreRequestsID"].ToString());
                                store.BuyersOfferId = Int32.Parse(reader["BuyersOfferID"].ToString());
                                store.UserName =  reader["UserName"].ToString();
                                store.PromDescription =  reader["PromDescription"].ToString();
                                store.SizeDescription =  reader["SizeDescription"].ToString();
                                store.TemplateId =  Int32.Parse(reader["TemplateID"].ToString());
                                store.TemplateName =  reader["TemplateName"].ToString();
                                store.StatusId =  Int32.Parse(reader["StatusId"].ToString());
                                store.StoreName =  reader["StoreName"].ToString();
                                store.StoreID = Int32.Parse(reader["StoreID"].ToString());
                                store.Amount = Int32.Parse(reader["Amount"].ToString());
                                store.BandLetter = reader["BandLetter"].ToString();
                                store.BandId = Int32.Parse(reader["BandId"].ToString());
                                store.PromProdCode =  reader["PromProdCode"].ToString();
                                store.TemplateMapId = Int32.Parse(reader["TemplateMapId"].ToString());
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public StoreRequests InsertStoreRequest(StoreRequests request){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequests>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"insert into StoreRequests (BuyersOfferID, UserID,SizeID, Quantity, StatusID,Orientation, StoreId)
                                OUTPUT INSERTED.StoreRequestsId
                                values ('{request.BuyersOfferId}', '{request.UserId}', 
                                '1', '2', 
                                '{request.StatusId}', 'LandScape', '{request.StoreId}')";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            //command.ExecuteNonQuery();
                            int id = (int)command.ExecuteScalar();
                            request.StoreRequestsId = id;
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            for(var x = 0; x < request.storeRequestTemplates.Count; x++){
                request.storeRequestTemplates[x].StoreRequestId = request.StoreRequestsId;
                request.storeRequestTemplates[x] = InsertStoreRequestTemplates(request.storeRequestTemplates[x]);
            }
            
            return request;
        }

        public bool UpdateTemplateMap(int id, int amount){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequests>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"update StoreRequestsTemplateMapping set Amount = {amount} where Id = {id}";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.ExecuteNonQuery();
                        }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return true;
        }

        public bool ConfirmStoreRequestByUser(int storeId){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequests>();

            var orderId = InsertRequestOrder();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"update StoreRequests set StatusId = '1004', StoreRequestOrderId = '{orderId}' where StatusId = '1006' and StoreId = '{storeId}'";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.ExecuteNonQuery();
                        }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return true;
        }

        public StoreRequestAction ConfirmStoreRequest(StoreRequestAction request){
            var dataSourceConnection = new DataSourceConnection(_config);
          
            //Create Offer Type - GUUID -- Return Offer Tpe Id
            //INSERT INTO dbo.OfferType (Name, BandId) OUTPUT INSERTED.ID values ('{storeOffer.Name}', '{storeOffer.BandId}') Amount
           
            //Add Multiple Template to the OfferTypeDisplayType - Store Request Template Id with Offer Type ID returned
            // INSERT INTO dbo.OfferTypeDisplayTypes (DisplayTypeId, OfferTypeId, Amount) 
            //         OUTPUT INSERTED.ID 
            //         values ('{storeOffer.DisplayTypeId}', '{storeOffer.OfferTypeId}', '{storeOffer.Amount}
            // "
            //Create Store Offer

            //Create Store Offer
            // INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, templateId, Amount, SizeAllocationId, LabelSizeId, AllocatorUserId, StatusId) 
            // OUTPUT INSERTED.StoreOffersId values ('{storeOffer.BuyersOfferId}', '{storeOffer.StoreID}', '{storeOffer.TemplateId}', '{storeOffer.Amount}', '1', '1', '1','{storeOffer.StatusId}')

              var storeId = request.StoreID;
              var bandLetter = request.BandLetter;
              var bandId = request.BandId;
              var offerId = CreateOfferType(bandId);
              var temps = GetStoreRequestTemplates(request.StoreRequestsId);

              foreach(var temp in temps){
                  CreateOfferDisplatType(offerId, temp.TemplateId, temp.Amount);
              }     
            
            var storeOfferId = 0;
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    string sql = @$"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, AllocatorUserId, StatusId, OfferTypeID) 
                    OUTPUT INSERTED.StoreOffersId values ('{request.BuyersOfferId}', '{storeId}', '1', '4', '{offerId}')";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            //command.ExecuteNonQuery();
                            int id = (int)command.ExecuteScalar();
                            storeOfferId = id;
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            var orderId = InsertOrder();

            var after = UpdateStoreOffer(orderId, storeOfferId);

            UpdateStoreRequestStatus(request.StoreRequestsId);
            
            return request;
        }

        public StoreRequestAction ConfirmStoreRequest(int orderIdReq){
            var dataSourceConnection = new DataSourceConnection(_config);
          
            var orderId = InsertOrder();
            var requestsOriginal = GetAllStoreRequestsByOrderId(orderIdReq);

            var requests = requestsOriginal
            .GroupBy(p => p.StoreRequestsId)
            .Select(g => g.First())
            .ToList();
            

            foreach(var request in requests){
             var storeId = request.StoreID;;
              var bandLetter = request.BandLetter;
              var bandId = request.BandId;
              
              var offerId = CreateOfferType(request.BandId);
              var temps = GetStoreRequestTemplates(request.StoreRequestsId);

              foreach(var temp in temps){
                  CreateOfferDisplatType(offerId, temp.TemplateId, temp.Amount);
              }     
            
            var storeOfferId = 0;
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    string sql = @$"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, AllocatorUserId, StatusId, OfferTypeID) 
                    OUTPUT INSERTED.StoreOffersId values ('{request.BuyersOfferId}', '{storeId}', '1', '4', '{offerId}')";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            //command.ExecuteNonQuery();
                            int id = (int)command.ExecuteScalar();
                            storeOfferId = id;
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            var after = UpdateStoreOffer(orderId, storeOfferId);

            UpdateStoreRequestStatus(request.StoreRequestsId);
        }
            
             UpdateStoreRequestOrderStatus(orderIdReq);
            
            return new StoreRequestAction();
        }

        private StoreRequestTemplates InsertStoreRequestTemplates(StoreRequestTemplates request){
            var dataSourceConnection = new DataSourceConnection(_config);

           
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"insert into StoreRequestsTemplateMapping 
                                    (StoreRequestsId, TemplateId, Amount) 
                                    OUTPUT INSERTED.id
                                    values ('{request.StoreRequestId}','{request.TemplateId}','{request.Amount}')";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            //command.ExecuteNonQuery();
                            int id = (int)command.ExecuteScalar();
                            request.Id = id;
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return request;
        }

        public List<StoreRequestTemplates> GetStoreRequestTemplates(int StoreRequestId){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandId = new List<StoreRequestTemplates>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select * from StoreRequestsTemplateMapping where StoreRequestsId = '{StoreRequestId}'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var x = new StoreRequestTemplates();
                                x.Id = Int32.Parse(reader["id"].ToString());
                                x.TemplateId = Int32.Parse(reader["TemplateId"].ToString());
                                x.StoreRequestId = Int32.Parse(reader["StoreRequestsId"].ToString());
                                x.Amount = Int32.Parse(reader["Amount"].ToString());
                                bandId.Add(x);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandId;
        }

        private int CreateOfferType(int bandId){
            var dataSourceConnection = new DataSourceConnection(_config);
            var offerTypeId = 0;
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"INSERT INTO dbo.OfferType (Name, BandId) 
                    OUTPUT INSERTED.ID values ('Test Full', '{bandId}')";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            //command.ExecuteNonQuery();
                            int id = (int)command.ExecuteScalar();
                            offerTypeId = id;
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return offerTypeId;
        }

        private int CreateOfferDisplatType(int offerType, int template, int amount){
            var dataSourceConnection = new DataSourceConnection(_config);
            var offerTypeId = 0;
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"INSERT INTO dbo.OfferTypeDisplayTypes (DisplayTypeId, OfferTypeId, Amount) 
                        OUTPUT INSERTED.ID 
                        values ('{template}', '{offerType}', '{amount}')";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            //command.ExecuteNonQuery();
                            int id = (int)command.ExecuteScalar();
                            offerTypeId = id;
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return offerTypeId;
        }

         private int UpdateStoreOffer(int offerType, int storeOffers){
            var dataSourceConnection = new DataSourceConnection(_config);
            var offerTypeId = 0;
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"update StoreOffer set OrderID = '{offerType}' where StoreOffersID = '{storeOffers}'";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.ExecuteNonQuery();
                            // int id = (int)command.ExecuteScalar();
                            // offerTypeId = id;
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return offerTypeId;
        }

        private int UpdateStoreRequestStatus(int reqId){
            var dataSourceConnection = new DataSourceConnection(_config);
            var offerTypeId = 0;
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"update StoreRequests set StatusID = '1005' where StoreRequestsID = '{reqId}'";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.ExecuteNonQuery();
                            // int id = (int)command.ExecuteScalar();
                            // offerTypeId = id;
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return offerTypeId;
        }

        private int UpdateStoreRequestOrderStatus(int orderId){
            var dataSourceConnection = new DataSourceConnection(_config);
            var offerTypeId = 0;
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"update RequestOrders SET Statusid = '1009' where RequestOrderID = {orderId}";

                       using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.ExecuteNonQuery();
                        }    
                                       
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return offerTypeId;
        }

        private int InsertOrder(){
            var dataSourceConnection = new DataSourceConnection(_config);
            var Id = 0;
            int offerTypeId = 0;
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    //string sql = $"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, templateId, Amount, SizeAllocationId, LabelSizeId, AllocatorUserId, StatusId) OUTPUT INSERTED.ID values ('{storeOffer.BuyersOfferId}', '{storeOffer.StoreID}', '{storeOffer.TemplateId}', '{storeOffer.Amount}', '1', '1', '1','{storeOffer.StatusId}')";
                    string sql = @$"INSERT INTO dbo.Orders (UserID, Statusid) 
                        OUTPUT INSERTED.OrderID 
                        values ('1', '1002')";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            //command.ExecuteNonQuery();
                            int id = (int)command.ExecuteScalar();
                            offerTypeId = id;
                        }                   
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

             return offerTypeId;
        }

        private int InsertRequestOrder(){
            var dataSourceConnection = new DataSourceConnection(_config);
            var Id = 0;
            int offerTypeId = 0;
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    //string sql = $"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, templateId, Amount, SizeAllocationId, LabelSizeId, AllocatorUserId, StatusId) OUTPUT INSERTED.ID values ('{storeOffer.BuyersOfferId}', '{storeOffer.StoreID}', '{storeOffer.TemplateId}', '{storeOffer.Amount}', '1', '1', '1','{storeOffer.StatusId}')";
                    string sql = @$"insert into RequestOrders (Statusid)
                    OUTPUT INSERTED.RequestOrderID 
                     values ('1008')";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            //command.ExecuteNonQuery();
                            int id = (int)command.ExecuteScalar();
                            offerTypeId = id;
                        }                   
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

             return offerTypeId;
        }

        public List<StoreRequestOrder> GetActiveStoreRequestOrdersByBand(string bandLetter){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreRequestOrder>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select distinct RequestOrderID, a.OrderNumber from StoreRequests b
                                    inner join RequestOrders a on a.RequestOrderID = b.StoreRequestOrderId
                                    inner join BuyerOffer c on c.BuyersOfferID = b.BuyersOfferID
                                    where c.PrBand = '{bandLetter}' and a.Statusid = '1008'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new StoreRequestOrder();
                                store.RequestOrderID = Int32.Parse(reader["RequestOrderID"].ToString());
                                store.OrderNumber = reader["OrderNumber"].ToString();
                                
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        //Tutorials
        public List<Tutorial> GetTutorials(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Tutorial>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select * from Tutorials";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new Tutorial();
                                store.TutorialId = Int32.Parse(reader["tutorialId"].ToString());
                                store.TutorialName = reader["tutorialName"].ToString();
                                store.Folder = reader["Folder"].ToString();
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<TutorialType> GetTutorialTypes(int parentId){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<TutorialType>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"select * from TutorialType where parentId = {parentId}";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new TutorialType();
                                store.TutorialId = Int32.Parse(reader["tutorialId"].ToString());
                                store.TutorialName = reader["tutorialName"].ToString();
                                store.Folder = reader["folder"].ToString();
                                store.ParentId = Int32.Parse(reader["parentId"].ToString());
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }
        

    }
}