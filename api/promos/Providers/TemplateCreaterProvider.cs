using Microsoft.Extensions.Configuration;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using System.IO;
using iText.Html2pdf;
using iText.Kernel.Geom;
using iText.Layout.Borders;
using System.Net;
using iText.IO.Image;
using iText.Kernel.Font;
using iText.IO.Font;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using iText.Kernel.Utils;
using iText.Kernel.Colors;
using iText.IO.Font.Constants;
using iText.Kernel.Pdf.Canvas;
using iText.Layout.Renderer;
using iText.Layout.Layout;
using Newtonsoft.Json;
using System.Drawing;

namespace promos.Controllers.Providers
{
    public class TemplateCreaterProvider : ITemplateCreater
    {
        private readonly IConfiguration _config;
        DataSourceConnection dataSourceConnection;

        public TemplateCreaterProvider(IConfiguration config)
        {
            _config = config;
            dataSourceConnection = new DataSourceConnection(_config);
        }

        private void CreatePdf(List<TemplateProperties> templateProperties, int amount, string templateId, string background, string folder, string productCode)
        {
            var fileName = productCode + templateId.ToString() + ".pdf";
            var folderName = System.IO.Path.Combine("StaticFiles", "Resources", "Images");
            var pathToSave = System.IO.Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = System.IO.Path.Combine(folder, fileName);

            PdfWriter writer = new PdfWriter(fullPath);
            PdfDocument pdf = new PdfDocument(writer);

            pdf.SetDefaultPageSize(PageSize.A4);

            Document document = new Document(pdf);
            document.SetMargins(0, 0, 0, 0);

            if (background != "null")
            {
                var imageUrl = "C:\\Users\\hicks\\work\\promos\\api\\promos\\StaticFiles\\Resources\\Images\\" + background;
                Image img = new Image(ImageDataFactory.Create(imageUrl))
                .SetHeight(PageSize.A4.GetHeight())
                .SetWidth(PageSize.A4.GetWidth())
                .SetFixedPosition(0, 0);
                document.Add(img);
            }

            foreach (var t in templateProperties)
            {
                document.Add(CreateParagraph(t));
            }

            if (amount > 1)
            {
                for (var i = 1; i < amount; i++)
                {
                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                    if (background != "null")
                    {
                        var imageUrl = "C:\\Users\\hicks\\work\\promos\\api\\promos\\StaticFiles\\Resources\\Images\\" + background;
                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                        .SetHeight(PageSize.A4.GetHeight())
                        .SetWidth(PageSize.A4.GetWidth())
                        .SetFixedPosition(0, 0);
                        document.Add(img);
                    }

                    foreach (var t in templateProperties)
                    {
                        document = resizeText(document, t, "A4");
                    }
                }
            }

            document.Close();
        }

        private void CreatePdfNew(List<Order> orders)
        {
            var ordersByStore = orders
            .GroupBy(item => item.StoreName)
            .ToDictionary(grp => grp.Key, grp => grp.ToList());
        }

        private Paragraph CreateParagraph(TemplateProperties templateProperty)
        {
            String REGULAR = "";
            var fontStyle = templateProperty.fontstyle;

            if (!string.IsNullOrEmpty(templateProperty.fontstyle))
            {
                if (templateProperty.fontstyle.Trim() == "Stylus ITC Bold")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\STYLIB.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "Avenir Black Oblique")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\blackopique.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "MorrisonsAgenda")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\morrisonsagenda.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "MorrisonsAgendaLight")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\morrisonsagendalight.TTF";
                } else if (templateProperty.fontstyle.Trim() == "FSIndustrieCd-Medium")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\FSIndustrieCd-Medium.ttf";
                }
            }

            PdfFont font;

            if (REGULAR != "")
            {
                FontProgram fontProgram = FontProgramFactory.CreateFont(REGULAR);
                font = PdfFontFactory.CreateFont(fontProgram, PdfEncodings.WINANSI, true);
            }
            else
            {
                font = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN);
            }

            var finalTopPosition = PageSize.A4.GetHeight() - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72);

            Paragraph property = new Paragraph(templateProperty.textProperty)
            .SetFixedPosition(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72), finalTopPosition, 0)
            //.SetTextAlignment(TextAlignment.CENTER)
            //.SetVerticalAlignment(VerticalAlignment.MIDDLE)
            .SetFont(font)
            .SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72))
            //.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72))
            //.SetBorder(new SolidBorder(1))
            //.SetFontSize(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.fontSize), 72))
            .SetCharacterSpacing(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.spacing), 72));

            return property;
        }

        private float ConvvertStringPixelsToInt(string pixels)
        {
            return float.Parse(pixels.Replace("px", ""));
        }

        private Document resizeText(Document pdfDocument, TemplateProperties templateProperty, string templateSize)
        {
            //Fonts
            String REGULAR = "";
            var fontStyle = templateProperty.fontstyle;
            //FSIndustrieCd-Medium
            if (!string.IsNullOrEmpty(templateProperty.fontstyle))
            {
                if (templateProperty.fontstyle.Trim() == "Stylus ITC Bold")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\STYLIB.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "Avenir Black Oblique")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\blackopique.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "FSIndustrieCd-Medium")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\FSIndustrieCd-Medium.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "MorrisonsAgenda")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\morrisonsagenda.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "MorrisonsAgendaLight")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\morrisonsagendalight.TTF";
                }
            }

            PdfFont font;

            if (REGULAR != "")
            {
                FontProgram fontProgram = FontProgramFactory.CreateFont(REGULAR);
                font = PdfFontFactory.CreateFont(fontProgram, PdfEncodings.WINANSI, true);
            }
            else
            {
                font = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN);
            }

            var text = templateProperty.textProperty;
            if (text == "Placeholder")
            {
                text = "";

                if (templateProperty.TextBefore.Trim() != "Placeholder")
                {
                    text = templateProperty.TextBefore + text;
                }
                else
                {

                }

                 if(templateProperty.FieldName == "PromEndRetDate" || templateProperty.FieldName == "PromStartRetDate"){
                     text = SetDateDisplay(templateProperty, text);
                }else{
                    text = text + templateProperty.FieldValue;
                }

                if (templateProperty.TextAfter.Trim() != "Placeholder")
                {
                    text = text + templateProperty.TextAfter;
                }
                else
                {

                }
            }

            var textAfter = "";
            var MoneyText = "";
            if (templateProperty.FieldName == "PromRetail" || templateProperty.FieldName == "CurrentCost" || templateProperty.FieldName == "CurrentRetail" || templateProperty.FieldName == "PromCost" || templateProperty.FieldName == "Savings" || templateProperty.FieldName == "MultibuyPrice")
            {
                if (text != "")
                {
                     string[] textList = {};
                    if(text.Contains(",")){
                      textList = text.Split(",");
                    }else if(text.Contains(".")){
                      textList = text.Split(".");
                    }else{
                        MoneyText = text;
                    }

                    if(textList.Any()){
                        MoneyText = textList[0];
                        textAfter = textList[1];
                    } 
                }
            }
            //var font = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN);
            Text lineTxt = new Text(text);


           Console.WriteLine("########################  #####################################3");
            Console.WriteLine("########################  #####################################3");
             Console.WriteLine("######################## TOP PROPERTY #####################################3");
            Console.WriteLine(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72));

            var finalTopPosition = 0f;
            var finalLeftPosition = 0f;
            if(templateSize == "A4"){
             finalTopPosition = PageSize.A4.GetHeight() - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72);
             finalLeftPosition = 0f;
            }else if(templateSize == "A4 Iceland + Express"){
             finalTopPosition = PageSize.A4.GetHeight() - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72);
             finalLeftPosition = 0f;
            }else if(templateSize == "SRA4"){
             finalTopPosition = 907.0866f - 32.5f - 8.5f - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72);
             finalLeftPosition = 0f;
            }

            var tLeft = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72);
            var tWidth = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72);
            if(templateSize == "A4 Iceland + Express"){
                if(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72) < 18f){
                    tLeft = 18f; 
                }
                
                if(tLeft + PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72) > PageSize.A4.GetWidth() - 18f){
                    tWidth = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72) - 18f;
                }

                if(finalTopPosition < 14.17f){
                    finalTopPosition = finalTopPosition + 14.17f;
                }else if(finalTopPosition > 14.17f){
                    finalTopPosition = finalTopPosition - 14.17f;
                }
            }
            
            iText.Kernel.Geom.Rectangle lineTxtRect = new iText.Kernel.Geom.Rectangle(tLeft + finalLeftPosition, finalTopPosition, tWidth, PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));

            Div lineDiv = new Div();

            if(templateProperty.VerticalAlign == "Top"){
                lineDiv.SetVerticalAlignment(VerticalAlignment.TOP);
            }else if(templateProperty.VerticalAlign == "Center"){
                lineDiv.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            }else if(templateProperty.VerticalAlign == "Bottom"){
                lineDiv.SetVerticalAlignment(VerticalAlignment.BOTTOM);
            }
            
            
            //lineDiv.SetBorder(new DashedBorder(1));

            var listInt = GenerateRgba(templateProperty.color);


            iText.Kernel.Colors.Color myColor = new DeviceRgb(listInt[0], listInt[1], listInt[2]);

            Paragraph linePara = new Paragraph().Add(lineTxt)
                .SetMultipliedLeading(0.7f)
                .SetFont(font)
                .SetFontColor(myColor)
                .SetMargin(0)
                .SetPadding(0);

            if (templateProperty.Rotate == 1)
            {
                linePara.SetRotationAngle(145);
            }

            if (templateProperty.TextAlign == "center")
            {
                linePara.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
            }
            else if (templateProperty.TextAlign == "left")
            {
                linePara.SetTextAlignment(iText.Layout.Properties.TextAlignment.LEFT);
            }
            else if (templateProperty.TextAlign == "right")
            {
                linePara.SetTextAlignment(iText.Layout.Properties.TextAlignment.RIGHT);
            }

            lineDiv.Add(linePara);


            float fontSizeL = 1; // 1 is the font size that is definitely small enough to draw all the text 
            float fontSizeR = 600; // 20 is the maximum value of the font size you want to use

            Canvas canvas = new Canvas(new PdfCanvas(pdfDocument.GetPdfDocument().GetPage(pdfDocument.GetPdfDocument().GetNumberOfPages())), lineTxtRect);

            var contextTop = 0f;
            var contextLeft = 0f;
            var contextRight = 0f;
            var contextBottom = 0f;
            var contextHeight = 0f;
            var contextWidth = 0f;


            // Binary search on the font size
            while (Math.Abs(fontSizeL - fontSizeR) > 1e-1)
            {
                float curFontSize = (fontSizeL + fontSizeR) / 2;
                lineDiv.SetFontSize(curFontSize);
                //lineDiv.SetLineThrough();

                // It is important to set parent for the current element renderer to a root renderer
                IRenderer renderer = lineDiv.CreateRendererSubTree().SetParent(canvas.GetRenderer());
                LayoutContext context = new LayoutContext(new LayoutArea(1, lineTxtRect));

                if (renderer.Layout(context).GetStatus() == LayoutResult.FULL)
                {
                    // we can fit all the text with curFontSize
                    fontSizeL = curFontSize;
                    contextLeft = renderer.GetOccupiedArea().GetBBox().GetLeft();
                    contextTop = renderer.GetOccupiedArea().GetBBox().GetTop();
                    contextRight = renderer.GetOccupiedArea().GetBBox().GetRight();
                    contextBottom = renderer.GetOccupiedArea().GetBBox().GetBottom();
                    contextHeight = renderer.GetOccupiedArea().GetBBox().GetHeight();
                    contextWidth = renderer.GetOccupiedArea().GetBBox().GetWidth();
                }
                else
                {
                    fontSizeR = curFontSize;
                }
            }

            // Use the biggest font size that is still small enough to fit all the text
            lineDiv.SetFontSize(fontSizeL);
            lineDiv.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
            lineDiv.SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72));


            // pdfDocument.Add(property);
            // CreateParagraph(tem);
            //lineDiv.SetPaddingTop(20f);
            //lineDiv.SetHeight(ConvvertStringPixelsToInt(templateProperty.height));

            var leftPosition = contextLeft;
            if (templateProperty.FieldName == "PromRetail" || templateProperty.FieldName == "CurrentCost" || templateProperty.FieldName == "CurrentRetail" || templateProperty.FieldName == "PromCost" || templateProperty.FieldName == "Savings" || templateProperty.FieldName == "MultibuyPrice")
            {         

                var symbol = "";
                var afterText = "";

                if(MoneyText != "" && MoneyText != "0"){
                    symbol = "£";
                    if(textAfter != ""){
                        afterText = "." + textAfter;
                    }
                }else{
                    symbol = "";
                    MoneyText = $"{textAfter}";
                    //MoneyText = $"800";
                    afterText = "p";
                }

                Console.WriteLine("TEXT WITH P");
                  Console.WriteLine(MoneyText);
                Paragraph p = new Paragraph();
                if(templateProperty.VerticalAlign == "Top"){
                p.SetVerticalAlignment(VerticalAlignment.TOP);
                }else if(templateProperty.VerticalAlign == "Center"){
                    p.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                }else if(templateProperty.VerticalAlign == "Bottom"){
                    p.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                }
                 if (templateProperty.Rotate == 1)
                {
                    p.SetRotationAngle(145);
                }
                p.SetVerticalAlignment(VerticalAlignment.TOP);
                p.SetMargin(0);
                p.SetPadding(0);
                p.Add(symbol);
                p.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
               //p.SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72));
                p.SetFontSize(fontSizeL / 2);
                p.SetMultipliedLeading(0.5f);
               
               // p.SetBorder(new SolidBorder(1));
                p.SetFont(font);
                p.SetFontColor(myColor);

                Paragraph mainText = new Paragraph();
                if(templateProperty.VerticalAlign == "Top"){
                mainText.SetVerticalAlignment(VerticalAlignment.TOP);
                }else if(templateProperty.VerticalAlign == "Center"){
                    mainText.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                }else if(templateProperty.VerticalAlign == "Bottom"){
                    mainText.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                }
                 if (templateProperty.Rotate == 1)
                {
                    mainText.SetRotationAngle(145);
                }
                mainText.SetVerticalAlignment(VerticalAlignment.TOP);
                mainText.SetMargin(0);
                mainText.SetPadding(0);
                mainText.Add(MoneyText);
                mainText.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
               //p.SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72));
                mainText.SetFontSize(fontSizeL);
                mainText.SetMultipliedLeading(0.5f);
                //mainText.SetBorder(new SolidBorder(1));
                mainText.SetFont(font);
                mainText.SetFontColor(myColor);


                Paragraph smallText = new Paragraph();
                if(templateProperty.VerticalAlign == "Top"){
                smallText.SetVerticalAlignment(VerticalAlignment.TOP);
                }else if(templateProperty.VerticalAlign == "Center"){
                    smallText.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                }else if(templateProperty.VerticalAlign == "Bottom"){
                    smallText.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                }
                 if (templateProperty.Rotate == 1)
                {
                    smallText.SetRotationAngle(145);
                }
                smallText.SetVerticalAlignment(VerticalAlignment.TOP);
                smallText.SetMargin(0);
                smallText.SetPadding(0);
                smallText.Add(afterText);
                smallText.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
               //p.SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72));
                smallText.SetFontSize(fontSizeL / 2);
                smallText.SetMultipliedLeading(0.5f);
               
                //smallText.SetBorder(new SolidBorder(1));
                smallText.SetFont(font);
                smallText.SetFontColor(myColor);
               


                var width = getRealParagraphWidth(pdfDocument, p);
                var width3 = getRealParagraphWidth(pdfDocument, mainText);
                var width2 = getRealParagraphWidth(pdfDocument, smallText);
                var totalWidthMoney = width + width2 + width3;
                var realWidth = totalWidthMoney;
                var difffromoriginal = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72) - realWidth;
                var finalLeft = 0f;
                if(difffromoriginal > 0){
                    Console.WriteLine("--------------- Calculating width objects ------------------");
                    Console.WriteLine("--------------- current Left Position ------------------");
                    Console.WriteLine(leftPosition);

                      Console.WriteLine("--------------- Real Width ------------------");
                    Console.WriteLine(realWidth);

                     Console.WriteLine("--------------- Context Width ------------------");
                    Console.WriteLine(contextWidth);

                    Console.WriteLine("--------------- Container width ------------------");
                    Console.WriteLine( ( PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72)));

                    var perc =  (( PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72)) / realWidth);
                     Console.WriteLine("--------------- perc ------------------");
                    Console.WriteLine(perc);

                    var diff = (realWidth * perc) - realWidth;
                    Console.WriteLine("--------------- Diff ------------------");
                    Console.WriteLine(diff);
                    
                    finalLeft = leftPosition + (diff / 2);
                    Console.WriteLine("--------------- Final left ------------------");
                    Console.WriteLine(finalLeft);
                }


                p.SetFixedPosition((finalLeft), finalTopPosition, 150);
                mainText.SetFixedPosition((finalLeft + (width - 2)), finalTopPosition, 150);
                if(afterText == "p" ){
                smallText.SetFixedPosition((finalLeft + width + width3 - 4), finalTopPosition - (fontSizeL / 7), 150);
                }else{
                    smallText.SetFixedPosition((finalLeft + width + width3 - 4), finalTopPosition, 150);
                }

                 pdfDocument.Add(p);
                pdfDocument.Add(smallText);
                pdfDocument.Add(mainText);

                 var heightDiffstroke = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - contextHeight;
           Console.WriteLine("MONEY TEXT");
           Console.WriteLine(MoneyText);
        //     canvas.GetPdfCanvas().Stroke().SetFontAndSize(font, 0);
        //    canvas.GetPdfCanvas().Stroke().SetStrokeColor(myColor);
            if (templateProperty.Strikethrough == 1 && MoneyText != "p" && MoneyText != " ")
            {
                
                if (templateProperty.Rotate == 1)
                {
                    canvas.GetPdfCanvas().SetStrokeColor(myColor); 
                    canvas.GetPdfCanvas().MoveTo(finalLeft + (contextLeft * 0.1),  (contextTop - ((fontSizeL / 2) + 3)));
                    canvas.GetPdfCanvas().LineTo(((finalLeft) + width2 + width3) - (contextRight * 0.1), contextTop);
                    canvas.GetPdfCanvas().Stroke();
                }
                else
                {
                    canvas.GetPdfCanvas().SetStrokeColor(myColor); 
                    canvas.GetPdfCanvas().MoveTo(finalLeft, (contextTop - ((fontSizeL / 2) + 3)));
                    canvas.GetPdfCanvas().LineTo((finalLeft) + width2 + width3, (contextTop));
                    canvas.GetPdfCanvas().Stroke(); 
                }
            }
            }else{
            //      var heightDiffstroke = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - contextHeight;
            // if (templateProperty.Strikethrough == 1 && text != "" && text != "Placeholder" && text != "£" && MoneyText != "")
            // {
                
            //     if (templateProperty.Rotate == 1)
            //     {
            //         canvas.GetPdfCanvas().MoveTo(contextLeft + (contextLeft * 0.1),  (contextBottom - (heightDiffstroke / 2)));
            //         canvas.GetPdfCanvas().LineTo(contextRight - (contextRight * 0.1), (contextTop - (heightDiffstroke / 2)));
            //     }
            //     else
            //     {
            //         canvas.GetPdfCanvas().MoveTo(contextLeft, (contextBottom - (heightDiffstroke / 2)));
            //         canvas.GetPdfCanvas().LineTo(contextRight, (contextTop - (heightDiffstroke / 2)));
            //     }

            //     canvas.GetPdfCanvas().Stroke().SetFontAndSize(font, 0);
            //     canvas.GetPdfCanvas().Stroke().SetStrokeColor(myColor);
            // }
                canvas.Add(lineDiv);
            }

            //canvas.Add(property);

            return pdfDocument;
        }


        public Document onGenericTag(Document doc, iText.Kernel.Geom.Rectangle rect, String tag)
        {
            Canvas canvas = new Canvas(new PdfCanvas(doc.GetPdfDocument().GetPage(doc.GetPdfDocument().GetNumberOfPages())), rect);

            //canvas.setColorStroke(iText.Kernel.Colors.Color.); // or whatever
            // You can also mess with the line's thickness, endcaps, dash style, etc.
            // Lots of options to play with.
            canvas.GetPdfCanvas().MoveTo(rect.GetLeft(), rect.GetBottom());
            canvas.GetPdfCanvas().LineTo(rect.GetRight(), rect.GetTop());
            canvas.GetPdfCanvas().Stroke();
            canvas.GetPdfCanvas().RestoreState().MoveTo(rect.GetLeft(), rect.GetBottom());
            return doc;
        }

        private string SetDateDisplay(TemplateProperties templateProperty, string text){
            DateTime oDate = Convert.ToDateTime(templateProperty.FieldValue);
                     var fullDay = oDate.ToString("dddd");
                     var fullMonth = oDate.ToString("MMMM");
                     var monthdate = oDate.ToString("dd");

                    if(templateProperty.DateFormat == 0){
                        var displayDate = oDate.ToString("dd/MM/yyyy");
                    text = text + displayDate;
                    }
                    else if(templateProperty.DateFormat == 1){
                    var displayDate = oDate.ToString("ddd, dd MMM yyy");
                    text = text + displayDate;
                    }else if(templateProperty.DateFormat == 2){
                    var displayDate = oDate.ToString("dddd, dd MMMM yyy");
                    text = text + displayDate;
                    }else if(templateProperty.DateFormat == 3){
                        //var displayDate = oDate.ToString("dddd dd MMMM");
                    var displayDate = fullDay + " " + monthdate + this.GetDateExtraText(monthdate) + " " + fullMonth;
                    text = text + displayDate;
                    }else if(templateProperty.DateFormat == 4){
                    var displayDate = oDate.ToString("dddd dd MMMM");
                    text = text + displayDate;
                    }else if(templateProperty.DateFormat == 5){
                    var displayDate = oDate.ToString("dddd dd MMMM yyy");
                    text = text + displayDate;
                    }else{
                        text = text + templateProperty.FieldValue;
                    }
            return text;
        }

        private string GetDateExtraText(string day){
            var x = Int32.Parse(day);
            switch(x){
                case 1:
                case 21: 
                case 31: 
                return "st";
                case 2: 
                case 22: 
                return "nd";
                case 3: 
                case 23: 
                return "rd";
                default:
                return "th";
            }
        }
        
        private Document resizeTextSmallBarker(Document pdfDocument, TemplateProperties templateProperty, float topPosition, float leftPosition, string size = null)
        {
            
            //Fonts
            String REGULAR = "";
            var fontStyle = templateProperty.fontstyle;
//FSIndustrieCd-Medium
            if (!string.IsNullOrEmpty(templateProperty.fontstyle))
            {
                if (templateProperty.fontstyle.Trim() == "Stylus ITC Bold")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\STYLIB.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "Avenir Black Oblique")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\blackopique.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "FSIndustrieCd-Medium")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\FSIndustrieCd-Medium.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "MorrisonsAgenda")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\morrisonsagenda.ttf";
                }
                else if (templateProperty.fontstyle.Trim() == "MorrisonsAgendaLight")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\morrisonsagendalight.TTF";
                } else if (templateProperty.fontstyle.Trim() == "FSIndustrieCd-Medium")
                {
                    REGULAR = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\fonts\\FSIndustrieCd-Medium.ttf";
                }
            }


            PdfFont font;

            if (REGULAR != "")
            {
                FontProgram fontProgram = FontProgramFactory.CreateFont(REGULAR);
                font = PdfFontFactory.CreateFont(fontProgram, PdfEncodings.WINANSI, true);
            }
            else
            {
                font = PdfFontFactory.CreateFont(StandardFonts.TIMES_ROMAN, true);
            }


           var text = templateProperty.textProperty;
            if (text == "Placeholder")
            {
                text = "";

                if (templateProperty.TextBefore.Trim() != "Placeholder")
                {
                    text = templateProperty.TextBefore + text;
                }
                else
                {

                }

                if(templateProperty.FieldName == "PromEndRetDate" || templateProperty.FieldName == "PromStartRetDate"){
                     text = SetDateDisplay(templateProperty, text);
                }else{
                    text = text + templateProperty.FieldValue;
                }

                if (templateProperty.TextAfter.Trim() != "Placeholder")
                {
                    text = text + templateProperty.TextAfter;
                }
                else
                {

                }
            }

            var textAfter = "";
            var MoneyText = "";
            if (templateProperty.FieldName == "PromRetail" || templateProperty.FieldName == "CurrentCost"  || templateProperty.FieldName == "CurrentRetail" || templateProperty.FieldName == "PromCost" || templateProperty.FieldName == "Savings" || templateProperty.FieldName == "MultibuyPrice")
            {
                if (text != "")
                {
                     string[] textList = {};
                    if(text.Contains(",")){
                      textList = text.Split(",");
                    }else if(text.Contains(".")){
                      textList = text.Split(".");
                    }else{
                        MoneyText = text;
                    }

                    if(textList.Any()){
                        MoneyText = textList[0];
                        textAfter = textList[1];
                    } 
                }
            }

            Text lineTxt = new Text(text);

            // Console.WriteLine("############# TOP POSITION ######################");
            // Console.WriteLine(topPosition);
            float finalTopPosition;
            if(string.IsNullOrEmpty(size)){
                 finalTopPosition = (PageSize.A4.GetHeight() - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72)) - topPosition;
            }else if(size == "SEL"){
                 finalTopPosition = (637.795f - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72)) - topPosition;
            }else if(size == "LB-SRA4"){
                finalTopPosition = (637.795f - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72)) - topPosition;
            }else if(size == "LB"){
                finalTopPosition = (PageSize.A4.GetWidth() - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72)) - topPosition;
            }else{
                finalTopPosition = (PageSize.A4.GetHeight() - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72)) - topPosition;
            }


             var tLeft = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72);
            var tWidth = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72);
            if(size == "LB"){
                if(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72) < (PageSize.A4.GetWidth() / 20)){
                    tLeft = tLeft + 18f; 
                }else{
                    tLeft = tLeft - 18f;
                }

                if(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.topProperty), 72) > (PageSize.A4.GetWidth() / 2) / 2){
                    finalTopPosition = finalTopPosition + 18f;
                }else{
                    finalTopPosition = finalTopPosition - 18f;
                }
            }
            
            //Set Fimal position, add padding if on edges
            iText.Kernel.Geom.Rectangle lineTxtRect = new iText.Kernel.Geom.Rectangle(leftPosition + tLeft, finalTopPosition, tWidth, PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
            
            Div lineDiv = new Div();

             lineDiv.SetMargin(0);
             lineDiv.SetPadding(0);

            var listInt = GenerateRgba(templateProperty.color);
            iText.Kernel.Colors.Color myColor = new DeviceRgb(listInt[0], listInt[1], listInt[2]);
            // new DeviceRgb(255, 100, 20);
            Paragraph linePara = new Paragraph().Add(lineTxt)
                .SetFont(font)
                .SetFontColor(myColor)
                .SetMultipliedLeading(0.7f)
                .SetMargin(0)
                .SetPadding(0);

            if (templateProperty.Rotate == 1)
            {
                linePara.SetRotationAngle(145);
            }


            if (templateProperty.TextAlign == "center")
            {
                linePara.SetTextAlignment(iText.Layout.Properties.TextAlignment.CENTER);
            }
            else if (templateProperty.TextAlign == "left")
            {
                linePara.SetTextAlignment(iText.Layout.Properties.TextAlignment.LEFT);
            }
            else if (templateProperty.TextAlign == "right")
            {
                linePara.SetTextAlignment(iText.Layout.Properties.TextAlignment.RIGHT);
            }
            lineDiv.Add(linePara);
            //lineDiv.SetBorder(new SolidBorder(1));

            float fontSizeL = 5; // 1 is the font size that is definitely small enough to draw all the text 
            float fontSizeR = 600; // 20 is the maximum value of the font size you want to use

            Canvas canvas = new Canvas(new PdfCanvas(pdfDocument.GetPdfDocument().GetPage(pdfDocument.GetPdfDocument().GetNumberOfPages())), lineTxtRect);

            var contextTop = 0f;
            var contextLeft = 0f;
            var contextRight = 0f;
            var contextBottom = 0f;
            var contextHeight = 0f;

            // Binary search on the font size
            while (Math.Abs(fontSizeL - fontSizeR) > 1e-1)
            {
                float curFontSize = (fontSizeL + fontSizeR) / 2;
                lineDiv.SetFontSize(curFontSize);
                // It is important to set parent for the current element renderer to a root renderer
                IRenderer renderer = lineDiv.CreateRendererSubTree().SetParent(canvas.GetRenderer());
                LayoutContext context = new LayoutContext(new LayoutArea(1, lineTxtRect));
                if (renderer.Layout(context).GetStatus() == LayoutResult.FULL)
                {
                    contextLeft = renderer.GetOccupiedArea().GetBBox().GetLeft();
                    contextTop = renderer.GetOccupiedArea().GetBBox().GetTop();
                    contextRight = renderer.GetOccupiedArea().GetBBox().GetRight();
                    contextBottom = renderer.GetOccupiedArea().GetBBox().GetBottom();
                    contextHeight = renderer.GetOccupiedArea().GetBBox().GetHeight();
                    // we can fit all the text with curFontSize
                    fontSizeL = curFontSize;
                }
                else
                {
                    fontSizeR = curFontSize;
                }
            }

            // Use the biggest font size that is still small enough to fit all the text
            lineDiv.SetFontSize(fontSizeL);
           

           

            
            // Console.WriteLine("###################### HEIGHT DIF #####################");
            // Console.WriteLine(heightDiff);
            lineDiv.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
            //lineDiv.SetWidth(getRealParagraphWidth());

            if(templateProperty.VerticalAlign == "Top"){
                lineDiv.SetVerticalAlignment(VerticalAlignment.TOP);
            }else if(templateProperty.VerticalAlign == "Center"){
                lineDiv.SetVerticalAlignment(VerticalAlignment.MIDDLE);
            }else if(templateProperty.VerticalAlign == "Bottom"){
                lineDiv.SetVerticalAlignment(VerticalAlignment.BOTTOM);
            }
            // lineDiv.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
            // lineDiv.SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72));
            // lineDiv.SetHeight(linePara.GetHeight());
           


         if (templateProperty.FieldName == "PromRetail" || templateProperty.FieldName == "CurrentCost" || templateProperty.FieldName == "CurrentRetail" || templateProperty.FieldName == "PromCost" || templateProperty.FieldName == "Savings" || templateProperty.FieldName == "MultibuyPrice")
            {         

                var symbol = "";
                var afterText = "";

                if(MoneyText != "" && MoneyText != "0"){
                    symbol = "£";
                    if(textAfter != ""){
                        afterText = "." + textAfter;
                    }
                }else{
                    symbol = "";
                    MoneyText = textAfter;
                    afterText = "p";
                }

                Paragraph p = new Paragraph();
                if(templateProperty.VerticalAlign == "Top"){
                p.SetVerticalAlignment(VerticalAlignment.TOP);
                }else if(templateProperty.VerticalAlign == "Center"){
                    p.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                }else if(templateProperty.VerticalAlign == "Bottom"){
                    p.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                }
                 if (templateProperty.Rotate == 1)
                {
                    p.SetRotationAngle(145);
                }
                p.SetVerticalAlignment(VerticalAlignment.TOP);
                p.SetMargin(0);
                p.SetPadding(0);
                p.Add(symbol);
                p.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
               //p.SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72));
                p.SetFontSize(fontSizeL / 2);
                p.SetMultipliedLeading(0.5f);
               
                p.SetFont(font);
                p.SetFontColor(myColor);

                Paragraph mainText = new Paragraph();
                if(templateProperty.VerticalAlign == "Top"){
                mainText.SetVerticalAlignment(VerticalAlignment.TOP);
                }else if(templateProperty.VerticalAlign == "Center"){
                    mainText.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                }else if(templateProperty.VerticalAlign == "Bottom"){
                    mainText.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                }
                 if (templateProperty.Rotate == 1)
                {
                    mainText.SetRotationAngle(145);
                }
                mainText.SetVerticalAlignment(VerticalAlignment.TOP);
                mainText.SetMargin(0);
                mainText.SetPadding(0);
                mainText.Add(MoneyText);
                mainText.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
               //p.SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72));
                mainText.SetFontSize(fontSizeL);
                mainText.SetMultipliedLeading(0.5f);
               
                mainText.SetFont(font);
                mainText.SetFontColor(myColor);


                Paragraph smallText = new Paragraph();
                if(templateProperty.VerticalAlign == "Top"){
                smallText.SetVerticalAlignment(VerticalAlignment.TOP);
                }else if(templateProperty.VerticalAlign == "Center"){
                    smallText.SetVerticalAlignment(VerticalAlignment.MIDDLE);
                }else if(templateProperty.VerticalAlign == "Bottom"){
                    smallText.SetVerticalAlignment(VerticalAlignment.BOTTOM);
                }
                 if (templateProperty.Rotate == 1)
                {
                    smallText.SetRotationAngle(145);
                }
                smallText.SetVerticalAlignment(VerticalAlignment.TOP);
                smallText.SetMargin(0);
                smallText.SetPadding(0);
                smallText.Add(afterText);
                smallText.SetHeight(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72));
               //p.SetWidth(PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72));
                smallText.SetFontSize(fontSizeL / 2);
                smallText.SetMultipliedLeading(0.5f);
               
                smallText.SetFont(font);
                smallText.SetFontColor(myColor);



                 var width = getRealParagraphWidth(pdfDocument, p);
                var width3 = getRealParagraphWidth(pdfDocument, mainText);
                var width2 = getRealParagraphWidth(pdfDocument, smallText);
                var totalWidthMoney = width + width2 + width3;
                var realWidth = totalWidthMoney;
                var difffromoriginal = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72) - realWidth;
                var finalLeft = 0f;
                if(difffromoriginal > 0){
                    Console.WriteLine("--------------- Calculating width objects ------------------");
                    Console.WriteLine("--------------- current Left Position ------------------");
                    Console.WriteLine(leftPosition);

                      Console.WriteLine("--------------- Real Width ------------------");
                    Console.WriteLine(realWidth);


                    Console.WriteLine("--------------- Container width ------------------");
                    Console.WriteLine( ( PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72)));

                    var perc =  (( PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.width), 72)) / realWidth);
                     Console.WriteLine("--------------- perc ------------------");
                    Console.WriteLine(perc);

                    var diff = (realWidth * perc) - realWidth;
                    Console.WriteLine("--------------- Diff ------------------");
                    Console.WriteLine(diff);
                    
                    finalLeft = leftPosition + (diff / 2);
                    Console.WriteLine("--------------- Final left ------------------");
                    Console.WriteLine(finalLeft);
                }
              
              
                //var heightDiff = ((PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72)) - fontSizeL);
                p.SetFixedPosition((leftPosition + (PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72))), finalTopPosition, width);
               // smallText.SetFixedPosition((leftPosition + (PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72)) + width + width3 - 4), finalTopPosition, 150);
                mainText.SetFixedPosition((leftPosition + (PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72)) + (width - 2)), finalTopPosition, width3);
             if(afterText == "p" ){
               // smallText.SetFixedPosition((finalLeft + width + width3 - 4), finalTopPosition, 150);
                smallText.SetFixedPosition((leftPosition + (PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72)) + width + width3 - 3), finalTopPosition  - (fontSizeL / 7), width2);
                }else{
                smallText.SetFixedPosition((leftPosition + (PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72)) + width + width3 - 3), finalTopPosition, width2);
                }

                pdfDocument.Add(p);
                pdfDocument.Add(smallText);
                pdfDocument.Add(mainText);

                 var heightDiffstroke = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - contextHeight;
        //    Console.WriteLine("Strike Through");
        //    Console.WriteLine(templateProperty.Strikethrough);
            if (templateProperty.Strikethrough == 1 && MoneyText != "p" && MoneyText != " " && MoneyText != "")
            {
                Console.WriteLine("Strike Through");
                Console.WriteLine(templateProperty.Strikethrough);
                if (templateProperty.Rotate == 1)
                {
                    canvas.GetPdfCanvas().SetStrokeColor(myColor); 
                    canvas.GetPdfCanvas().MoveTo(contextLeft + (contextLeft * 0.1),  (contextTop - ((fontSizeL / 2) + 3)));
                    canvas.GetPdfCanvas().LineTo(((finalLeft + (PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72))) + width2 + width3) - (contextRight * 0.1), contextTop);
                    canvas.GetPdfCanvas().Stroke(); 
                }
                else
                {
                    canvas.GetPdfCanvas().SetStrokeColor(myColor); 
                    canvas.GetPdfCanvas().MoveTo(contextLeft, (contextTop - ((fontSizeL / 2) + 3)));
                    canvas.GetPdfCanvas().LineTo((leftPosition + (PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.leftProperty), 72))) + width2 + width3, (contextTop));
                    canvas.GetPdfCanvas().Stroke();
                }
            }
            }else{
            //      var heightDiffstroke = PixelsToPoints(ConvvertStringPixelsToInt(templateProperty.height), 72) - contextHeight;
            // if (templateProperty.Strikethrough == 1 && text != "" && text != "Placeholder" && text != "£" && MoneyText != "")
            // {
                
            //     if (templateProperty.Rotate == 1)
            //     {
            //         canvas.GetPdfCanvas().MoveTo(contextLeft + (contextLeft * 0.1),  (contextBottom - (heightDiffstroke / 2)));
            //         canvas.GetPdfCanvas().LineTo(contextRight - (contextRight * 0.1), (contextTop - (heightDiffstroke / 2)));
            //     }
            //     else
            //     {
            //         canvas.GetPdfCanvas().MoveTo(contextLeft, (contextBottom - (heightDiffstroke / 2)));
            //         canvas.GetPdfCanvas().LineTo(contextRight, (contextTop - (heightDiffstroke / 2)));
            //     }

            //     canvas.GetPdfCanvas().Stroke().SetFontAndSize(font, 0);
            //     canvas.GetPdfCanvas().Stroke().SetStrokeColor(myColor);
            // }
                canvas.Add(lineDiv);
            }
            
            return pdfDocument;
        }

        private float getRealParagraphWidth(Document doc, Paragraph paragraph) {
            // Create renderer tree
            IRenderer paragraphRenderer = paragraph.CreateRendererSubTree();
            // Do not forget setParent(). Set the dimensions of the viewport as needed
            LayoutResult result = paragraphRenderer.SetParent(doc.GetRenderer()).
                    Layout(new LayoutContext(new LayoutArea(1, new iText.Kernel.Geom.Rectangle(1000, 100))));
            // LayoutResult#getOccupiedArea() contains the information you need
            //return result.getOccupiedArea().getBBox().getWidth();
            return ((ParagraphRenderer) paragraphRenderer).GetMinMaxWidth().GetMaxWidth();
 }

        public List<int> GenerateRgba(string backgroundColor)
        {

            var returnList = new List<int>();

            var split = backgroundColor.Replace("rgba(", "");
            var stringList = split.Replace(")", "");
            var listToConvert = stringList.Split(",");
            foreach (var c in listToConvert)
            {
                returnList.Add(Int32.Parse(c));
            }

            return returnList;
        }


        public CreatedModel ProcessOrder(int id, int printerId)
        {

            var orders = GetOrder(id);
            var orderDirectory = "";

            var response = new CreatedModel();

            if (orders.Count > 0)
            {
                //Get the order number
                var orderNumber = orders[0].OrderNumber;
                orderDirectory = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\orders\\" + orderNumber;
                //Create folder
                var created = CreateDirectory(orderDirectory);
                var createStoreDirectories = CreateStoreDirectories(orders, orderDirectory);
                var createStorePrints = CreatePdfByStore(orders, orderDirectory);
                var updateStores = UpdateOrders(orders);
                MoveOrders(orderDirectory, printerId);
                response.url = orderDirectory;
            }

            return response;
        }

        private void MoveOrders(string filepath, int printerId){
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"[dbo].[SendToPrinter]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@FileName", filepath));
                        command.Parameters.Add(new SqlParameter("@PrinterID", printerId));
                        command.ExecuteNonQuery();
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private bool UpdateOrders(List<Order> orders)
        {

            foreach (var order in orders)
            {
                try
                {
                    SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                    builder.DataSource = dataSourceConnection.DataSource;
                    builder.UserID = dataSourceConnection.UserId;
                    builder.Password = dataSourceConnection.Password;
                    builder.InitialCatalog = dataSourceConnection.InitialCatalog;

                    using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                    {
                        connection.Open();
                        //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                        string sql = $"UPDATE Orders SET Statusid = 1003 where OrderID = {order.OrderId}";
                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch (SqlException e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            return true;
        }

        private bool CreateStoreDirectories(List<Order> orders, string directory)
        {
            var ordersByStore = orders
            .GroupBy(item => item.StoreName)
            .ToDictionary(grp => grp.Key, grp => grp.ToList());

            foreach (KeyValuePair<string, List<Order>> entry in ordersByStore)
            {
                var storeDirectory = directory + "\\" + entry.Key;
                CreateDirectory(storeDirectory);
            }
            return true;
        }

        private bool CreatePdfByStore(List<Order> orders, string directory)
        {
            var ordersByStore = orders
            .GroupBy(item => item.StoreName)
            .ToDictionary(grp => grp.Key, grp => grp.ToList());

            var orderByStoreAndSize = new Dictionary<string, Dictionary<string, List<Order>>>();


            //Create Split by size
            foreach (KeyValuePair<string, List<Order>> entry in ordersByStore)
            {
                var ordersBySize = entry.Value
                .GroupBy(item => item.SizeDescription)
                .ToDictionary(grp => grp.Key, grp => grp.ToList());

                orderByStoreAndSize.Add(entry.Key, ordersBySize);
            }

            var totalPos = 0;
            foreach (KeyValuePair<string, List<Order>> entry in ordersByStore)
            {
                foreach (var o in entry.Value)
                {
                    totalPos += o.Amount;
                }
            }

            var fileName = $"{orders[0].OrderNumber}.pdf";
            var fullPath = System.IO.Path.Combine(directory, fileName);

            PdfWriter writer = new PdfWriter(fullPath);
            PdfDocument pdf = new PdfDocument(writer);

            pdf.SetDefaultPageSize(PageSize.A4);

            Document document = new Document(pdf);
            document.SetMargins(0, 0, 0, 0);

            // Paragraph pageHeader = new Paragraph("SandPiper Pos")
            // .SetFontSize(30)
            // .SetWidth(PageSize.A4.GetWidth())
            // .SetTextAlignment(TextAlignment.CENTER);
            // document.Add(pageHeader);


        //     var orderNumberDisplay = $"Order Number: {orders[0].OrderNumber}";
        //     Paragraph ordernemberheader = new Paragraph(orderNumberDisplay)
        //    .SetFontSize(12)
        //    .SetWidth(PageSize.A4.GetWidth())
        //    .SetMarginTop(10)
        //    .SetTextAlignment(TextAlignment.CENTER);
        //     document.Add(ordernemberheader);

            //Number Of Pos Start
        //     var numberOfPrintsDisplay = $"Total Prints: {totalPos.ToString()}";
        //     Paragraph numPosHeader = new Paragraph(numberOfPrintsDisplay)
        //    .SetFontSize(12)
        //    .SetWidth(PageSize.A4.GetWidth())
        //    .SetMarginTop(10)
        //    .SetTextAlignment(TextAlignment.CENTER);
        //     document.Add(numPosHeader);

            //Number Of Pos End

            //Number Of Stores Start
        //     var numStoresDisplay = $"Number Of Stores: {ordersByStore.Count}";
        //     Paragraph numStores = new Paragraph(numStoresDisplay)
        //    .SetFontSize(12)
        //    .SetWidth(PageSize.A4.GetWidth())
        //    .SetMarginTop(10)
        //    .SetTextAlignment(TextAlignment.CENTER);
        //     document.Add(numStores);

            //Number Of Stores End

            //Number Of Stores Start
        //     Paragraph StorValues = new Paragraph("Stores")
        //    .SetFontSize(12)
        //    .SetWidth(PageSize.A4.GetWidth())
        //    .SetMarginTop(10)
        //    .SetTextAlignment(TextAlignment.CENTER);
        //     document.Add(StorValues);

            foreach (KeyValuePair<string, List<Order>> entry in ordersByStore)
            {

                //     var totall = 0;
                //     var store = entry.Key;

                //     for (var i = 0; i < entry.Value.Count; i++)
                //     {
                //         totall += entry.Value[i].Amount;
                //     }

                //     var finalText = $"{store} - Total Prints {totall}";
                 pdf.SetDefaultPageSize(PageSize.A4);
            //     Paragraph storeTotalName = new Paragraph(entry.Key)
            //    .SetFontSize(12)
            //    .SetWidth(PageSize.A4.GetWidth())
            //    .SetMarginTop(10)
            //    .SetTextAlignment(TextAlignment.CENTER);
            //     document.Add(storeTotalName);

                // foreach (KeyValuePair<string, Dictionary<string, List<Order>>> entry2 in orderByStoreAndSize)
                // {
                //     foreach (var order in entry2.Value)
                //     {
                //         var name = order.Key;
                //         var sizeTotal = 0;

                //         foreach (var o in order.Value)
                //         {
                //             sizeTotal += o.Amount;
                //         }

                //         var finalText = $"{name} - Total Prints {sizeTotal}";

                //         Paragraph storeTotalName2 = new Paragraph(finalText)
                //         .SetFontSize(12)
                //         .SetWidth(PageSize.A4.GetWidth())
                //         .SetMarginTop(10)
                //         .SetTextAlignment(TextAlignment.CENTER);
                //         document.Add(storeTotalName2);

                //     }
                // }
            }

            document = CreatePages(document, orderByStoreAndSize);


            document.Close();
            return true;
        }

        private Document CreatePages(Document document, Dictionary<string, Dictionary<string, List<Order>>> orderList)
        {
            //
            foreach (KeyValuePair<string, Dictionary<string, List<Order>>> entry in orderList)
            {
                 document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                // document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                // Paragraph property = new Paragraph(entry.Key)
                // .SetFontSize(70)
                // .SetFixedPosition(20, 500, 0)
                // .SetWidth(PageSize.A4.GetWidth())
                // .SetMarginTop(10)
                // .SetTextAlignment(TextAlignment.CENTER);
                // document.Add(property);

                foreach (var order in entry.Value)
                {
                     document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                    // document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                    // Paragraph prop = new Paragraph(order.Key)
                    // .SetFontSize(70)
                    // .SetFixedPosition(20, 500, 0)
                    // .SetWidth(PageSize.A4.GetWidth())
                    // .SetMarginTop(10)
                    // .SetTextAlignment(TextAlignment.CENTER);
                    // document.Add(prop);

                    if (order.Key == "A4")
                    {
                        foreach (var o in order.Value)
                        {
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                            //New Page
                            document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                            document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));

                            //Add Image
                            if (o.Background != "null")
                            {
                                try
                                {
                                    var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                    Image img = new Image(ImageDataFactory.Create(imageUrl))
                                    .SetHeight(PageSize.A4.GetHeight())
                                    .SetWidth(PageSize.A4.GetWidth())
                                    .SetFixedPosition(0, 0);
                                    document.Add(img);
                                }
                                catch { };

                            }

                            //Add text
                            foreach (var t in templateProperties)
                            {

                                document = resizeText(document, t, "A4");
                            }

                            if (o.Amount > 1)
                            {
                                for (var i = 1; i < o.Amount; i++)
                                {
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                    if (o.Background != "null")
                                    {
                                        try
                                        {
                                            var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                            Image img = new Image(ImageDataFactory.Create(imageUrl))
                                            .SetHeight(PageSize.A4.GetHeight())
                                            .SetWidth(PageSize.A4.GetWidth())
                                            .SetFixedPosition(0, 0);
                                            document.Add(img);
                                        }
                                        catch
                                        {
                                        }

                                    }

                                    foreach (var t in templateProperties)
                                    {
                                        document = resizeText(document, t, "A4");
                                    }
                                }
                            }
                        }
                    }else if (order.Key == "A4 Iceland + Express")
                    {
                        foreach (var o in order.Value)
                        {
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                            //New Page
                            document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                            document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));

                            //Add Image
                            if (o.Background != "null")
                            {
                                try
                                {
                                    var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                    Image img = new Image(ImageDataFactory.Create(imageUrl))
                                    .SetHeight(PageSize.A4.GetHeight() - 28.3465f)
                                    .SetWidth(PageSize.A4.GetWidth() - 28.3465f)
                                    .SetFixedPosition(14.17f, 14.17f);
                                    document.Add(img);
                                }
                                catch { };

                            }

                            //Add text
                            foreach (var t in templateProperties)
                            {

                                document = resizeText(document, t, "A4 Iceland + Express");
                            }

                            if (o.Amount > 1)
                            {
                                for (var i = 1; i < o.Amount; i++)
                                {
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                    if (o.Background != "null")
                                    {
                                        try
                                        {
                                            var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                            Image img = new Image(ImageDataFactory.Create(imageUrl))
                                            .SetHeight(PageSize.A4.GetHeight() - 28.3465f)
                                            .SetWidth(PageSize.A4.GetWidth() - 28.3465f)
                                            .SetFixedPosition(14.17f, 14.17f);
                                            document.Add(img);
                                        }
                                        catch
                                        {
                                        }

                                    }

                                    foreach (var t in templateProperties)
                                    {
                                        document = resizeText(document, t, "A4 Iceland + Express");
                                    }
                                }
                            }
                        }
                    }else if (order.Key == "SRA4 WWH")
                    {
                        foreach (var o in order.Value)
                        {
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                            //New Page
                            var pageSize = new iText.Kernel.Geom.Rectangle(637.795f,907.0866f);
                            var x = new PageSize(637.795f, 907.0866f);
                           
                            document.GetPdfDocument().SetDefaultPageSize(x);
                            document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                            var leftPos = 21f;
                            var finalTopPosition = 907.0866f;
                            var bleedLineTop = finalTopPosition - 907f + 32f - 8.5f;

                            //Add Image
                            if (o.Background != "null")
                            {
                                try
                                {
                                   var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                    Image img = new Image(ImageDataFactory.Create(imageUrl))
                                    .SetHeight(PageSize.A4.GetHeight() + 17f)
                                    .SetWidth(PageSize.A4.GetWidth() + 17f)
                                    .SetFixedPosition(leftPos - 8.5f, finalTopPosition - 907f + 32f - 8.5f);
                                    document.Add(img);
                                }
                                catch { };

                            }

                            //Add text
                            foreach (var t in templateProperties)
                            {

                                document = resizeText(document, t, "SRA4");
                            }
                            PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f);       
                                canvas.LineTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas.LineTo(leftPos - 8.5f,finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas.ClosePathStroke();

                                //  // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f);       
                                canvas.LineTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas.LineTo(leftPos + PageSize.A4.GetWidth() + 8.5F, finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                canvas.MoveTo(leftPos, (907.0866f - bleedLineTop)); 
                                canvas.LineTo(leftPos, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas.LineTo(leftPos - 8.5f, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas.ClosePathStroke();

                                // Top Right
                                canvas.MoveTo(leftPos + PageSize.A4.GetWidth(), (907.0866f- bleedLineTop)); 
                                canvas.LineTo(leftPos + PageSize.A4.GetWidth(), (907.0866f - bleedLineTop) - 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos + PageSize.A4.GetWidth(), (907.0866f - bleedLineTop - 8.5f)); 
                                canvas.LineTo(leftPos + PageSize.A4.GetWidth() + 8.5F, (907.0866f - bleedLineTop - 8.5f)); 
                                canvas.ClosePathStroke();

                            if (o.Amount > 1)
                            {
                                for (var i = 1; i < o.Amount; i++)
                                {
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                    if (o.Background != "null")
                                    {
                                        try
                                        {
                                           var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                            Image img = new Image(ImageDataFactory.Create(imageUrl))
                                            .SetHeight(PageSize.A4.GetHeight() + 17f)
                                            .SetWidth(PageSize.A4.GetWidth() + 17f)
                                            .SetFixedPosition(leftPos - 8.5f, finalTopPosition - 907f + 32f - 8.5f);
                                            document.Add(img);
                                        }
                                        catch
                                        {
                                        }

                                    }

                                    foreach (var t in templateProperties)
                                    {
                                        document = resizeText(document, t, "SRA4");
                                    }

                                    PdfCanvas canvas2 = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // // Bootom Left
                                canvas2.SetLineWidth(0.5f);      
                                canvas2.MoveTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f);       
                                canvas2.LineTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f + 8.5f);      
                                canvas2.ClosePathStroke(); 

                                canvas2.MoveTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas2.LineTo(leftPos - 8.5f,finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas2.ClosePathStroke();

                                //  // Bootom Right
                                canvas2.SetLineWidth(0.5f);      
                                canvas2.MoveTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f);       
                                canvas2.LineTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f + 8.5f);      
                                canvas2.ClosePathStroke(); 

                                canvas2.MoveTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas2.LineTo(leftPos + PageSize.A4.GetWidth() + 8.5F, finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas2.ClosePathStroke();
                                
                                // Top Left
                                canvas2.MoveTo(leftPos, (907.0866f - bleedLineTop)); 
                                canvas2.LineTo(leftPos, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas2.ClosePathStroke();

                                canvas2.MoveTo(leftPos, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas2.LineTo(leftPos - 8.5f, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas2.ClosePathStroke();

                                // Top Right
                                canvas2.MoveTo(leftPos + PageSize.A4.GetWidth(), (907.0866f- bleedLineTop)); 
                                canvas2.LineTo(leftPos + PageSize.A4.GetWidth(), (907.0866f - bleedLineTop) - 8.5f); 
                                canvas2.ClosePathStroke();

                                canvas2.MoveTo(leftPos + PageSize.A4.GetWidth(), (907.0866f - bleedLineTop - 8.5f)); 
                                canvas2.LineTo(leftPos + PageSize.A4.GetWidth() + 8.5F, (907.0866f - bleedLineTop - 8.5f)); 
                                canvas2.ClosePathStroke();
                                }
                            }
                        }
                    }
                    else if (order.Key == "SB")
                    {
                        document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                        document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                        var currentHeight = 225 + 31f + 30f;
                        var currentHeightObjects = 22.5f + 30f;
                        var bleedLineTop = 22.5 + 30f;
                        var leftPos = 150f - 4.25f;
                        var leftPosPercent = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;

                        var text = "Number Of templates ==" + order.Value.Count;
                        Console.WriteLine(text);


                        foreach (var o in order.Value)
                        {

                            Console.WriteLine(JsonConvert.SerializeObject(o));
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                            leftPosPercent = ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) / 2) * 100;

                            //leftPos = (int)(PageSize.A4.GetWidth() - ((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))));
                            leftPos = 75f - 4.25f;
                            //   leftPos =  (int)(((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) * 100) - ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / 2)));
                            for (var x = 0; x < o.Amount; x++)
                            {

                                if (myNum % 4 == 0)
                                {
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                    finalTopPositionObject = 0f;
                                    currentHeightObjects = 22.5f + 30f;
                                    currentHeight = 225 + 31f + 30f;
                                    bleedLineTop = 22.5f + 30f;
                                    myNum = 1;
                                }
                                finalTopPosition = PageSize.A4.GetHeight() - currentHeight;
                                finalTopPositionObject = PageSize.A4.GetHeight() - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(224 + 17f)
                                        .SetWidth(448 + 17f + 8.50394f)
                                        .SetFixedPosition(leftPos - 8.5f, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                  var shiftLineLeft = 8.5f;
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos, finalTopPosition - shiftLineLeft);       
                                canvas.LineTo(leftPos, finalTopPosition + 8.5f - shiftLineLeft);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos - shiftLineLeft, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos - 20.5f - shiftLineLeft, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();

                                 // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + 448f + 8.5f, finalTopPosition - shiftLineLeft);       
                                canvas.LineTo(leftPos + 448f + 8.5f, finalTopPosition + 8.5f - shiftLineLeft);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + 448f + 8.5f + shiftLineLeft, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos + 448f + 8.5f + 20.5f + shiftLineLeft, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                if(myNum == 1){
                                    canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop + shiftLineLeft)); 
                                    canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop + shiftLineLeft) + 20.5f); 
                                    canvas.ClosePathStroke();
                                }else{  
                                    canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop + shiftLineLeft)); 
                                    canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop + shiftLineLeft) + 8.5f); 
                                    canvas.ClosePathStroke();
                                }
                                

                                canvas.MoveTo(leftPos - shiftLineLeft, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos - 20.5f - shiftLineLeft, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                // Top Right
                                if(myNum == 1){
                                     canvas.MoveTo(leftPos + 448f + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop + shiftLineLeft)); 
                                    canvas.LineTo(leftPos + 448f + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop + shiftLineLeft) + 20.5f); 
                                    canvas.ClosePathStroke();
                                }else{
                                     canvas.MoveTo(leftPos + 448f + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop + shiftLineLeft)); 
                                    canvas.LineTo(leftPos + 448f + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop + shiftLineLeft) + 8.5f); 
                                    canvas.ClosePathStroke();
                                }
                               

                                canvas.MoveTo(leftPos + 448f + 8.5f + shiftLineLeft, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 448f + 8.5f + 20.5f + shiftLineLeft, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                foreach (var t in templateProperties)
                                {
                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos);
                                }
                                
                                currentHeight += 225 + 31f - 8.5f;
                                currentHeightObjects += 225 + 31f - 8.5f;
                                bleedLineTop += 225 + 31f - 8.5f;
                                myNum += 1;
                            }
                            // var text = "Number Of templates ==" + myNum;
                            // Console.WriteLine(text);
                        }
                    }
                    else if (order.Key == "LB")
                    {
                        
                        document.GetPdfDocument().SetDefaultPageSize(PageSize.A4.Rotate());
                        document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                       var totalTemplates = 0;
                        var currentTemplate = 0;
                        var currentHeight = PageSize.A4.GetWidth() / 2;
                        var currentHeightObjects = 0f;
                        var bleedLineTop = 22.5f;

                        var leftPos = 0;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;
                        

                        var text = "Number Of templates ==" + order.Value.Count;
                        Console.WriteLine(text);


                        foreach (var o in order.Value)
                        {

                            Console.WriteLine(JsonConvert.SerializeObject(o));
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                         //   leftPosPercent = ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) / 2) * 100;

                            //leftPos = (int)(PageSize.A4.GetWidth() - ((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))));
                            leftPos = 0;
                            //   leftPos =  (int)(((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) * 100) - ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / 2)));
                            for (var x = 0; x < o.Amount; x++)
                            {

                                if (myNum % 3 == 0)
                                {
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                     currentHeight = PageSize.A4.GetWidth() / 2;
                                     currentHeightObjects = 0f;
                                     bleedLineTop = 22.5f;
                                    myNum = 1;
                                }
                                finalTopPosition = PageSize.A4.GetWidth() - currentHeight;
                                finalTopPositionObject = PageSize.A4.GetWidth() - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                         var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                            Image img = new Image(ImageDataFactory.Create(imageUrl))
                                            .SetHeight((PageSize.A4.GetWidth() / 2) - 28f)
                                            .SetWidth(PageSize.A4.GetHeight() - 28f)
                                            .SetFixedPosition(leftPos + 14f, finalTopPosition + 14f);
                                            document.Add(img);
                                    }
                                    catch { };

                                }

                                
                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                // canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(leftPos, finalTopPosition);       
                                // canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                // canvas.MoveTo(leftPos, finalTopPosition + 8.5f); 
                                // canvas.LineTo(leftPos + 8.5f, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();

                                 // Bootom Right
                                // canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(PageSize.A4.GetWidth(), finalTopPosition);       
                                // canvas.LineTo(PageSize.A4.GetWidth(), finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                // canvas.MoveTo(PageSize.A4.GetWidth(), finalTopPosition + 8.5f); 
                                // canvas.LineTo(PageSize.A4.GetWidth() -8.5F, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();
                                
                                // Top Left
                                // canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop) - 8.5f); 
                                // canvas.ClosePathStroke();

                                // canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(leftPos + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.ClosePathStroke();

                                // Top Right
                                // canvas.MoveTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop) + 8.5f); 
                                // canvas.ClosePathStroke();

                                // canvas.MoveTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(PageSize.A4.GetWidth() - 8.5F, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.ClosePathStroke();

                                foreach (var t in templateProperties)
                                {

                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos, "LB");
                                }
                                //    Paragraph container = new Paragraph("Text" + myNum)
                                //     .SetFixedPosition(100, finalTopPosition, 0)
                                //     .SetBorder(new SolidBorder(1))
                                //     .SetWidth(PixelsToPoints(ConvvertStringPixelsToInt("448"), 72))
                                //     .SetHeight(PixelsToPoints(ConvvertStringPixelsToInt("224"), 72));
                                // document.Add(container);
                                currentHeight += PageSize.A4.GetWidth() / 2;
                                currentHeightObjects += PageSize.A4.GetWidth() / 2;
                                bleedLineTop += 298 + 31f;
                                myNum += 1;
                            }

                            // var text = "Number Of templates ==" + myNum;
                            // Console.WriteLine(text);
                        }


                    }
                    else if (order.Key == "LB -WWH")
                    {
                        
                        var PP = new PageSize(907.0866f, 637.795f);   
                        document.GetPdfDocument().SetDefaultPageSize(PP);
                        document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));

                       var totalTemplates = 0;
                        var currentTemplate = 0;
                        var currentHeight = 297 + 22.5f;
                        var currentHeightObjects = 22.5f;
                        var bleedLineTop = 22.5f;

                        var leftPos = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;
                        

                        var text = "Number Of templates ==" + order.Value.Count;
                        Console.WriteLine(text);


                        foreach (var o in order.Value)
                        {

                            Console.WriteLine(JsonConvert.SerializeObject(o));
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                         //   leftPosPercent = ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) / 2) * 100;

                            //leftPos = (int)(PageSize.A4.GetWidth() - ((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))));
                            leftPos =  (((907.0866f - (841.89f - 31f)) / 2) / 2) + 8.5f + 2.125f;
                            //   leftPos =  (int)(((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) * 100) - ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / 2)));
                            for (var x = 0; x < o.Amount; x++)
                            {

                                if (myNum % 3 == 0)
                                {
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                     currentHeight = 297 + 22.5f;
                                     currentHeightObjects = 22.5f;
                                     bleedLineTop = 22.5f;
                                    myNum = 1;
                                }
                                finalTopPosition = 637.795f - currentHeight;
                                finalTopPositionObject = 637.795f - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                         var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                            Image img = new Image(ImageDataFactory.Create(imageUrl))
                                            .SetHeight(297.638f + 17f)
                                            .SetWidth(PageSize.A4.GetHeight() + 17f)
                                            .SetFixedPosition(leftPos - 13f + 5f, finalTopPosition - 4.25f);
                                            document.Add(img);
                                    }
                                    catch { };

                                }

                                
                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  //.kernel.color.Color myColor = new DeviceRgb(255, 100, 20);

                                  iText.Kernel.Colors.Color myColor = new DeviceRgb(0, 0, 0);
                                  canvas.SetStrokeColor(myColor);
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);  
                                if(myNum == 2){
                                    canvas.MoveTo(leftPos, finalTopPosition - 4.25f);       
                                    canvas.LineTo(leftPos, finalTopPosition - 20.5f - 4.25f);      
                                    canvas.ClosePathStroke(); 
                                }    
                                

                                if(myNum % 2 == 0){
                                    canvas.SetLineWidth(0.5f);  
                                    canvas.MoveTo(leftPos - 8.5f, finalTopPosition + 4.5f); 
                                    canvas.LineTo(leftPos - 8.5f - 30.5f, finalTopPosition + 4.5f); 
                                    canvas.ClosePathStroke();
                                 }else{
                                    canvas.SetLineWidth(0.5f);  
                                    canvas.MoveTo(leftPos - 8.5f, finalTopPosition + 8.5f); 
                                    canvas.LineTo(leftPos - 8.5f - 30.5f, finalTopPosition + 8.5f); 
                                    canvas.ClosePathStroke();
                                 }

                                 // Bootom Right
                                 canvas.SetLineWidth(0.5f);
                                 if(myNum == 2){      
                                    canvas.MoveTo(leftPos + PageSize.A4.GetHeight(), finalTopPosition - 4.25f);       
                                    canvas.LineTo(leftPos + PageSize.A4.GetHeight(), finalTopPosition - 20.5f - 4.25f);      
                                    canvas.ClosePathStroke(); 
                                 }
                                
                                if(myNum % 2 == 0){
                                     canvas.SetLineWidth(0.5f);  
                                    canvas.MoveTo(leftPos + 8.5f + PageSize.A4.GetHeight(), finalTopPosition + 4.5f); 
                                    canvas.LineTo(leftPos + 8.5f + PageSize.A4.GetHeight()  + 30.5F, finalTopPosition + 4.5f); 
                                    canvas.ClosePathStroke();
                                 }else{
                                     canvas.SetLineWidth(0.5f);  
                                    canvas.MoveTo(leftPos + 8.5f + PageSize.A4.GetHeight(), finalTopPosition + 8.5f); 
                                    canvas.LineTo(leftPos + 8.5f + PageSize.A4.GetHeight()  + 30.5F, finalTopPosition + 8.5f); 
                                    canvas.ClosePathStroke();
                                 }
                                
                                // Top Left
                                if(myNum == 1){
                                    canvas.SetLineWidth(0.5f);  
                                    canvas.MoveTo(leftPos, (637.795f - bleedLineTop + 8.5f + 4.25f)); 
                                    canvas.LineTo(leftPos, (637.795f - bleedLineTop) + 20.5f); 
                                    canvas.ClosePathStroke();
                                }
                                
                                canvas.SetLineWidth(0.5f);  
                                canvas.MoveTo(leftPos - 8.5f, (637.795f - bleedLineTop + 8.5f + 4.25f - 4.25f)); 
                                canvas.LineTo(leftPos - 8.5f - 30.5f, (637.795f - bleedLineTop + 8.5f + 4.25f - 4.25f)); 
                                canvas.ClosePathStroke();

                                // Top Right
                                if(myNum == 1){
                                    canvas.SetLineWidth(0.5f);  
                                    canvas.MoveTo(leftPos + PageSize.A4.GetHeight(), (637.795f - bleedLineTop + 8.5f + 4.25f)); 
                                    canvas.LineTo(leftPos + PageSize.A4.GetHeight(), (637.795f - bleedLineTop) + 20.5f); 
                                    canvas.ClosePathStroke();
                                }
                                canvas.SetLineWidth(0.5f);  
                                canvas.MoveTo(leftPos + 8.5f + PageSize.A4.GetHeight(), (637.795f - bleedLineTop + 8.5f + 4.25f - 4.25f)); 
                                canvas.LineTo(leftPos + 8.5f + PageSize.A4.GetHeight() + 30.5F, (637.795f - bleedLineTop + 8.5f + 4.25f - 4.25f)); 
                                canvas.ClosePathStroke();

                                foreach (var t in templateProperties)
                                {
                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos, "LB-SRA4");
                                }
                                //    Paragraph container = new Paragraph("Text" + myNum)
                                //     .SetFixedPosition(100, finalTopPosition, 0)
                                //     .SetBorder(new SolidBorder(1))
                                //     .SetWidth(PixelsToPoints(ConvvertStringPixelsToInt("448"), 72))
                                //     .SetHeight(PixelsToPoints(ConvvertStringPixelsToInt("224"), 72));
                                // document.Add(container);
                                currentHeight += 297 + 8.5f;
                                currentHeightObjects += 297 + 8.5f;
                                bleedLineTop += 297 + 13f;
                                myNum += 1;
                            }

                            // var text = "Number Of templates ==" + myNum;
                            // Console.WriteLine(text);
                        }


                    }
                    else if (order.Key == "SEL")
                    {
                        var PP = new PageSize(907.0866f, 637.795f);   
                        document.GetPdfDocument().SetDefaultPageSize(PP);
                        document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                        
                        var currentHeight = 107f + 17f + 5f + 11f;
                        float currentHeightObjects = 18f + 11f;
                        
                        //var leftPos = PageSize.A4.GetWidth() - (PixelsToPoints(ConvvertStringPixelsToInt("0"), 72));
                        var leftPos = 100f;
                        var leftPosPercent = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;
                        
                        
                        var bleedLineTop = 18f + 11f;

                        var text = "Number Of templates ==" + order.Value.Count;
                        Console.WriteLine(text);


                        foreach (var o in order.Value)
                        {

                            Console.WriteLine(JsonConvert.SerializeObject(o));
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                            leftPosPercent = ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) / 2) * 100;

                            //leftPos = (int)(PageSize.A4.GetWidth() - ((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))));
                            
                            //   leftPos =  (int)(((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) * 100) - ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / 2)));
                           var currentCount = 1;
                           var actualAmount = Math.Ceiling((double)o.Amount / 2);
                            for (var x = 0; x < actualAmount; x++)
                            {
                                
                                 if (myNum % 6 == 0)
                                {  
                                    document.GetPdfDocument().SetDefaultPageSize(PP);
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                    
                                    
                                    finalTopPositionObject = 0f;
                                    bleedLineTop = 18f + 11f;
                                    currentHeightObjects = 18f + 11f;
                                    currentHeight = 107f + 17f + 5f + 11f;
                                    myNum =1;
                                }

                                //################### Start Left Objects #############
                                leftPos = 85.5f;                                              
                                finalTopPosition = 637.795f - currentHeight;
                                finalTopPositionObject = 637.795f - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(107 + 8.5f)
                                        .SetWidth(354 + 8.5f)
                                        .SetFixedPosition(leftPos, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);  
                                if(myNum == 5 || currentCount == actualAmount){
                                    canvas.MoveTo(leftPos + 4.25f + 2.25f, finalTopPosition);       
                                    canvas.LineTo(leftPos + 4.25f + 2.25f, finalTopPosition - 20.5f);      
                                    canvas.ClosePathStroke(); 
                                }    
                                

                                canvas.MoveTo(leftPos, finalTopPosition + 4.25f + 2.25f); 
                                canvas.LineTo(leftPos - 30.5f, finalTopPosition + 4.25f + 2.25f); 
                                canvas.ClosePathStroke();

                                //  // Bootom Right
                                if(myNum == 5 || currentCount == actualAmount){
                                    canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + 354f + 4.25f  - 2.25f, finalTopPosition);       
                                canvas.LineTo(leftPos + 354f + 4.25f  - 2.25f, finalTopPosition - 20.5f);      
                                canvas.ClosePathStroke(); 
                                }
                                

                                // canvas.MoveTo(leftPos + 297f + 8.5f, finalTopPosition + 8.5f); 
                                // canvas.LineTo(leftPos + 297f + 8.5f + 8.5f, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();
                                
                                // // Top Left
                               // Vertical
                                if(myNum == 1){
                                canvas.MoveTo(leftPos + 4.25f  + 2.25f, (637.795f - bleedLineTop + 4.25f)); 
                                canvas.LineTo(leftPos + 4.25f  + 2.25f, (637.795f - bleedLineTop + 4.25f) + 20.5f); 
                                canvas.ClosePathStroke();
                                }

                                // Horizontal
                                 canvas.MoveTo(leftPos, (637.795f - bleedLineTop  - 2.25f)); 
                                canvas.LineTo(leftPos - 30.5f, (637.795f - bleedLineTop  - 2.25f)); 
                                canvas.ClosePathStroke();
                                

                                // // Top Right

                                 if(myNum == 1){
                                canvas.MoveTo(leftPos + 4.25f + 354f - 2.25f , (637.795f - bleedLineTop + 4.25)); 
                                canvas.LineTo(leftPos + 4.25f + 354f - 2.25f, (637.795f - bleedLineTop + 4.25) + 20.5f); 
                                canvas.ClosePathStroke();
                                }
                               

                                // canvas.MoveTo(leftPos + 4.25f + 354, (637.795f - bleedLineTop)); 
                                // canvas.LineTo(leftPos + 4.25f + 354 + 8.5f, (637.795f - bleedLineTop)); 
                                // canvas.ClosePathStroke();



                                foreach (var t in templateProperties)
                                {

                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos + 4.25f + 2.125f, "SEL");
                                }



                                //################## Start Right Object ###############################
                                leftPos = 85.5f + 354f + 8.5f + 8.5f; 
                                finalTopPosition = 637.795f - currentHeight;
                                finalTopPositionObject = 637.795f - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(107 + 8.5f)
                                        .SetWidth(354 + 8.5f)
                                        .SetFixedPosition(leftPos, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                                 canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                

                                if(myNum == 5 || currentCount == actualAmount){
                                   canvas.MoveTo(leftPos + 4.25f + 2.25f, finalTopPosition);       
                                    canvas.LineTo(leftPos + 4.25f + 2.25f, finalTopPosition - 20.5f);      
                                    canvas.ClosePathStroke(); 
                                }
                                // canvas.MoveTo(leftPos + 4.25f, finalTopPosition + 8.5f); 
                                // canvas.LineTo(leftPos + 4.25f - 8.5f, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();

                                // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                if(myNum == 5 || currentCount == actualAmount){
                                    canvas.MoveTo(leftPos  + 357f - 2.25f, finalTopPosition);       
                                    canvas.LineTo(leftPos + 357f - 2.25f, finalTopPosition - 20.5f);      
                                    canvas.ClosePathStroke(); 
                                }
                                canvas.MoveTo( leftPos + 8.5f + 354f , finalTopPosition + 4.25f + 2.25f); 
                                canvas.LineTo(leftPos + 8.5f + 354f + 30.5f, finalTopPosition + 4.25f + 2.25f); 
                                canvas.ClosePathStroke();
                                
                                // // Top Left
                                if(myNum == 1){
                                    canvas.MoveTo(leftPos + 4.25f + 2.25f, (637.795f - bleedLineTop + 4.25)); 
                                    canvas.LineTo(leftPos + 4.25f + 2.25f, (637.795f - bleedLineTop + 4.25) + 20.5f); 
                                    canvas.ClosePathStroke();
                                }
                                

                                // canvas.MoveTo(leftPos + 4.25f, (637.795f - bleedLineTop)); 
                                // canvas.LineTo(leftPos + 4.25f - 8.5f, (637.795f - bleedLineTop)); 
                                // canvas.ClosePathStroke();

                                // // Top Right
                                if(myNum == 1){
                                    canvas.MoveTo(leftPos + 4.25f + 354f - 2.25f, (637.795f - bleedLineTop + 4.25)); 
                                    canvas.LineTo(leftPos + 4.25f + 354f - 2.25f, (637.795f - bleedLineTop + 4.25) + 20.5f); 
                                    canvas.ClosePathStroke();
                                }
                                
                                canvas.MoveTo(leftPos + 8.5f + 354f, (637.795f - bleedLineTop - 2.25f)); 
                                canvas.LineTo(leftPos + 8.5f + 354f + 30.5f, (637.795f - bleedLineTop - 2.25f)); 
                                canvas.ClosePathStroke();



                                foreach (var t in templateProperties)
                                {

                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos + 4.25f + 2.125f, "SEL");
                                }
                                // currentHeight += 107f + 8.5f + 5f - 1.4173225f;
                                // currentHeightObjects += 107f +  8.5f - 1.4173225f;
                                // bleedLineTop += 100 +  8.5f + 5f - 1.4173225f;
                                
                                currentHeight += 107f + 8.5f + 5f - 1.4173225f;
                                currentHeightObjects += 107f +  8.5f + 5f - 1.4173225f;
                                bleedLineTop += 100 +  8.5f + 12.5f - 2.125f;
                                myNum += 1;
                                currentCount += 1;
                            }

                            // var text = "Number Of templates ==" + myNum;
                            // Console.WriteLine(text);
                        }


                    } else if (order.Key == "SEL - Morrisons")
                     {
                        document.GetPdfDocument().SetDefaultPageSize(PageSize.A4.Rotate());
                        document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                        
                        var currentHeight = 100 + 28.3465f;
                        float currentHeightObjects = 22.5f;
                        
                        //var leftPos = PageSize.A4.GetWidth() - (PixelsToPoints(ConvvertStringPixelsToInt("0"), 72));
                        var leftPos = 100f;
                        var leftPosPercent = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;
                        var countColNum = 1;
                        var bleedLineTop = 22.5f;

                        var text = "Number Of templates ==" + order.Value.Count;
                        Console.WriteLine(text);


                        foreach (var o in order.Value)
                        {

                            Console.WriteLine(JsonConvert.SerializeObject(o));
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                            leftPosPercent = ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) / 2) * 100;

                            //leftPos = (int)(PageSize.A4.GetWidth() - ((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))));
                            
                            //   leftPos =  (int)(((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) * 100) - ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / 2)));
                           
                           var actualAmount = Math.Ceiling((double)o.Amount / 2);
                            for (var x = 0; x < actualAmount; x++)
                            {
                                
                                 if (myNum % 5 == 0)
                                {
                                    document.GetPdfDocument().SetDefaultPageSize(PageSize.A4.Rotate());
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                    
                                    finalTopPositionObject = 0f;
                                    bleedLineTop = 22.5f;
                                    currentHeightObjects = 22.5f;
                                    currentHeight = 100 + 28.3465f;
                                    myNum =1;
                                }

                                //################### Start Left Objects #############
                                leftPos = 105.5f - 15.5f;                                              
                                finalTopPosition = PageSize.A4.Rotate().GetHeight() - currentHeight;
                                finalTopPositionObject = PageSize.A4.Rotate().GetHeight() - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(86 + 28.3465f)
                                        .SetWidth(323.15f + 17f)
                                        .SetFixedPosition(leftPos - 8.5f, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos, finalTopPosition);       
                                canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos - 8.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();

                                 // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + 297f, finalTopPosition);       
                                canvas.LineTo(leftPos + 297f, finalTopPosition + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + 297f, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos + 297f + 8.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                canvas.MoveTo(leftPos, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos, (PageSize.A4.Rotate().GetHeight() - bleedLineTop) + 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos - 8.5f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                // Top Right
                                canvas.MoveTo(leftPos + 297f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 297f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop) + 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos + 297f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 297f + 8.5f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                foreach (var t in templateProperties)
                                {
                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos - 8.5f, "SEL");
                                }

                                //################## Start Right Object ###############################
                                leftPos = 105.5f + 314f - 15.5f; 
                                finalTopPosition = PageSize.A4.Rotate().GetHeight() - currentHeight;
                                finalTopPositionObject = PageSize.A4.Rotate().GetHeight() - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(86 + 28.3465f)
                                        .SetWidth(314 - 31f)
                                        .SetFixedPosition(leftPos - 8.5f, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                                 canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos, finalTopPosition);       
                                canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos - 8.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();

                                 // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + 297f, finalTopPosition);       
                                canvas.LineTo(leftPos + 297f, finalTopPosition + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + 297f, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos + 297f + 8.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                canvas.MoveTo(leftPos, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos, (PageSize.A4.Rotate().GetHeight() - bleedLineTop) + 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos - 8.5f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                // Top Right
                                canvas.MoveTo(leftPos + 297f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 297f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop) + 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos + 297f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 297f + 8.5f, (PageSize.A4.Rotate().GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();



                                foreach (var t in templateProperties)
                                {

                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos - 8.5f, "SEL");
                                }
                                
                                currentHeight += 100 + 28.3465f;
                                currentHeightObjects += 100 + 28.3465f;
                                bleedLineTop += 100 + 28.3465f;
                                myNum += 1;
                            }

                            // var text = "Number Of templates ==" + myNum;
                            // Console.WriteLine(text);
                        }


                    }else if (order.Key == "HB")
                    {
                         
                        document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                        document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                        
                        var currentHeight = 215.433f + 8.5f + 95f;
                        float currentHeightObjects = 103f;
                        
                        //var leftPos = PageSize.A4.GetWidth() - (PixelsToPoints(ConvvertStringPixelsToInt("0"), 72));
                        var leftPos = 100f;
                        var leftPosPercent = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;
                        var countColNum = 1;
                        var bleedLineTop = 103f;

                        var text = "Number Of templates ==" + order.Value.Count;
                        Console.WriteLine(text);


                        foreach (var o in order.Value)
                        {

                            Console.WriteLine(JsonConvert.SerializeObject(o));
                            var templateProperties = GettemplateDisplayByBuyerAndTemplate(o.BuyersOfferId, o.TemplateId);
                            leftPosPercent = ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) / 2) * 100;

                            //leftPos = (int)(PageSize.A4.GetWidth() - ((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))));
                            
                            //   leftPos =  (int)(((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / PageSize.A4.GetWidth()) * 100) - ((((PixelsToPoints(ConvvertStringPixelsToInt(o.Width), 72))) / 2)));
                           
                           var actualAmount = Math.Ceiling((double)o.Amount / 2);
                           var currentCount = 0;
                            for (var x = 0; x < actualAmount; x++)
                            {
                                
                                 if (myNum % 4 == 0)
                                {  
                                    document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                                    
                                    
                                    finalTopPositionObject = 0f;
                                    bleedLineTop = 103f;
                                    currentHeightObjects = 103f;
                                    currentHeight = 215.433f + 8.5f + 95f;
                                    myNum =1;
                                }

                                //################### Start Left Objects #############
                                leftPos = 8.5f;                                              
                                finalTopPosition = PageSize.A4.GetHeight() - currentHeight;
                                finalTopPositionObject = PageSize.A4.GetHeight() - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(215.433f)
                                        .SetWidth(289.134f)
                                        .SetFixedPosition(leftPos, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);  
                                if(myNum == 3 || currentCount == actualAmount - 1){
                                    canvas.MoveTo(leftPos, finalTopPosition - 4.25f);       
                                    canvas.LineTo(leftPos, finalTopPosition - 4.25f - 20.5f);      
                                    canvas.ClosePathStroke(); 
                                }    
                                
                                canvas.MoveTo(leftPos - 8.5f, finalTopPosition); 
                                canvas.LineTo(leftPos - 4.25f, finalTopPosition); 
                                canvas.ClosePathStroke();

                                //  // Bootom Right
                                if(myNum == 3 || currentCount == actualAmount - 1){
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + 289.134f, finalTopPosition - 4.25);       
                                canvas.LineTo(leftPos + 289.134f, finalTopPosition - 4.25 - 20.5f);      
                                canvas.ClosePathStroke(); 
                                }
                                
                                //  if(currentCount == actualAmount){
                                //     canvas.MoveTo(leftPos + 297f + 8.5f, finalTopPosition + 8.5f); 
                                //     canvas.LineTo(leftPos + 297f + 8.5f + 8.5f, finalTopPosition + 8.5f); 
                                //     canvas.ClosePathStroke();
                                //  }
                               
                                // // Top Left
                               // Vertical
                                if(myNum == 1){
                                canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop + 4.25f)); 
                                canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop + 4.25f) + 30.5f); 
                                canvas.ClosePathStroke();
                                }

                                // Horizontal
                                 canvas.MoveTo(leftPos -8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos - 4.25f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();
                                

                                // // Top Right

                                 if(myNum == 1){
                                canvas.MoveTo(leftPos + 289.134f, (PageSize.A4.GetHeight() - bleedLineTop + 4.25)); 
                                canvas.LineTo(leftPos + 289.134f, (PageSize.A4.GetHeight() - bleedLineTop + 4.25) + 30.5f); 
                                canvas.ClosePathStroke();
                                }
                               

                                // canvas.MoveTo(leftPos + 4.25f + 354, (637.795f - bleedLineTop)); 
                                // canvas.LineTo(leftPos + 4.25f + 354 + 8.5f, (637.795f - bleedLineTop)); 
                                // canvas.ClosePathStroke();



                                foreach (var t in templateProperties)
                                {

                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos);
                                }



                                //################## Start Right Object ###############################
                                leftPos = 0f + 289.134f + 8.5f; 
                                finalTopPosition = PageSize.A4.GetHeight() - currentHeight;
                                finalTopPositionObject = PageSize.A4.GetHeight() - currentHeightObjects;
                                if (o.Background != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + o.Background;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(215.433f)
                                        .SetWidth(289.134f)
                                        .SetFixedPosition(leftPos, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                                 canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                

                                // if(myNum == 3){
                                //    canvas.MoveTo(leftPos, finalTopPosition);       
                                // canvas.LineTo(leftPos, finalTopPosition - 20.5f);      
                                // canvas.ClosePathStroke(); 
                                // }
                                // canvas.MoveTo(leftPos + 4.25f, finalTopPosition + 8.5f); 
                                // canvas.LineTo(leftPos + 4.25f - 8.5f, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();

                                // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                if(myNum == 3 || currentCount == actualAmount - 1){
                                    canvas.MoveTo(leftPos + 289.134f, finalTopPosition - 4.25f);       
                                    canvas.LineTo(leftPos + 289.134f, finalTopPosition - 4.25f - 20.5f);      
                                    canvas.ClosePathStroke(); 

                                    canvas.MoveTo(leftPos + 289.134f + 3.5f , finalTopPosition); 
                                    canvas.LineTo(leftPos + 289.134f + 8.5f, finalTopPosition); 
                                    canvas.ClosePathStroke();
                                }
                                if(currentCount == actualAmount - 1){
                                    canvas.MoveTo(leftPos + 297f + 8.5f, finalTopPosition + 8.5f); 
                                    canvas.LineTo(leftPos + 297f + 8.5f + 8.5f, finalTopPosition + 8.5f); 
                                    canvas.ClosePathStroke();
                                 }
                                
                                // // Top Left
                                // if(myNum == 1){
                                //     canvas.MoveTo(leftPos + 4.25f, (PageSize.A4.GetHeight() - bleedLineTop + 4.25)); 
                                //     canvas.LineTo(leftPos + 4.25f, (PageSize.A4.GetHeight() - bleedLineTop + 4.25) + 20.5f); 
                                //     canvas.ClosePathStroke();
                                // }
                                

                                // canvas.MoveTo(leftPos + 4.25f, (637.795f - bleedLineTop)); 
                                // canvas.LineTo(leftPos + 4.25f - 8.5f, (637.795f - bleedLineTop)); 
                                // canvas.ClosePathStroke();

                                // // Top Right
                                if(myNum == 1){
                                    canvas.MoveTo(leftPos + 289.134f, (PageSize.A4.GetHeight() - bleedLineTop + 4.25)); 
                                    canvas.LineTo(leftPos + 289.134f, (PageSize.A4.GetHeight() - bleedLineTop + 4.25) + 30.5f); 
                                    canvas.ClosePathStroke();
                                }
                                
                                canvas.MoveTo(leftPos + 289.134f + 3.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 289.134f + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();



                                foreach (var t in templateProperties)
                                {

                                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos);
                                }
                                
                                currentHeight += 215.433f;
                                currentHeightObjects += 215.433f;
                                bleedLineTop += 215.433f;
                                myNum += 1;
                                currentCount += 1;
                            }

                            // var text = "Number Of templates ==" + myNum;
                            // Console.WriteLine(text);
                        }


                    }

                }



            }
            document.GetPdfDocument().RemovePage(1);
            return document;
        }

        private bool CreateDirectory(string path)
        {
            try
            {
                // Determine whether the directory exists.
                if (Directory.Exists(path))
                {
                    Console.WriteLine("That path exists already.");
                    return true;
                }

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(path);
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path));
            }
            catch (Exception e)
            {
                return false;
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

            return true;
        }



        public List<Order> GetOrder(int id)
        {
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Order>();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource;
                builder.UserID = dataSourceConnection.UserId;
                builder.Password = dataSourceConnection.Password;
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    // String sql = $@"
                    // SELECT 
                    // d.StoreID
                    // ,d.StoreName
                    // ,b.IslandName
                    // ,c.name AS bandname
                    // INTO #store
                    // FROM IslandBandMap a
                    // INNER JOIN Island b ON a.IslandID=b.ID
                    // INNER JOIN Band   c ON c.id=a.BandID
                    // INNER JOIN store  d ON d.IslandBandMapID =a.ID

                    // SELECT c.OrderID,OrderNumber,OrderDate,
                    // b.StoreID,bandname,IslandName,StoreName,
                    // d.PromProdCode,PromDescription,
                    // f.Name OfferTypeName ,g.id TemplateID,g.templateName,g.backgroundImage, d.BuyersOfferId,
					// i.Description AS SizeDescription, i.Height, i.Width, i.Size,
					// d.PromProdCode,
                    // CASE WHEN h.Amount IS NOT NULL THEN h.Amount ELSE e.Amount END AS Amount,
                    // j.name OrderStatus
                    // --,e.Amount BandAmount,h.Amount StoreAmount
                    // FROM StoreOffer a
                    // INNER JOIN #store b ON a.StoreID = b.StoreID
                    // INNER JOIN  Orders c ON c.OrderID = a.OrderID
                    // INNER JOIN StoreOfferStatus j on j.id = c.StatusID
                    // INNER JOIN BuyerOffer d on a.BuyersOfferID = d.BuyersOfferID
                    // INNer JOIN OfferType f on f.id=a.OfferTypeID
                    // INNER JOIN  OfferTypeDisplayTypes e ON e.OfferTypeId = a.OfferTypeID
                    // INNER JOIN Template g on e.DisplayTypeId =g.id
					// inner join PromoDisplayType i on g.templateType = i.id
                    // LEFT JOIN  StoreOfferTypeDisplayTypes h on h.OfferTypeDisplayTypeID=e.id
                    //                                         AND h.StoreID =a.StoreID
                    // WHERE c.OrderID = {id}
                    // AND a.statusid = 4
                    // order by b.StoreName


                    // drop table #store;";

                    string sql = $@"
                     SELECT 
                    d.StoreID
                    ,d.StoreName
                    ,b.IslandName
                    ,c.name AS bandname
                    INTO #store
                    FROM IslandBandMap a
                    INNER JOIN Island b ON a.IslandID=b.ID
                    INNER JOIN Band   c ON c.id=a.BandID
                    INNER JOIN store  d ON d.IslandBandMapID =a.ID

					DECLARE @store INT
					SELECT top (1) @store =  StoreID
					FROM StoreOffer a
					INNER JOIN  Orders c ON c.OrderID = a.OrderID
                    WHERE 
					c.OrderID = {id} AND 
					a.statusid = 4

                    SELECT c.OrderID,OrderNumber,OrderDate,
                    b.StoreID,bandname,IslandName,StoreName,
                    d.PromProdCode,PromDescription,
                    f.Name OfferTypeName ,g.id TemplateID,g.templateName,g.backgroundImage, d.BuyersOfferId,
                    i.Description AS SizeDescription, i.Height, i.Width, i.Size,
                    d.PromProdCode,
                    CASE WHEN h.Amount IS NOT NULL THEN h.Amount ELSE e.Amount END AS Amount,
                    j.name OrderStatus
                    --,e.Amount BandAmount,h.Amount StoreAmount
                    FROM StoreOffer a
                    INNER JOIN #store b ON a.StoreID = b.StoreID
                    INNER JOIN  Orders c ON c.OrderID = a.OrderID
                    INNER JOIN StoreOfferStatus j on j.id = c.StatusID
                    INNER JOIN BuyerOffer d on a.BuyersOfferID = d.BuyersOfferID
                    INNer JOIN OfferType f on f.id=a.OfferTypeID
                    INNER JOIN  OfferTypeDisplayTypes e ON e.OfferTypeId = a.OfferTypeID
                    INNER JOIN Template g on e.DisplayTypeId =g.id
                    inner join PromoDisplayType i on g.templateType = i.id
                    LEFT JOIN  StoreOfferTypeDisplayTypes h on h.OfferTypeDisplayTypeID=e.id
                                                            AND h.StoreID =a.StoreID
                    WHERE 
					c.OrderID = {id} AND 
					a.statusid = 4 AND 
					a.StoreID = @store
                    order by b.StoreName

					drop table #store";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // command.CommandType = CommandType.StoredProcedure;
                        // command.Parameters.Add(new SqlParameter("@OrderID", id));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new Order();
                                band.OrderId = Int32.Parse(reader["OrderId"].ToString());
                                band.OrderNumber = reader["OrderNumber"].ToString();
                                band.Amount = Int32.Parse(reader["Amount"].ToString());
                                band.TemplateId = Int32.Parse(reader["TemplateId"].ToString());
                                band.BuyersOfferId = Int32.Parse(reader["BuyersOfferId"].ToString());
                                band.TemplateName = reader["TemplateName"].ToString();
                                band.Background = reader["backgroundImage"].ToString();
                                band.StoreName = reader["StoreName"].ToString();
                                band.PromProdCode = reader["PromProdCode"].ToString();
                                band.SizeDescription = reader["SizeDescription"].ToString();
                                band.Size = reader["Size"].ToString();
                                band.Width = reader["Width"].ToString();
                                bandList.Add(band);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<TemplateProperties> GettemplateDisplayByBuyerAndTemplate(int buyerOfferId, int templateId)
        {

            var returnList = new List<TemplateProperties>();
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource;
                builder.UserID = dataSourceConnection.UserId;
                builder.Password = dataSourceConnection.Password;
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    string sql = $"[dbo].[GetTemplateInfoByBuyer]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@BuyerOffersId", buyerOfferId));
                        command.Parameters.Add(new SqlParameter("@TemplateId", templateId));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new TemplateProperties();
                                //template.templateId = Int32.Parse(reader["templateId"].ToString());
                                template.width = reader["width"].ToString();
                                template.height = reader["height"].ToString();
                                template.topProperty = reader["topProperty"].ToString();
                                template.leftProperty = reader["leftProperty"].ToString();
                                template.fontSize = reader["fontSize"].ToString();
                                template.color = reader["color"].ToString();
                                template.spacing = reader["spacing"].ToString();
                                template.fontWeight = reader["fontWeight"].ToString();
                                template.active = bool.Parse(reader["active"].ToString());
                                template.textProperty = reader["textProperty"].ToString();
                                template.FieldValue = reader["FieldValue"].ToString();
                                template.FieldName = reader["FieldName"].ToString();
                                template.fontstyle = reader["fontstyle"].ToString();
                                template.TemplateSizeDescription = reader["TemplateSizeDescription"].ToString();
                                template.BackgroundImage = reader["backgroundImage"].ToString();
                                template.Savings = reader["Savings"].ToString();
                                template.TextAlign = reader["textalign"].ToString();
                                template.Strikethrough = Int32.Parse(reader["strikethrough"].ToString());
                                template.DateFormat = Int32.Parse(reader["dateFormat"].ToString());
                                template.Rotate = Int32.Parse(reader["rotate"].ToString());
                                template.TextBefore = reader["textBefore"].ToString();
                                template.TextAfter = reader["textAfter"].ToString();
                                template.VerticalAlign = reader["verticalAlign"].ToString();

                                returnList.Add(template);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return returnList;
        }

        public void CreatePreview(int buyerOffer, int template)
        {
            var fileName = "preview.pdf";
            var folderName = System.IO.Path.Combine("StaticFiles", "Resources", "Images");
            var pathToSave = System.IO.Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = System.IO.Path.Combine(pathToSave, fileName);

            PdfWriter writer = new PdfWriter(fullPath);
            PdfDocument pdf = new PdfDocument(writer);

            pdf.SetDefaultPageSize(PageSize.A4);
            Document document = new Document(pdf);
           // document.SetMargins(100, 100, 100, 100);
           document.SetLeftMargin(50f);
           document.SetRightMargin(50f);
        //    document.SetLeftMargin(50f);
        //    document.SetLeftMargin(50f);
            var templateProperties = GettemplateDisplayByBuyerAndTemplate(buyerOffer, template);
            var templateSize = "";
            var backgroundImage = "";
            try{
                 templateSize = templateProperties[0].TemplateSizeDescription;
                 backgroundImage = templateProperties[0].BackgroundImage;
            }
            catch{

            }
           
            try{
                if (templateSize == "A4")
            {
                document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
               // document.SetMargins(20,20,20,20);
               
                if (backgroundImage != "null")
                {
                    try
                    {
                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                        .SetHeight(PageSize.A4.GetHeight())
                        .SetWidth(PageSize.A4.GetWidth())
                        .SetFixedPosition(0f, 0f);
                        document.Add(img);
                    }
                    catch { };

                }

                foreach (var t in templateProperties)
                {
                    document = resizeText(document, t, "A4");
                }

            }else if (templateSize == "A4 Iceland + Express")
            {
                document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
               // document.SetMargins(20,20,20,20);
               
                if (backgroundImage != "null")
                {
                    try
                    {
                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                        .SetHeight(PageSize.A4.GetHeight() - 28.3465f)
                        .SetWidth(PageSize.A4.GetWidth() - 28.3465f)
                        .SetFixedPosition(14.17f, 14.17f);
                        document.Add(img);
                    }
                    catch { };

                }

                foreach (var t in templateProperties)
                {
                    document = resizeText(document, t, "A4 Iceland + Express");
                }

            }
            else if(templateSize == "SRA4 WWH"){
                var pageSize = new iText.Kernel.Geom.Rectangle(637.795f,907.0866f);
                var x = new PageSize(637.795f, 907.0866f);
                           
                document.GetPdfDocument().SetDefaultPageSize(x);
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));

                var leftPos = 21f;
                var finalTopPosition = 907.0866f;
                var bleedLineTop = finalTopPosition - 907f + 32f - 8.5f;

                if (backgroundImage != "null")
                {
                    try
                    {
                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                        .SetHeight(PageSize.A4.GetHeight() + 17f)
                        .SetWidth(PageSize.A4.GetWidth() + 17f)
                        .SetFixedPosition(leftPos - 8.5f, finalTopPosition - 907f + 32f - 8.5f);
                        document.Add(img);
                    }
                    catch { };

                }

                foreach (var t in templateProperties)
                {
                    document = resizeText(document, t, "SRA4");
                }

                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f);       
                                canvas.LineTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos, finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas.LineTo(leftPos - 8.5f,finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas.ClosePathStroke();

                                //  // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f);       
                                canvas.LineTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f - 8.5f + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + PageSize.A4.GetWidth(), finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas.LineTo(leftPos + PageSize.A4.GetWidth() + 8.5F, finalTopPosition - PageSize.A4.GetHeight() - 32f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                canvas.MoveTo(leftPos, (907.0866f - bleedLineTop)); 
                                canvas.LineTo(leftPos, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas.LineTo(leftPos - 8.5f, (907.0866f - bleedLineTop) - 8.5f); 
                                canvas.ClosePathStroke();

                                // Top Right
                                canvas.MoveTo(leftPos + PageSize.A4.GetWidth(), (907.0866f- bleedLineTop)); 
                                canvas.LineTo(leftPos + PageSize.A4.GetWidth(), (907.0866f - bleedLineTop) - 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos + PageSize.A4.GetWidth(), (907.0866f - bleedLineTop - 8.5f)); 
                                canvas.LineTo(leftPos + PageSize.A4.GetWidth() + 8.5F, (907.0866f - bleedLineTop - 8.5f)); 
                                canvas.ClosePathStroke();
            }
            else if (templateSize == "SB")
            {
                document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                 document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
             
                        var totalTemplates = 0;
                        var currentTemplate = 0;
                        var currentHeight = 225 + 31f;
                        var currentHeightObjects = 22.5f;
                        var bleedLineTop = 22.5f;
                        var left = 0;
                        //var leftPos = PageSize.A4.GetWidth() - (PixelsToPoints(ConvvertStringPixelsToInt("0"), 72));
                        var leftPos = 75f - 4.25f;
                        var leftPosPercent = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;

                finalTopPosition = PageSize.A4.GetHeight() - currentHeight;
                finalTopPositionObject = PageSize.A4.GetHeight() - currentHeightObjects;
                if (backgroundImage != "null")
                {
                    try
                    {
                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(224 + 17f)
                                        .SetWidth(448 + 17f + 8.50394f)
                                        .SetFixedPosition(leftPos - 8.5f, finalTopPosition);
                                        document.Add(img);
                    }
                    catch { };

                }

                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos, finalTopPosition);       
                                canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos - 8.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();

                                 // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + 448f + 4.25f, finalTopPosition);       
                                canvas.LineTo(leftPos + 448f + 4.25f, finalTopPosition + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + 448f + 4.25f, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos + 448f + 8.5f + 4.25f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop) + 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos - 8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                // Top Right
                                canvas.MoveTo(leftPos + 448f + 4.25f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 448f + 4.25f, (PageSize.A4.GetHeight() - bleedLineTop) + 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos + 448f + 4.25f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 448f + 8.5f + 4.25f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                foreach (var t in templateProperties)
                {

                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos);
                }

            }else if (templateSize == "LB")
            {
                document.GetPdfDocument().SetDefaultPageSize(PageSize.A4.Rotate());
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                var totalTemplates = 0;
                var currentTemplate = 0;
                var currentHeight = 297 + 31f;
                var currentHeightObjects = 22.5f;
                var bleedLineTop = 22.5f;

                var leftPos = 0;
                var finalTopPosition = 0f;
                var finalTopPositionObject = 0f;

                leftPos = 0;

                finalTopPosition = PageSize.A4.GetWidth() - currentHeight;
                finalTopPositionObject = PageSize.A4.GetWidth() - currentHeightObjects;
                if (backgroundImage != "null")
                {
                    try
                    {
                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                        .SetHeight((PageSize.A4.GetWidth() / 2) - 28f)
                        .SetWidth(PageSize.A4.GetHeight() - 28f)
                        .SetFixedPosition(leftPos + 14f, finalTopPosition + 14f);
                        document.Add(img);
                    }
                    catch { };

                }

                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                // canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(leftPos, finalTopPosition);       
                                // canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                // canvas.MoveTo(leftPos, finalTopPosition + 8.5f); 
                                // canvas.LineTo(leftPos + 8.5f, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();

                                 // Bootom Right
                                // canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(PageSize.A4.GetWidth(), finalTopPosition);       
                                // canvas.LineTo(PageSize.A4.GetWidth(), finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                // canvas.MoveTo(PageSize.A4.GetWidth(), finalTopPosition + 8.5f); 
                                // canvas.LineTo(PageSize.A4.GetWidth() -8.5F, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();
                                
                                // Top Left
                                // canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop) - 8.5f); 
                                // canvas.ClosePathStroke();

                                // canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(leftPos + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.ClosePathStroke();

                                // // Top Right
                                // canvas.MoveTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop) + 8.5f); 
                                // canvas.ClosePathStroke();

                                // canvas.MoveTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(PageSize.A4.GetWidth() - 8.5F, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.ClosePathStroke();

                foreach (var t in templateProperties)
                {

                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos, "LB");
                }
            }else if (templateSize == "LB -WWH")
            {
                var PP = new PageSize(907.0866f, 637.795f);   
                document.GetPdfDocument().SetDefaultPageSize(PP);
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                var totalTemplates = 0;
                var currentTemplate = 0;
                var currentHeight = 297 + 31f;
                var currentHeightObjects = 22.5f;
                var bleedLineTop = 22.5f;

                var leftPos = 0f;
                var finalTopPosition = 0f;
                var finalTopPositionObject = 0f;

                leftPos =  (((907.0866f - (841.89f - 31f)) / 2) / 2) + 8.5f;

                finalTopPosition = 637.795f - currentHeight;
                finalTopPositionObject = 637.795f - currentHeightObjects;
                if (backgroundImage != "null")
                {
                    try
                    {
                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                        .SetHeight(297.638f + 8.5f)
                        .SetWidth(PageSize.A4.GetHeight() + 8.5f)
                        .SetFixedPosition(leftPos - 8.5f + 5f, finalTopPosition);
                        document.Add(img);
                    }
                    catch { };

                }

                PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                // canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(leftPos, finalTopPosition);       
                                // canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                // canvas.MoveTo(leftPos, finalTopPosition + 8.5f); 
                                // canvas.LineTo(leftPos + 8.5f, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();

                                 // Bootom Right
                                // canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(PageSize.A4.GetWidth(), finalTopPosition);       
                                // canvas.LineTo(PageSize.A4.GetWidth(), finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                // canvas.MoveTo(PageSize.A4.GetWidth(), finalTopPosition + 8.5f); 
                                // canvas.LineTo(PageSize.A4.GetWidth() -8.5F, finalTopPosition + 8.5f); 
                                // canvas.ClosePathStroke();
                                
                                // Top Left
                                // canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop) - 8.5f); 
                                // canvas.ClosePathStroke();

                                // canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(leftPos + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.ClosePathStroke();

                                // // Top Right
                                // canvas.MoveTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop) + 8.5f); 
                                // canvas.ClosePathStroke();

                                // canvas.MoveTo(PageSize.A4.GetWidth(), (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.LineTo(PageSize.A4.GetWidth() - 8.5F, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                // canvas.ClosePathStroke();

                foreach (var t in templateProperties)
                {

                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos, "LB-SRA4");
                }
            }
            else if (templateSize == "SEL")
            {
                document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                        var currentHeight = 107f + 17f + 10f;
                        float currentHeightObjects = 22.5f;
                        
                        //var leftPos = PageSize.A4.GetWidth() - (PixelsToPoints(ConvvertStringPixelsToInt("0"), 72));
                        var leftPos = 140.5f;
                        var leftPosPercent = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;
                        var bleedLineTop = 10f + 8.5f + 4.5f;

        
                finalTopPosition = PageSize.A4.GetHeight() - currentHeight;
                finalTopPositionObject = PageSize.A4.GetHeight() - currentHeightObjects;
                

                 PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(leftPos, finalTopPosition - 8.5f);       
                                // canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos, finalTopPosition + 7f); 
                                canvas.LineTo(leftPos - 20.5f, finalTopPosition + 7f); 
                                canvas.ClosePathStroke();

                                 // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(leftPos + 354f, finalTopPosition - 8.5f);       
                                // canvas.LineTo(leftPos + 354f, finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + 354f, finalTopPosition + 7f); 
                                canvas.LineTo(leftPos + 354f + 20.5f, finalTopPosition + 7f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                canvas.MoveTo(leftPos - 2.125f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos - 2.125f, (PageSize.A4.GetHeight() - bleedLineTop) + 15.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop - 2.125f)); 
                                canvas.LineTo(leftPos - 15.5f, (PageSize.A4.GetHeight() - bleedLineTop - 2.125f)); 
                                canvas.ClosePathStroke();

                                // Top Right
                                canvas.MoveTo(leftPos + 354f - 8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 354f - 8.5f, (PageSize.A4.GetHeight() - bleedLineTop) + 15.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos + 354f, (PageSize.A4.GetHeight() - bleedLineTop - 2.125)); 
                                canvas.LineTo(leftPos + 354f + 15.5f, (PageSize.A4.GetHeight() - bleedLineTop - 2.125)); 
                                canvas.ClosePathStroke();

                                if (backgroundImage != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(107 + 8.5f)
                                        .SetWidth(354 + 8.5f)
                                        .SetFixedPosition(leftPos - 8.5f, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                foreach (var t in templateProperties)
                {

                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos);
                }
            }else if (templateSize == "SEL - Morrisons")
            {
                document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                        var currentHeight = 100 + 28.3465f;
                        float currentHeightObjects = 22.5f;
                        
                        //var leftPos = PageSize.A4.GetWidth() - (PixelsToPoints(ConvvertStringPixelsToInt("0"), 72));
                        var leftPos = 140.5f;
                        var leftPosPercent = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;
                        var bleedLineTop = 22.5f;

        
                finalTopPosition = PageSize.A4.GetHeight() - currentHeight;
                finalTopPositionObject = PageSize.A4.GetHeight() - currentHeightObjects;
                if (backgroundImage != "null")
                {
                    try
                    {
                         var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(86 + 28.3465f)
                                        .SetWidth(323 - 17f)
                                        .SetFixedPosition(leftPos - 8.5f, finalTopPosition);
                                        document.Add(img);
                    }
                    catch { };

                }

                 PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos, finalTopPosition);       
                                canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos - 8.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();

                                 // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                canvas.MoveTo(leftPos + 323f, finalTopPosition);       
                                canvas.LineTo(leftPos + 323f, finalTopPosition + 8.5f);      
                                canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + 323f, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos + 323f + 8.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop) + 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos - 8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                // Top Right
                                canvas.MoveTo(leftPos + 323f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 323f, (PageSize.A4.GetHeight() - bleedLineTop) + 8.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos + 323f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 323f + 8.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                foreach (var t in templateProperties)
                {

                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos);
                }
            }else if (templateSize == "HB")
            {
                document.GetPdfDocument().SetDefaultPageSize(PageSize.A4);
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                        var currentHeight = 215.433f + 17f + 10f;
                        float currentHeightObjects = 22.5f;
                        
                        //var leftPos = PageSize.A4.GetWidth() - (PixelsToPoints(ConvvertStringPixelsToInt("0"), 72));
                        var leftPos = 140.5f;
                        var leftPosPercent = 0f;
                        var finalTopPosition = 0f;
                        var finalTopPositionObject = 0f;
                        var myNum = 1;
                        var bleedLineTop = 10f + 8.5f;

        
                finalTopPosition = PageSize.A4.GetHeight() - currentHeight;
                finalTopPositionObject = PageSize.A4.GetHeight() - currentHeightObjects;
                

                 PdfCanvas canvas = new PdfCanvas(document.GetPdfDocument().GetPage(document.GetPdfDocument().GetNumberOfPages()));
                                  
                                // Bootom Left
                                canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(leftPos, finalTopPosition - 8.5f);       
                                // canvas.LineTo(leftPos, finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos - 20.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();

                                 // Bootom Right
                                canvas.SetLineWidth(0.5f);      
                                // canvas.MoveTo(leftPos + 354f, finalTopPosition - 8.5f);       
                                // canvas.LineTo(leftPos + 354f, finalTopPosition + 8.5f);      
                                // canvas.ClosePathStroke(); 

                                canvas.MoveTo(leftPos + 289.134f, finalTopPosition + 8.5f); 
                                canvas.LineTo(leftPos + 289.134f + 20.5f, finalTopPosition + 8.5f); 
                                canvas.ClosePathStroke();
                                
                                // Top Left
                                canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop) + 15.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos - 15.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                // Top Right
                                canvas.MoveTo(leftPos + 289.134f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 289.134f, (PageSize.A4.GetHeight() - bleedLineTop) + 15.5f); 
                                canvas.ClosePathStroke();

                                canvas.MoveTo(leftPos + 289.134f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.LineTo(leftPos + 289.134f + 15.5f, (PageSize.A4.GetHeight() - bleedLineTop)); 
                                canvas.ClosePathStroke();

                                if (backgroundImage != "null")
                                {
                                    try
                                    {
                                        var imageUrl = Directory.GetCurrentDirectory() + "\\StaticFiles\\Resources\\Images\\" + backgroundImage;
                                        Image img = new Image(ImageDataFactory.Create(imageUrl))
                                        .SetHeight(215.433f + 17f)
                                        .SetWidth(289.134f + 17f)
                                        .SetFixedPosition(leftPos - 8.5f, finalTopPosition);
                                        document.Add(img);
                                    }
                                    catch { };

                                }

                foreach (var t in templateProperties)
                {

                    document = resizeTextSmallBarker(document, t, currentHeightObjects, leftPos);
                }
            }
            document.GetPdfDocument().RemovePage(1);
            }
            catch(Exception e){
                Console.WriteLine(e.Message);
            }
            
            document.Close();
        }


        public int ConvertToPoints(int pixels)
        {
            return pixels * 96 / 72;
        }

        public static float PixelsToPoints(float value, int dpi)
        {
            return value / dpi * 72;
        }

        public void createPdf2(string html, string dest)
        {
            HtmlConverter.ConvertToPdf(html, new FileStream(dest, FileMode.Create));
        }

    }
}