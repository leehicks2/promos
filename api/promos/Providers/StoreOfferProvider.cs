using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.Extensions.Configuration;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers.Providers{
    public class StoreOfferProvider: IStoreOffer{
        private readonly IConfiguration _config;
        public StoreOfferProvider(IConfiguration config){
            _config = config;
        }
       public int Insert(int buyerOfferId, int userId){
            var dataSourceConnection = new DataSourceConnection(_config);
            var Id = 0;
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    //string sql = $"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, templateId, Amount, SizeAllocationId, LabelSizeId, AllocatorUserId, StatusId) OUTPUT INSERTED.ID values ('{storeOffer.BuyersOfferId}', '{storeOffer.StoreID}', '{storeOffer.TemplateId}', '{storeOffer.Amount}', '1', '1', '1','{storeOffer.StatusId}')";
                    string sql = $"[dbo].[AddStoreOffer]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@BuyersOfferID", buyerOfferId));
                        command.Parameters.Add(new SqlParameter("@AllocatorUserID", 1));
                        //command.ExecuteNonQuery();
                         using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                               // Console.WriteLine(Int32.Parse(reader["Id"].ToString()));
                               try{
                                    Id = Int32.Parse(reader["ID"].ToString());
                                }
                                catch(System.FormatException)
                                {
                                   Id = 111;
                                }
                                
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

             return Id;
        }

        public List<StoreOffer> UpsertMultiple(List<StoreOffer> storeOffers){

            for(var x=0; x < storeOffers.Count; x++){
                if(storeOffers[x].StoreOffersId == 0){
                    storeOffers[x].StoreOffersId = InsertStoreOffer(storeOffers[x]);
                }
                else{
                    UpdateStoreOffer(storeOffers[x]);
                }
            }

            return storeOffers;
        }   


        private int InsertStoreOffer(StoreOffer storeOffer){
            var dataSourceConnection = new DataSourceConnection(_config);
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = @$"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, templateId, Amount, SizeAllocationId, LabelSizeId, AllocatorUserId, StatusId) 
                    OUTPUT INSERTED.StoreOffersId values ('{storeOffer.BuyersOfferId}', '{storeOffer.StoreID}', '{storeOffer.TemplateId}', '{storeOffer.Amount}', '1', '1', '1','{storeOffer.StatusId}')";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        int id = (int)command.ExecuteScalar();
                        storeOffer.StoreOffersId = id;
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return storeOffer.StoreOffersId;
        }

        private int UpdateStoreOffer(StoreOffer storeOffer){
            var dataSourceConnection = new DataSourceConnection(_config);
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $"UPDATE dbo.StoreOffer set BuyersOfferId = '{storeOffer.BuyersOfferId}', StoreId = '{storeOffer.StoreID}', templateId = '{storeOffer.TemplateId}', Amount = '{storeOffer.Amount}', SizeAllocationId = '1', LabelSizeId = '1', AllocatorUserId = '1', StatusId= '{storeOffer.StatusId}' WHERE StoreOffersId = '{storeOffer.StoreOffersId}'";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return storeOffer.StoreOffersId;
        }

        public StoreOffer GetById(int id){
            return new StoreOffer();
        }

        public List<StoreOffer> GetAll(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreOffer>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $"SELECT * from dbo.StoreOffer";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new StoreOffer();
                                band.StoreOffersId = Int32.Parse(reader["StoreOffersId"].ToString());
                                band.StoreID =  Int32.Parse(reader["StoreId"].ToString());
                                band.BuyersOfferId =  Int32.Parse(reader["BuyersOfferId"].ToString());
                                band.StatusId =  Int32.Parse(reader["StatusId"].ToString());
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<StoreOffer> GetStoreOffersByBandId(int bandId){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<StoreOffer>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                    Select 
                    d.StoreID
                    ,d.StoreName
                    ,b.IslandName
                    ,c.name AS bandname
                    INTO #stores
                    FROM IslandBandMap a
                    INNER JOIN Island b ON a.IslandID=b.ID
                    INNER JOIN Band   c ON c.id=a.BandID
                    INNER JOIN store  d ON d.IslandBandMapID =a.ID
                    WHERE c.id = {bandId}

                    select z.StoreOffersID, z.BuyersOfferID, z.OfferTypeID, z.StatusId, z.StoreID, y.StoreName,
                    x.PromIsland, x.IslandBandMapID, x.CurrentRetail, x.CurrentCost, x.Savings
                    from StoreOffer z
                    inner join #stores y on y.StoreID = z.StoreID
                    inner join BuyerOffer x on x.BuyersOfferID = z.BuyersOfferID
                    where z.StatusId = 1

                    drop table #stores
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine(reader["StoreOffersId"]);
                                var band = new StoreOffer();
                                band.StoreOffersId = Int32.Parse(reader["StoreOffersId"].ToString());
                                band.StoreID =  Int32.Parse(reader["StoreID"].ToString());
                                band.StoreName =  reader["StoreName"].ToString();
                                band.BuyersOfferId =  Int32.Parse(reader["BuyersOfferId"].ToString());
                                band.StatusId =  Int32.Parse(reader["StatusId"].ToString());
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public int DeleteStoreOffer(int buyerOfferId){
            var dataSourceConnection = new DataSourceConnection(_config);
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    //string sql = $"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, templateId, Amount, SizeAllocationId, LabelSizeId, AllocatorUserId, StatusId) OUTPUT INSERTED.ID values ('{storeOffer.BuyersOfferId}', '{storeOffer.StoreID}', '{storeOffer.TemplateId}', '{storeOffer.Amount}', '1', '1', '1','{storeOffer.StatusId}')";
                    string sql = $"[dbo].[DeleteStoreOffer]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@BuyersOfferID", buyerOfferId));
                        command.ExecuteNonQuery();
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

             return buyerOfferId;
        }

        public int DeleteStoreOfferById(int id){
            var dataSourceConnection = new DataSourceConnection(_config);
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    //string sql = $"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, templateId, Amount, SizeAllocationId, LabelSizeId, AllocatorUserId, StatusId) OUTPUT INSERTED.ID values ('{storeOffer.BuyersOfferId}', '{storeOffer.StoreID}', '{storeOffer.TemplateId}', '{storeOffer.Amount}', '1', '1', '1','{storeOffer.StatusId}')";
                    string sql = $"DELETE FROM StoreOffer where StoreOffersID='{id}'";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

             return id;
        }



        public List<StoreOfferStatus> GetStoreOfferStatusAll(){
             var dataSourceConnection = new DataSourceConnection(_config);

            var returnList = new List<StoreOfferStatus>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $"SELECT * from dbo.StoreOfferStatus";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var storeOffStatus = new StoreOfferStatus();
                                storeOffStatus.id = Int32.Parse(reader["id"].ToString());
                                storeOffStatus.name =  reader["name"].ToString();
                                returnList.Add(storeOffStatus);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return returnList;
        }

        public List<PromoDisplayType> GetPromoDisplayType(){
             var dataSourceConnection = new DataSourceConnection(_config);

            var returnList = new List<PromoDisplayType>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $"SELECT * from dbo.PromoDisplayType";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var storeOffStatus = new PromoDisplayType();
                                storeOffStatus.Id = Int32.Parse(reader["id"].ToString());
                                storeOffStatus.Description =  reader["Description"].ToString();
                                storeOffStatus.Size =  reader["Size"].ToString();
                                returnList.Add(storeOffStatus);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return returnList;
        }

        public List<OfferType> GetOfferType(){
             var dataSourceConnection = new DataSourceConnection(_config);

            var returnList = new List<OfferType>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $"SELECT * from dbo.OfferType";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var storeOffStatus = new OfferType();
                                storeOffStatus.Id = Int32.Parse(reader["id"].ToString());
                                storeOffStatus.Name =  reader["Name"].ToString();
                                storeOffStatus.BandId =  Int32.Parse(reader["BandId"].ToString());
                                returnList.Add(storeOffStatus);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return returnList;
        }

        public List<OfferType> GetOfferTypeByBandId(int id){
             var dataSourceConnection = new DataSourceConnection(_config);

            var returnList = new List<OfferType>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $"SELECT * from dbo.OfferType where BandId = '{id}'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var storeOffStatus = new OfferType();
                                storeOffStatus.Id = Int32.Parse(reader["id"].ToString());
                                storeOffStatus.Name =  reader["Name"].ToString();
                                storeOffStatus.BandId =  Int32.Parse(reader["BandId"].ToString());
                                returnList.Add(storeOffStatus);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return returnList;
        }

        public OfferType InsertOfferType(OfferType storeOffer){
            var dataSourceConnection = new DataSourceConnection(_config);
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $"INSERT INTO dbo.OfferType (Name, BandId) OUTPUT INSERTED.ID values ('{storeOffer.Name}', '{storeOffer.BandId}')";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //command.ExecuteNonQuery();
                        int id = (int)command.ExecuteScalar();
                        storeOffer.Id = id;
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return storeOffer;
        }

        public List<OfferDisplayType> GetOfferDisplayTypesByOfferId(int id){
             var dataSourceConnection = new DataSourceConnection(_config);

            var returnList = new List<OfferDisplayType>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                        select dbo.OfferTypeDisplayTypes.id, Amount, dbo.Template.templateName AS description, dbo.Template.id AS templateId
                        from dbo.OfferTypeDisplayTypes
                        inner join dbo.Template on dbo.OfferTypeDisplayTypes.DisplayTypeId = dbo.Template.id
                        inner join dbo.OfferType on dbo.OfferTypeDisplayTypes.OfferTypeId = dbo.OfferType.id
                        where dbo.OfferType.id = {id}
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var storeOffStatus = new OfferDisplayType();
                                storeOffStatus.Id = Int32.Parse(reader["id"].ToString());
                                storeOffStatus.Amount =  Int32.Parse(reader["Amount"].ToString());
                                storeOffStatus.Description =  reader["Description"].ToString();
                                storeOffStatus.TemplateId = Int32.Parse(reader["templateId"].ToString());
                                returnList.Add(storeOffStatus);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return returnList;
        }

        public List<StoreOfferDisplayOverride> GetOfferDisplayTypesByOfferIdForStore(int id){
             var dataSourceConnection = new DataSourceConnection(_config);

            var returnList = new List<StoreOfferDisplayOverride>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                        select z.id,
                        a.StoreID,
                        CASE WHEN x.Amount IS NOT NULL THEN x.Amount ELSE z.Amount END AS FinalAmount
                        from dbo.OfferTypeDisplayTypes z
                        inner join dbo.Template on z.DisplayTypeId = dbo.Template.id
                        inner join dbo.OfferType on z.OfferTypeId = dbo.OfferType.id
                        right join StoreOfferTypeDisplayTypes x on x.OfferTypeDisplayTypeID = z.id
                        inner join Store a on a.StoreID = x.StoreId
                        where dbo.OfferType.id = {id}
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var storeOffStatus = new StoreOfferDisplayOverride();
                                storeOffStatus.Id = Int32.Parse(reader["id"].ToString());
                                storeOffStatus.StoreId =  Int32.Parse(reader["StoreId"].ToString());
                                storeOffStatus.FinalAmount =  Int32.Parse(reader["FinalAmount"].ToString());
                                returnList.Add(storeOffStatus);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return returnList;
        }

        public OfferDisplayTypeDb InsertOfferDisplayType(OfferDisplayTypeDb storeOffer){
            var dataSourceConnection = new DataSourceConnection(_config);
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $@"INSERT INTO dbo.OfferTypeDisplayTypes (DisplayTypeId, OfferTypeId, Amount) 
                    OUTPUT INSERTED.ID 
                    values ('{storeOffer.DisplayTypeId}', '{storeOffer.OfferTypeId}', '{storeOffer.Amount}')
                    ";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //command.ExecuteNonQuery();
                        int id = (int)command.ExecuteScalar();
                        storeOffer.Id = id;
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }


            // List of stores To Update
            List<int> offerDisplayTypeIds = new List<int>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $@"select id from OfferTypeDisplayTypes where OfferTypeId = '{storeOffer.OfferTypeId}'";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //command.ExecuteNonQuery();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                offerDisplayTypeIds.Add(Int32.Parse(reader["id"].ToString()));
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }


            var storeList = new List<int>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();   

                    var typeList = string.Join(",", offerDisplayTypeIds);    
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $@"select DISTINCT StoreId from StoreOfferTypeDisplayTypes where OfferTypeDisplayTypeID in({typeList})";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //command.ExecuteNonQuery();
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                storeList.Add(Int32.Parse(reader["StoreId"].ToString()));
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            

            foreach(var store in storeList){
                
                        try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();   

                    var typeList = string.Join(",", offerDisplayTypeIds);    
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    var sql = @$"
                        insert into StoreOfferTypeDisplayTypes
                        (OfferTypeDisplayTypeID,StoreID,Userid,Amount) 
                        OUTPUT INSERTED.StoreOfferTypeDisplayTypeId
                        VALUES
                        ('{storeOffer.Id}', '{store}', '1', '{storeOffer.Amount}')
                        ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            }


             return storeOffer;
        }

        public OfferDisplayType UpdateOfferDisplayType(OfferDisplayType storeOffer){
            var dataSourceConnection = new DataSourceConnection(_config);
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $@"UPDATE dbo.OfferTypeDisplayTypes SET
                    Amount = '{storeOffer.Amount}'
                    WHERE id = '{storeOffer.Id}'
                    ";
                   using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }                   

                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return storeOffer;
        }

        public OfferDisplayType DeleteOfferDisplayType(OfferDisplayType offerDisplaytype){
            var dataSourceConnection = new DataSourceConnection(_config);

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $@"DELETE from dbo.StoreOfferTypeDisplayTypes 
                     WHERE OfferTypeDisplayTypeID = '{offerDisplaytype.Id}'
                    ";
                   using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }                   

                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $@"DELETE from dbo.OfferTypeDisplayTypes 
                     WHERE id = '{offerDisplaytype.Id}'
                    ";
                   using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }                   

                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return offerDisplaytype;
        }


        public List<StoreOfferTypeDisplayTypesDb> GetOfferTypeDisplayTypesDb(int offerId, int storeId){
             var dataSourceConnection = new DataSourceConnection(_config);

            var offerTypesList = GetOfferDisplayTypesByOfferId(offerId);

            var offerTypeIds = offerTypesList.Select(o => o.Id).ToList();

            Console.WriteLine("######################");
             Console.WriteLine(offerTypeIds.Count);
             Console.WriteLine("######################");

            var returnList = new List<StoreOfferTypeDisplayTypesDb>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                        Select 
                        a.StoreOfferTypeDisplayTypeID
                        ,d.StoreId
                        ,a.Amount
                        ,c.DisplayTypeId
                        ,c.Amount AS ParentAmount
                        ,c.id AS OfferTypeDisplayTypesId
                        ,e.templateName
                        from StoreOfferTypeDisplayTypes a
                        INNER JOIN store  d on d.StoreID =a.StoreID
                        --inner join OfferType f on a.OfferTypeDisplayTypeID = f.id
                        INNER JOIN OfferTypeDisplayTypes c on a.OfferTypeDisplayTypeID =c.id
                        inner join dbo.Template e on c.DisplayTypeId = e.id
                        where a.OfferTypeDisplayTypeID in ({string.Join(',', offerTypeIds)}) and a.StoreID = '{storeId}'
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var storeOffStatus = new StoreOfferTypeDisplayTypesDb();
                                storeOffStatus.id = Int32.Parse(reader["StoreOfferTypeDisplayTypeId"].ToString());
                                storeOffStatus.Amount =  Int32.Parse(reader["Amount"].ToString());
                                storeOffStatus.StoreID =  Int32.Parse(reader["StoreId"].ToString());
                                storeOffStatus.OfferTypeDisplayTypeId =  Int32.Parse(reader["OfferTypeDisplayTypesId"].ToString());
                                storeOffStatus.TemplateName = reader["TemplateName"].ToString();
                                returnList.Add(storeOffStatus);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return returnList;
        }

        public List<StoreOfferTypeDisplayTypesDb> GetOfferTypeDisplayTypeStoreNameByOfferId(int offerId){
             var dataSourceConnection = new DataSourceConnection(_config);

            var offerTypesList = GetOfferDisplayTypesByOfferId(offerId);

            var offerTypeIds = offerTypesList.Select(o => o.Id).ToList();

            Console.WriteLine("######################");
             Console.WriteLine(offerTypeIds.Count);
             Console.WriteLine("######################");

            var returnList = new List<StoreOfferTypeDisplayTypesDb>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                        Select 
                        a.StoreOfferTypeDisplayTypeID
                        ,d.StoreId
                        ,d.StoreName
                        ,a.Amount
                        ,c.DisplayTypeId
                        ,c.Amount AS ParentAmount
                        ,c.id AS OfferTypeDisplayTypesId
                        ,e.templateName
                        from StoreOfferTypeDisplayTypes a
                        INNER JOIN store  d on d.StoreID =a.StoreID
                        --inner join OfferType f on a.OfferTypeDisplayTypeID = f.id
                        INNER JOIN OfferTypeDisplayTypes c on a.OfferTypeDisplayTypeID =c.id
                        inner join dbo.Template e on c.DisplayTypeId = e.id
                        where a.OfferTypeDisplayTypeID in ({string.Join(',', offerTypeIds)})
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var storeOffStatus = new StoreOfferTypeDisplayTypesDb();
                                storeOffStatus.id = Int32.Parse(reader["StoreOfferTypeDisplayTypeId"].ToString());
                                storeOffStatus.Amount =  Int32.Parse(reader["Amount"].ToString());
                                storeOffStatus.StoreID =  Int32.Parse(reader["StoreId"].ToString());
                                storeOffStatus.OfferTypeDisplayTypeId =  Int32.Parse(reader["OfferTypeDisplayTypesId"].ToString());
                                storeOffStatus.TemplateName = reader["TemplateName"].ToString();
                                storeOffStatus.StoreName = reader["StoreName"].ToString();
                                returnList.Add(storeOffStatus);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            List<StoreOfferTypeDisplayTypesDb> StoreNames = returnList.GroupBy(car => car.StoreName).Select(x=>x.FirstOrDefault()).ToList();
            return StoreNames;
        }

        public List<StoreOfferTypeDisplayTypesDb> InsertOfferTypeDisplayType(List<StoreOfferTypeDisplayTypesDb> storeOffers){
             var dataSourceConnection = new DataSourceConnection(_config);


            var returnList = new List<StoreOfferTypeDisplayTypesDb>();

            foreach(var ot in storeOffers){
                try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                        insert into StoreOfferTypeDisplayTypes
                        (OfferTypeDisplayTypeID,StoreID,Userid,Amount) 
                        OUTPUT INSERTED.StoreOfferTypeDisplayTypeId
                        VALUES
                        ('{ot.OfferTypeDisplayTypeId}', '{ot.StoreID}', '1', '{ot.Amount}')
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                            int id = (int)command.ExecuteScalar();
                            ot.id = id;
                            returnList.Add(ot);
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            }
            
            return returnList;
        }

        public StoreOfferTypeDisplayTypesDb UpdateOfferTypeDisplayType(StoreOfferTypeDisplayTypesDb storeOffers){
             var dataSourceConnection = new DataSourceConnection(_config);

                try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                        UPDATE  StoreOfferTypeDisplayTypes
                        SET
                        Amount = '{storeOffers.Amount}'
                        WHERE StoreOfferTypeDisplayTypeID = '{storeOffers.id}'
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                             command.ExecuteNonQuery();
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            
            return storeOffers;
        }

        //ORDERS
        public int InsertOrder(int bandId, int userId, int offerTypeId){
            var dataSourceConnection = new DataSourceConnection(_config);
            var Id = 0;
           try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    //string sql = $"INSERT INTO dbo.StoreOffer (BuyersOfferId, StoreId, templateId, Amount, SizeAllocationId, LabelSizeId, AllocatorUserId, StatusId) OUTPUT INSERTED.ID values ('{storeOffer.BuyersOfferId}', '{storeOffer.StoreID}', '{storeOffer.TemplateId}', '{storeOffer.Amount}', '1', '1', '1','{storeOffer.StatusId}')";
                    string sql = $"[dbo].[AddOrder]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@BandID", bandId));
                        command.Parameters.Add(new SqlParameter("@UserId", 1));
                        command.Parameters.Add(new SqlParameter("@OfferTypeID", offerTypeId));
                        //command.ExecuteNonQuery();
                         using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                               // Console.WriteLine(Int32.Parse(reader["Id"].ToString()));
                               try{
                                    //Id = Int32.Parse(reader["OrderID"].ToString());
                                    Id = 1;
                                }
                                catch(System.FormatException)
                                {
                                   Id = 2;
                                }
                                
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

             return Id;
        }

        public List<Order> GetOrders(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Order>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $"SELECT * from dbo.Orders where Statusid = 1002";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new Order();
                                band.OrderId = Int32.Parse(reader["OrderId"].ToString());
                                band.OrderNumber =  reader["OrderNumber"].ToString();
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<Order> GetOrdersComplated(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Order>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $"SELECT * from dbo.Orders where Statusid = 1003";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new Order();
                                band.OrderId = Int32.Parse(reader["OrderId"].ToString());
                                band.OrderNumber =  reader["OrderNumber"].ToString();
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<OrderDto> GetOrdersById(int id){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<OrderDto>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                    SELECT 
                    d.StoreID
                    ,d.StoreName
                    ,b.IslandName
                    ,c.name AS bandname
                    INTO #store
                    FROM IslandBandMap a
                    INNER JOIN Island b ON a.IslandID=b.ID
                    INNER JOIN Band   c ON c.id=a.BandID
                    INNER JOIN store  d ON d.IslandBandMapID =a.ID

                    SELECT c.OrderID,OrderNumber,OrderDate,
                    b.StoreID,bandname,IslandName,StoreName,
                    d.PromProdCode,PromDescription,
                    f.Name OfferTypeName ,g.id TemplateID,g.templateName ,
                    CASE WHEN h.Amount IS NOT NULL THEN h.Amount ELSE e.Amount END AS Amount,
                    j.name OrderStatus
                    --,e.Amount BandAmount,h.Amount StoreAmount
                    FROM StoreOffer a
                    INNER JOIN #store b ON a.StoreID = b.StoreID
                    INNER JOIN  Orders c ON c.OrderID = a.OrderID
                    INNER JOIN StoreOfferStatus j on j.id = c.StatusID
                    INNER JOIN BuyerOffer d on a.BuyersOfferID = d.BuyersOfferID
                    INNer JOIN OfferType f on f.id=a.OfferTypeID
                    INNER JOIN  OfferTypeDisplayTypes e ON e.OfferTypeId = a.OfferTypeID
                    INNER JOIN Template g on e.DisplayTypeId =g.id
                    LEFT JOIN  StoreOfferTypeDisplayTypes h on h.OfferTypeDisplayTypeID=e.id
                                                            AND h.StoreID =a.StoreID
                    WHERE c.OrderID = {id}
                    AND a.statusid = 4
                    order by b.StoreName


                    drop table #store";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // command.CommandType = CommandType.StoredProcedure;
                        // command.Parameters.Add(new SqlParameter("@OrderID", id));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new OrderDto();
                                band.OrderId = Int32.Parse(reader["OrderId"].ToString());
                                band.OrderNumber =  reader["OrderNumber"].ToString();
                                band.StoreId =  Int32.Parse(reader["StoreId"].ToString());
                                band.StoreName =  reader["StoreName"].ToString();
                                band.TemplateName = reader["TemplateName"].ToString();
                                band.PromDescription = reader["PromDescription"].ToString();
                                band.PromProdCode = reader["PromProdCode"].ToString();
                                band.IslandName = reader["IslandName"].ToString();
                                band.Amount =  Int32.Parse(reader["Amount"].ToString());
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<OrderDto> GetOrdersByBand(int id){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<OrderDto>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                    SELECT 
                    d.StoreID
                    ,d.StoreName
                    ,b.IslandName
                    ,c.name AS bandname
                    INTO #store
                    FROM IslandBandMap a
                    INNER JOIN Island b ON a.IslandID=b.ID
                    INNER JOIN Band   c ON c.id=a.BandID
                    INNER JOIN store  d ON d.IslandBandMapID =a.ID
                    WHERE c.id = {id}

                    SELECT c.OrderID,OrderNumber,OrderDate,
                    b.StoreID,bandname,IslandName,StoreName,
                    d.PromProdCode,PromDescription,
                    f.Name OfferTypeName ,g.id TemplateID,g.templateName ,
                    CASE WHEN h.Amount IS NOT NULL THEN h.Amount ELSE e.Amount END AS Amount,
                    j.name OrderStatus
                    --,e.Amount BandAmount,h.Amount StoreAmount
                    FROM StoreOffer a
                    INNER JOIN #store b ON a.StoreID = b.StoreID
                    INNER JOIN  Orders c ON c.OrderID = a.OrderID
                    INNER JOIN StoreOfferStatus j on j.id = c.StatusID
                    INNER JOIN BuyerOffer d on a.BuyersOfferID = d.BuyersOfferID
                    INNer JOIN OfferType f on f.id=a.OfferTypeID
                    INNER JOIN  OfferTypeDisplayTypes e ON e.OfferTypeId = a.OfferTypeID
                    INNER JOIN Template g on e.DisplayTypeId =g.id
                    LEFT JOIN  StoreOfferTypeDisplayTypes h on h.OfferTypeDisplayTypeID=e.id
                                                            AND h.StoreID =a.StoreID
                    where  c.Statusid = 1002
                    order by b.StoreName


                    drop table #store";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // command.CommandType = CommandType.StoredProcedure;
                        // command.Parameters.Add(new SqlParameter("@OrderID", id));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new OrderDto();
                                band.OrderId = Int32.Parse(reader["OrderId"].ToString());
                                band.OrderNumber =  reader["OrderNumber"].ToString();
                                band.StoreId =  Int32.Parse(reader["StoreId"].ToString());
                                band.StoreName =  reader["StoreName"].ToString();
                                band.TemplateName = reader["TemplateName"].ToString();
                                band.PromDescription = reader["PromDescription"].ToString();
                                band.PromProdCode = reader["PromProdCode"].ToString();
                                band.IslandName = reader["IslandName"].ToString();
                                band.Amount =  Int32.Parse(reader["Amount"].ToString());
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<PrinterDto> GetPrinters(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<PrinterDto>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"SELECT * FROM Printers";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // command.CommandType = CommandType.StoredProcedure;
                        // command.Parameters.Add(new SqlParameter("@OrderID", id));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new PrinterDto();
                                band.id = Int32.Parse(reader["Id"].ToString());
                                band.Name =  reader["Name"].ToString();
                                band.Location = reader["Location"].ToString();
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

         public List<ReportDto> GetReports(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<ReportDto>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"SELECT * FROM Reports";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        // command.CommandType = CommandType.StoredProcedure;
                        // command.Parameters.Add(new SqlParameter("@OrderID", id));

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new ReportDto();
                                band.id = Int32.Parse(reader["Id"].ToString());
                                band.Name =  reader["Name"].ToString();
                                band.Location = reader["Location"].ToString();
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }
        
    }
}