using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers.Providers{
    public class BandProvider: IBand{
        private readonly IConfiguration _config;
        public BandProvider(IConfiguration config){
            _config = config;
        }
       public Band GetBandById(int id){
           return new Band();
        }

        public List<Band> GetAllBands(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Band>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = @$"select * from Band where id<>4";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new Band();
                                band.id = Int32.Parse(reader["id"].ToString());
                                band.name =  reader["name"].ToString();
                                band.BandLetter =  reader["BandLetter"].ToString();
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }
        
        public Band Insert(Band obj){
             return new Band();
        }
        public Band Update(Band obj){
             return new Band();
        }
    }
}