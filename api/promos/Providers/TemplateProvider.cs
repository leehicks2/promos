using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers.Providers{
    public class TemplateProvider: ITemplate{
        private readonly IConfiguration _config;
        DataSourceConnection dataSourceConnection;

        public TemplateProvider(IConfiguration config){
            _config = config;
           dataSourceConnection = new DataSourceConnection(_config);
        }

        public  Template GetTemplateById(int id){
            Template responseTemplate = new Template();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"select dbo.Template.id, backgroundImage, templateName, BandId, templateType, Width, Height, Description, dbo.PromoDisplayType.Size from dbo.Template inner join dbo.PromoDisplayType on dbo.Template.templateType = dbo.PromoDisplayType.id where dbo.Template.id = {id}";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new Template();
                                template.id = Int32.Parse(reader["id"].ToString());
                                template.BackgroundImage = reader["BackgroundImage"].ToString();
                                template.TemplateName = reader["templateName"].ToString();
                                template.templateType = Int32.Parse(reader["templateType"].ToString());
                                template.BandId = Int32.Parse(reader["BandId"].ToString());
                                template.Width = reader["Width"].ToString();
                                template.Height = reader["Height"].ToString();
                                template.SizeDescription = reader["Size"].ToString();
                                responseTemplate = template;
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return responseTemplate;
        } 

        public  List<Template> GetTemplateByBandAndSize(string bandLetter, int sizeId){
            List<Template> responseTemplate = new List<Template>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = @$"SELECT * FROM Template a
                                inner join Band b on b.id = a.BandId
                                inner join PromoDisplayType c on c.id = a.templateType
                                where b.BandLetter = '{bandLetter}' and c.id = {sizeId}";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new Template();
                                template.id = Int32.Parse(reader["id"].ToString());
                                template.BackgroundImage = reader["BackgroundImage"].ToString();
                                template.TemplateName = reader["templateName"].ToString();
                                template.templateType = Int32.Parse(reader["templateType"].ToString());
                                template.BandId = Int32.Parse(reader["BandId"].ToString());
                                template.Width = reader["Width"].ToString();
                                template.Height = reader["Height"].ToString();
                                template.SizeDescription = reader["Description"].ToString();
                                responseTemplate.Add(template);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return responseTemplate;
        } 

        public List<Template> GetAllTemplates(){
            List<Template> templateList = new List<Template>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"select dbo.Template.id, backgroundImage, templateName, BandId, templateType, Width, Height, Description from dbo.Template inner join dbo.PromoDisplayType on dbo.Template.templateType = dbo.PromoDisplayType.id";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new Template();
                                template.id = Int32.Parse(reader["id"].ToString());
                                template.BackgroundImage = reader["BackgroundImage"].ToString();
                                template.TemplateName = reader["templateName"].ToString();
                                template.templateType = Int32.Parse(reader["templateType"].ToString());
                                template.BandId = Int32.Parse(reader["BandId"].ToString());
                                template.Width = reader["Width"].ToString();
                                template.Height = reader["Height"].ToString();
                                template.SizeDescription = reader["Description"].ToString();
                                templateList.Add(template);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return templateList;
        }

        public List<Template> GetTemplatesByBand(int id){
            List<Template> templateList = new List<Template>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"select dbo.Template.id, backgroundImage, templateName, BandId, templateType, Width, Height, Description from dbo.Template inner join dbo.PromoDisplayType on dbo.Template.templateType = dbo.PromoDisplayType.id where BandId = {id}";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new Template();
                                template.id = Int32.Parse(reader["id"].ToString());
                                template.BackgroundImage = reader["BackgroundImage"].ToString();
                                template.TemplateName = reader["templateName"].ToString();
                                template.templateType = Int32.Parse(reader["templateType"].ToString());
                                template.BandId = Int32.Parse(reader["BandId"].ToString());
                                template.Width = reader["Width"].ToString();
                                template.Height = reader["Height"].ToString();
                                template.SizeDescription = reader["Description"].ToString();
                                templateList.Add(template);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return templateList;
        }

        public  List<Template> GetById(List<int> ids){
            return new List<Template>();
        }

         public Template Insert(Template template){
             try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $"INSERT INTO dbo.Template (backgroundImage, templateName, BandId, templateType) OUTPUT INSERTED.ID values ('{template.BackgroundImage}', '{template.TemplateName}', '{template.BandId}', '{template.templateType}')";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //command.ExecuteNonQuery();
                        int id = (int)command.ExecuteScalar();
                        template.id = id;
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return template;
         }

         public Template Update(Template template){
             try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                   // string sql = $"INSERT INTO dbo.Template (bandId, locationId, backgroundImage, size) OUTPUT INSERTED.ID values ('{template.BandId}', '{template.LocationId}', '{template.BackgroundImage}', '{template.Size}')";
                   string sql = $"UPDATE dbo.Template SET backgroundImage = '{template.BackgroundImage}' WHERE id='{template.id}'";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return template;
         }

         public List<TemplatePack> GetTemplatePacks(){
            List<TemplatePack> templateList = new List<TemplatePack>();

            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"select * from dbo.TemplatePacks";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var template = new TemplatePack();
                                template.id = Int32.Parse(reader["id"].ToString());
                                template.Name = reader["name"].ToString();
                                templateList.Add(template);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return templateList;
         }

         public TemplatePack InsertTemplatePack(TemplatePack template){
             try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    //string sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";
                    string sql = $"INSERT INTO dbo.TemplatePack (name) OUTPUT INSERTED.ID values ('{template.Name}')";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //command.ExecuteNonQuery();
                        int id = (int)command.ExecuteScalar();
                        template.id = id;
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
             return template;
         }

         public List<TemplateDisplay> GettemplateDisplay(int storeId, int bandId, int templateId){

             var returnList = new List<TemplateDisplay>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"[dbo].[GetTemplateInfo]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@StoreId", storeId));
                        command.Parameters.Add(new SqlParameter("@band", bandId));
                        command.Parameters.Add(new SqlParameter("@templateId", templateId));
                        command.Parameters.Add(new SqlParameter("@Branchid", 1));
                        command.Parameters.Add(new SqlParameter("@LabelSizeID", 1));
                         using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var display = new TemplateDisplay();
                               // Console.WriteLine(Int32.Parse(reader["Id"].ToString()));
                                display.TemplatePropertiesId = Int32.Parse(reader["TemplatePropertiesId"].ToString());
                                display.DisplayText = reader["FieldValue"].ToString();
                                returnList.Add(display);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return returnList;
        }

        public List<TemplateDisplay> GettemplateDisplayByBuyerAndTemplate(int buyerOfferId, int templateId){

             var returnList = new List<TemplateDisplay>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       
                    string sql = $"[dbo].[GetTemplateInfoByBuyer]";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@BuyerOffersId", buyerOfferId));
                        command.Parameters.Add(new SqlParameter("@TemplateId", templateId));

                         using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var display = new TemplateDisplay();
                               // Console.WriteLine(Int32.Parse(reader["Id"].ToString()));
                                display.TemplatePropertiesId = Int32.Parse(reader["TemplatePropertiesId"].ToString());
                                display.DisplayText = reader["FieldValue"].ToString();
                                returnList.Add(display);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            return returnList;
        }
    }
}
