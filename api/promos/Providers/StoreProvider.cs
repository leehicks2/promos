using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers.Providers{
    public class StoreProvider: IStore{
        private readonly IConfiguration _config;
        public StoreProvider(IConfiguration config){
            _config = config;
        }

       public Store GetStoreById(int id){
           return new Store();
        }

        public List<Store> GetAllStores(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Store>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                    Select 
                    d.StoreID
                    ,d.StoreName
                    from IslandBandMap a
                    INNER JOIN Island b on a.IslandID=b.ID
                    INNER JOIN Band   c on c.id=a.BandID
                    INNER JOIN store  d on d.IslandBandMapID =a.ID
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var store = new Store();
                                store.StoreID = Int32.Parse(reader["StoreID"].ToString());
                                store.StoreName =  reader["StoreName"].ToString();
                                bandList.Add(store);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

         public List<Store> GetStoreBandIdId(int id){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Store>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                    Select 
                    d.StoreID
                    ,d.StoreName
                    from IslandBandMap a
                    INNER JOIN Island b on a.IslandID=b.ID
                    INNER JOIN Band   c on c.id=a.BandID
                    INNER JOIN store  d on d.IslandBandMapID =a.ID
                    where a.bandid= '{id}'
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new Store();
                                band.StoreID = Int32.Parse(reader["StoreId"].ToString());
                                band.StoreName =  reader["StoreName"].ToString();
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<Store> GetStoreBandAndIsland(int id, int islandId){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Store>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                    Select 
                    d.StoreID
                    ,d.StoreName
                    from IslandBandMap a
                    INNER JOIN Island b on a.IslandID=b.ID
                    INNER JOIN Band   c on c.id=a.BandID
                    INNER JOIN store  d on d.IslandBandMapID =a.ID
                    where a.bandid= '{id}' and a.IslandID = {islandId}
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var band = new Store();
                                band.StoreID = Int32.Parse(reader["StoreId"].ToString());
                                band.StoreName =  reader["StoreName"].ToString();
                                bandList.Add(band);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<Island> GetAllIslands(){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Island>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                    Select 
                    b.IslandName
                    ,a.islandid
                    from IslandBandMap a
                    INNER JOIN Island b on a.IslandID=b.ID
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var island = new Island();
                                island.IslandId = Int32.Parse(reader["islandId"].ToString());
                                island.IslandName =  reader["IslandName"].ToString();
                                bandList.Add(island);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }

        public List<Island> GetIslandsByBandId(int id){
            var dataSourceConnection = new DataSourceConnection(_config);

            var bandList = new List<Island>();
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();       

                    String sql = $@"
                    Select 
                    b.IslandName
                    ,a.islandid
                    from IslandBandMap a
                    INNER JOIN Island b on a.IslandID=b.ID
                    INNER JOIN Band c on c.id = a.BandID
                    where c.id= '{id}'
                    ";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                var island = new Island();
                                island.IslandId = Int32.Parse(reader["islandId"].ToString());
                                island.IslandName =  reader["IslandName"].ToString();
                                bandList.Add(island);
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return bandList;
        }
    }
}