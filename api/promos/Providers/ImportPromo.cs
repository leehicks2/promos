
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

namespace promos.Controllers.Providers{
    public class ImportPromo: IImportPromo{


        private readonly IConfiguration _config;

        public ImportPromo(IConfiguration config){
            _config = config;
        }
        private List<TemoPromoFile> TemoList(DataTable temoPromo, string fileName){
            var temoList = new List<TemoPromoFile>();
            var count  = 0; 
                foreach(DataRow row in temoPromo.Rows)
                {
                    if(row["ProductCode"].ToString() != null && row["ProductCode"].ToString() != ""){
                        var temoTempFile = new TemoPromoFile();
                        temoTempFile.PromProdCode = row["ProductCode"].ToString().Replace("'","#12345!");
                        temoTempFile.PromDescription = row["Description"].ToString().Replace("'","#12345!");
                        temoTempFile.PromStartRetDate = row["PromStartDt"].ToString().Replace("'","#12345!");
                        temoTempFile.PromEndRetDate = row["PromEndDt"].ToString().Replace("'","#12345!");
                        temoTempFile.PromIsland = row["D"].ToString().Replace("'","#12345!");
                        temoTempFile.PrBand = row["B"].ToString().Replace("'","#12345!");
                        temoTempFile.PromRetail = row["NormPrc"].ToString().Replace("'","#12345!");
                        temoTempFile.CurrentCost = row["PromPrice"].ToString().Replace("'","#12345!");
                        temoTempFile.PromMessage =  row["PromotionMessage"].ToString().Replace("'","#12345!");
                        temoTempFile.SuppProdCode =  row["SuppProdCode"].ToString().Replace("'","#12345!");
                        temoTempFile.proddescrip =  row["Slogan"].ToString().Replace("'","#12345!");
                        temoTempFile.PromDeptDesc =  row["DeptDesc"].ToString().Replace("'","#12345!");
                        temoTempFile.Size =  row["Size"].ToString().Replace("'","#12345!");
                        temoTempFile.Slogan =  row["Slogan"].ToString().Replace("'","#12345!");
                        temoTempFile.DeptDesc =  row["DeptDesc"].ToString().Replace("'","#12345!");
                        temoTempFile.Savings =  row["Savings"].ToString().Replace("'","#12345!");
                        temoTempFile.MultibuyPrice =  row["Iceland/Morrisons Multibuy Price"].ToString().Replace("'","#12345!");
                        temoTempFile.MultibuyQty =  row["Morrisons Multibuy Qty"].ToString().Replace("'","#12345!");
                        temoTempFile.GenericDescription =  row["Morrisons Generic Description"].ToString().Replace("'","#12345!");
                        temoList.Add(temoTempFile);
                        count += 1;
                    }

                    Console.WriteLine("################### NUMBER OF ROW #######################3");
                     Console.WriteLine(count);
                    
                }
                return temoList;
        }

        public DataTable ToDataTable<T>(List<T> items)  
        {  
            DataTable dataTable = new DataTable(typeof(T).Name);  
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);  
            foreach (PropertyInfo prop in Props)  
            {  
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);  
            }  
            foreach (T item in items)  
            {  
                var values = new object[Props.Length];  
                for (int i = 0; i < Props.Length; i++)  
                {  
                     
                    values[i] = Props[i].GetValue(item, null);  
                }  
                dataTable.Rows.Add(values);  
            }  
              
            return dataTable;  
        }   

        public bool Import(DataTable temoPromo, string filename){

            var dataSourceConnection = new DataSourceConnection(_config);

            var temoList = TemoList(temoPromo, filename);
    
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    
                    connection.Open();   

                foreach(var temo in temoList){
                    try{
                         String sql = $"insert into dbo.TemoPromoFile (CurrentCost,CurrentRetail, PrBand, proddescrip, PromCost, PromDeptDesc, PromDescription, PromEndCostDate, PromEndRetDate, PromIsland, PromMessage, PromProdCode, PromRecordId, PromRetail, PromStartCostDate,PromStartRetDate, PromSupp, SuppProdCode, Size, Savings, Slogan, DeptDesc, MultibuyPrice, MultibuyQty, GenericDescription) values('{temo.CurrentCost}', '{temo.CurrentRetail}','{temo.PrBand}','{temo.proddescrip}','{temo.PromCost}','{temo.PromDeptDesc}','{temo.PromDescription}','{temo.PromEndCostDate}', '{temo.PromEndRetDate}','{temo.PromIsland}','{temo.PromMessage}','{temo.PromProdCode}', '{temo.PromRecordId}', '{temo.PromRetail}', '{temo.PromStartCostDate}','{temo.PromStartRetDate}','{temo.PromSupp}','{temo.SuppProdCode}','{temo.Size}','{temo.Savings}','{temo.Slogan}','{temo.DeptDesc}','{temo.MultibuyPrice}','{temo.MultibuyQty}','{temo.GenericDescription}');";
                    Console.WriteLine(sql); 
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                    Console.WriteLine("Complete"); 
                    }catch(Exception e){
                        Console.WriteLine(e);
                    }
                    
                }     
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return true;
        }

        public bool CallProc(string filename, string fileSize, int userId){

            var dataSourceConnection = new DataSourceConnection(_config);
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    Console.WriteLine("################### FILE SIZE ##################3333");
                    Console.WriteLine(fileSize);
                    
                    connection.Open();   
                    using (SqlCommand command = new SqlCommand("dbo.usp_FileImport", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add(new SqlParameter("@FileName", filename));
                        command.Parameters.Add(new SqlParameter("@Filesize", fileSize));
                        command.Parameters.Add(new SqlParameter("@UserID", userId));
                        command.ExecuteNonQuery();
                    }
                    Console.WriteLine("Complete");     
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return true;
        }

        public bool MapWorkSheetToTemoPromo(string filePath, string fileName){

          DataTable db = ReadExcelFile("sheet1",filePath); 
           
           Import(db, fileName);
           
            return true;
        }

        private DataTable ReadExcelFile(string sheetName, string path)
        {

            using (OleDbConnection conn = new OleDbConnection())
            {
                DataTable dt = new DataTable();
                string Import_FileName = path;
                string fileExtension = Path.GetExtension(Import_FileName);
                if (fileExtension == ".xls")
                //Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\!SQLmail\Downloads\test3.xls;Extended Properties="EXCEL 8.0;HDR=YES";
                    conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
                if (fileExtension == ".xlsx")
                    conn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";
                using (OleDbCommand comm = new OleDbCommand())
                {
                    comm.CommandText = "Select * from [" + sheetName + "$]";
                    comm.Connection = conn;
                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        da.SelectCommand = comm;
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
        }

        public bool UpdatePromos(List<BuyerOffer> promos){
             var dataSourceConnection = new DataSourceConnection(_config);
            try{
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

               builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    
                    connection.Open();   

                foreach(var promo in promos){

                    String sql = @$"
                    Update dbo.BuyerOffer set 
                    PromDescription='{promo.PromDescription.Replace("'", "''")}',
                    Size = '{promo.Size.Replace("'", "''")}',
                    PromRetail = '{promo.PromRetail.Replace("'", "''")}',
                    CurrentCost = '{promo.CurrentCost.Replace("'", "''")}',
                    Savings = '{promo.Savings.Replace("'", "''")}',
                    PromMessage = '{promo.PromMessage.Replace("'", "''")}',
                    PromDeptDesc = '{promo.PromDeptDesc.Replace("'", "''")}',
                    Slogan = '{promo.Slogan.Replace("'", "''")}',
                    MultibuyPrice = '{promo.MultibuyPrice.Replace("'", "''")}',
                    MultibuyQty = '{promo.MultibuyQty.Replace("'", "''")}',
                    GenericDescription = '{promo.GenericDescription.Replace("'", "''")}',
                    SuppProdCode = '{promo.SuppProdCode.Replace("'", "''")}'
                    where BuyersOfferID = {promo.BuyersOfferID}";

                    Console.WriteLine(sql); 
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                    Console.WriteLine("Complete");  
                }     
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            return true;
        }

    }
}