using System.Collections.Generic;
using promos.Controllers.Interfaces;
using promos.Controllers.Models;

using System.Data.SqlClient;
using System;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace promos.Controllers.Providers{
    public class Login: ILogin{

        private readonly IConfiguration _config;

        public Login(IConfiguration config){
            _config = config;
        }

        public AuthResponseModel login(LoginModel loginModel){
            var user = new User();
            var authModel = new AuthResponseModel();
            var dataSourceConnection = new DataSourceConnection(_config);
         

            try{
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = dataSourceConnection.DataSource; 
                builder.UserID = dataSourceConnection.UserId;            
                builder.Password = dataSourceConnection.Password;     
                builder.InitialCatalog = dataSourceConnection.InitialCatalog;
         
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    
                    
                    connection.Open();       

                    String sql = $"SELECT * from dbo.SystemUser where UserName = '{loginModel.Username}' and UserPassword = '{loginModel.Password}'";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                user.Userid = Int32.Parse(reader["UserId"].ToString());
                                user.UserName = reader["UserName"].ToString();
                                user.DateAdded = Convert.ToDateTime(reader["DateAdded"]);
                                user.UserPassword = reader["UserPassword"].ToString();
                                user.AccessLevelID = Int32.Parse(reader["AccessLevelId"].ToString());
                                user.StoreId = Int32.Parse(reader["StoreId"].ToString());
                                user.UserEmail = reader["UserEmail"].ToString();
                            }
                        }
                    }                    
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }

            if(user.UserName != null){
                authModel.User = user;
                authModel.Authed = true;
            }else{
                authModel.Authed = false;
            }
            Console.WriteLine(JsonConvert.SerializeObject(authModel));
        return authModel;
     }
    };
} 