USE Promos
GO
CREATE PROCEDURE LoginSystemUser
@UserName		 NVARCHAR(200) ,
@UserPassword	 NVARCHAR(50)  NULL

AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to insert and update Users to the system
*/
BEGIN

  SELECT 'True' Authorized,UserName,UserPassword,AccessLevelID,StoreID,UserEmail
  FROM SystemUser
  WHERE UserName		= @UserName	
  AND   UserPassword	= @UserPassword	;
  IF @@ROWCOUNT = 0
  BEGIN
     Select 'False' Authorized,NULL UserName,NULL UserPassword,NULL AccessLevelID,NULL StoreID,NULL UserEmail
  END

END;
GO
Exec  LoginSystemUser 'username','leeh'

CREATE TABLE EmailGroup
(
EmailGroupID INT NOT NULL IDENTITY(1,1) PRIMARY KEY CLUSTERED,
EmailGroupName NVARCHAR(200) NOT NULL ,
DateCreated DATETIME DEFAULT GETDATE()
)
GO
CREATE PROCEDURE UpsertEmailGroup
@EmailGroupID   INT NULL,
@EmailGroupName NVARCHAR(200)
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to insert and update EmailGroup
*/
BEGIN
  UPDATE dbo.EmailGroup
  SET EmailGroupName = @EmailGroupName
  WHERE EmailGroupID = @EmailGroupID;

  IF @@ROWCOUNT=0
  BEGIN
   INSERT INTO dbo.EmailGroup
   (EmailGroupName)
   SELECT @EmailGroupName;
  END

  SELECT EmailGroupID,EmailGroupName,DateCreated FROM dbo.EmailGroup WHERE EmailGroupName = @EmailGroupName
END;
GO

EXEC UpsertEmailGroup
 @EmailGroupID   = NULL
,@EmailGroupName ='Buyer'
GO
CREATE PROCEDURE GETEmailGroup
@EmailGroupID   INT NULL
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to Get EmailGroup
*/
BEGIN
  SELECT EmailGroupID,EmailGroupName,DateCreated 
  FROM dbo.EmailGroup 
  WHERE EmailGroupID = CASE WHEN @EmailGroupID IS NULL THEN EmailGroupID ELSE @EmailGroupID END
END;
GO
EXEC GETEmailGroup
 @EmailGroupID   = NULL

GO

CREATE TABLE EmailList
(
EmailListID INT NOT NULL IDENTITY(1,1) PRIMARY KEY CLUSTERED,
EmailGroupID INT NOT NULL FOREIGN KEY REFERENCES EmailGroup(EmailGroupID),
EmailSubject NVARCHAR(200) NOT NULL ,
EmailBody NVARCHAR(2000) NOT NULL,
DateCreated Datetime default getdate()
)
GO
CREATE PROCEDURE UpsertEmailList
@EmailListID   INT NULL,
@EmailGroupID  INT,
@EmailSubject  VARCHAR(200),
@EmailBody     VARCHAR(2000)
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to insert and update EmailList
*/
BEGIN
  UPDATE dbo.EmailList
  SET EmailGroupID = @EmailGroupID,
      EmailSubject  = @EmailSubject,  
      EmailBody     = @EmailBody     
  WHERE EmailListID = @EmailListID;


  IF @@ROWCOUNT=0
  BEGIN
   INSERT INTO dbo.EmailList
   (EmailGroupID,EmailSubject,EmailBody)
   SELECT @EmailGroupID,@EmailSubject,@EmailBody;
  END

  SELECT EmailListID,EmailGroupID,EmailSubject,EmailBody FROM dbo.EmailList WHERE EmailListID = ISNULL(@EmailListID,SCOPE_IDENTITY())
END;

GO

EXEC UpsertEmailList
@EmailListID   =NULL,
@EmailGroupID  =1,
@EmailSubject  ='2',
@EmailBody     ='1'



GO
CREATE PROCEDURE GETEmailList
@EmailListID   INT NULL
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to Get EmailList
*/
BEGIN
  SELECT EmailListID,EmailGroupID,EmailSubject,EmailBody
  FROM dbo.EmailList 
  WHERE EmailListID = CASE WHEN @EmailListID IS NULL THEN EmailListID ELSE @EmailListID END
END;
GO
EXEC GETEmailList
 @EmailListID   = 1

GO

CREATE TABLE EmailGroupUserMap
(
Userid INT NOT NULL FOREIGN KEY REFERENCES SystemUser(Userid),
EmailGroupID INT NOT NULL FOREIGN KEY REFERENCES EmailGroup(EmailGroupID),
CONSTRAINT pk_EmailGroupUserMap PRIMARY KEY (Userid,EmailGroupID)
)
GO
CREATE PROCEDURE UpsertEmailGroupUserMap
@UserID   INT ,
@EmailGroupID  INT
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to insert and update EmailGroupUserMap
*/
BEGIN

   INSERT INTO dbo.EmailGroupUserMap
   (UserID,EmailGroupID)
   SELECT @UserID,@EmailGroupID
   WHERE NOT EXISTS (SELECT 1 FROM EmailGroupUserMap WHERE userid = @UserID and EmailGroupID=@EmailGroupID)

END;

GO

EXEC UpsertEmailGroupUserMap
@UserID   =1,
@EmailGroupID  =1

GO

-- procs
Create table Field
(  id int IDENTITY(1,1) PRIMARY KEY,
 FieldName varchar (255))
 GO
Insert into Field
(FieldName)
 SELECT 'PromRecordId'
 UNION ALL SELECT 'PromProdCode'
 UNION ALL SELECT 'PromIsland'
 UNION ALL SELECT 'PrBand'
 UNION ALL SELECT 'PromCode'
 UNION ALL SELECT 'PromDescription'
 UNION ALL SELECT 'PromSupp'
 UNION ALL SELECT 'PromStartRetDate'
 UNION ALL SELECT 'PromEndRetDate'
 UNION ALL SELECT 'PromStartCostDate'
 UNION ALL SELECT 'PromEndCostDate'
 UNION ALL SELECT 'CurrentCost'
 UNION ALL SELECT 'PromCost'
 UNION ALL SELECT 'PromMessage'
 UNION ALL SELECT 'CurrentRetail'
 UNION ALL SELECT 'PromRetail'
 UNION ALL SELECT 'SuppProdCode'
 UNION ALL SELECT 'HostedInd'
 UNION ALL SELECT 'PromDeptDesc'
 UNION ALL SELECT 'proddescrip'
 GO


 CREATE PROCEDURE UpsertField
@FieldID   INT ,
@FieldName varchar(200)
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to insert and update EmailGroupUserMap
*/
BEGIN

  UPDATE dbo.FielD
  SET FieldName = @FieldName 
  WHERE ID = @FieldID;


  IF @@ROWCOUNT=0
  BEGIN
   INSERT INTO dbo.Field
   (FieldName)
   SELECT @FieldName;
  END

  SELECT id,FieldName FROM dbo.Field WHERE id = ISNULL(@FieldID,SCOPE_IDENTITY())

END;

GO

EXEC UpsertField
@FieldID   =21,
@FieldName  ='Test'

GO


CREATE PROCEDURE GETField
@FieldID   INT NULL
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to Get Field
*/
BEGIN
  SELECT id,FieldName FROM dbo.Field WHERE id = CASE WHEN @FieldID IS NULL THEN id ELSE @FieldID END
END;
GO
-- GET the field based on id or null for all fields
EXEC GETField
 @FieldID   = 1

GO

ALTER TABLE TemplateProperties
ADD Fieldid INT  NULL FOREIGN KEY REFERENCES FIELD(ID)

GO
sp_rename 'PromoType','PromoSize'
GO
Alter table PromoSize
Add Width varchar(50)
GO
Alter table PromoSize
Add Height varchar(50)
GO
Alter table PromoSize
Add Pixel varchar(50)
GO
CREATE PROCEDURE Upserttemplate
@templateID   INT ,
@size varchar(200),
@backgroundImage varchar(200)
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to insert and update EmailGroupUserMap
*/
BEGIN

  UPDATE dbo.template
  SET 
  size			  = @size,
  backgroundImage = @backgroundImage
  WHERE ID = @templateID;

  IF @@ROWCOUNT=0
  BEGIN
   INSERT INTO dbo.template
   (size,backgroundImage)
   SELECT @size,@backgroundImage;
  END

  SELECT id,size,backgroundImage FROM dbo.template WHERE id = ISNULL(@templateID,SCOPE_IDENTITY())

END;

GO

EXEC Upserttemplate
@templateID   =NULL,
@size ='A5',
@backgroundImage ='1'

GO

CREATE PROCEDURE GETtemplate
@templateID   INT NULL
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to Get template
*/
BEGIN
  SELECT id,size,backgroundImage FROM dbo.template WHERE id = CASE WHEN @templateID IS NULL THEN id ELSE @templateID END
END;
GO
EXEC GETtemplate
 @templateID   = NULL

GO


 CREATE TABLE TemplateMap
(
TemplateMapID INT IDENTITY(1,1) PRIMARY KEY CLUSTERED,
StoreID INT  NULL ,
Branchid INT  NULL ,
Band INT  NULL ,
LabelSizeID INT NOT NULL FOREIGN KEY REFERENCES PromoSize(PromoTypeID),
templateId int NOT NULL FOREIGN KEY REFERENCES Template(id)
)
GO
CREATE PROCEDURE GETTemplateMap
@StoreID		    INT
,@Branchid		    INT
,@Band			    INT
,@LabelSizeID	    INT
,@templateId		INT

AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to Get TemplateMap
*/
BEGIN
  SELECT TemplateMapID,StoreID,Branchid,Band,LabelSizeID,templateId 
  FROM dbo.TemplateMap 
  WHERE 
  (Storeid = CASE WHEN @StoreID IS NULL THEN Storeid ELSE @StoreID END) AND
  (Branchid = CASE WHEN @Branchid IS NULL THEN Branchid ELSE @Branchid END) AND
  (Band = CASE WHEN @Band IS NULL THEN Band ELSE @Band END) AND
  (LabelSizeID = CASE WHEN @LabelSizeID IS NULL THEN LabelSizeID ELSE @LabelSizeID END) AND
  (templateId = CASE WHEN @templateId IS NULL THEN templateId ELSE @templateId END)
END;
GO
EXEC GETTemplateMap
@StoreID		= NULL 
,@Branchid		= NULL  
,@Band			= NULL  
,@LabelSizeID	= NULL  
,@templateId	= NULL

GO 
CREATE PROCEDURE UpsertTemplateProperties
@templateId     INT
,@width         varchar(255)
,@height		varchar(255)
,@topProperty	varchar(255)
,@leftProperty	varchar(255)
,@fontSize		varchar(255)
,@color			varchar(255)
,@spacing		varchar(255)
,@fontWeight    varchar(255)
,@active	    varchar(255)
,@textProperty	varchar(255)
,@Fieldid        INT
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to insert and update EmailGroupUserMap
*/
BEGIN

  UPDATE dbo.TemplateProperties
  SET 
     width       	 = @width       
    ,height		     = @height		
    ,topProperty	 = @topProperty
    ,leftProperty	 = @leftProperty
    ,fontSize		 = @fontSize		
    ,color			 = @color			
    ,spacing		 = @spacing		
    ,fontWeight  	 = @fontWeight  
    ,active	  	     = @active	  
    ,textProperty	 = @textProperty    
  WHERE templateId = @templateId
  AND   Fieldid    = @Fieldid;

  IF @@ROWCOUNT=0
  BEGIN
   INSERT INTO dbo.TemplateProperties
   (width,height,topProperty,leftProperty,fontSize,color,fontWeight,active,textProperty,templateId,Fieldid)
   SELECT @width,@height,@topProperty,@leftProperty,@fontSize,@color,@fontWeight,@active,@textProperty,@templateId,@Fieldid
  END

  SELECT width,height,topProperty,leftProperty,fontSize,color,fontWeight,active,textProperty,templateId,Fieldid,id
  FROM dbo.TemplateProperties 
  WHERE templateId = @templateId
  AND   Fieldid    = @Fieldid;

END;

GO

EXEC UpsertTemplateProperties
 @width       	 ='22'
,@height		 ='2'
,@topProperty	 ='2'
,@leftProperty	 ='2'
,@fontSize		 ='2'
,@color			 ='2'
,@spacing		 ='2'
,@fontWeight  	 ='2'
,@active	  	 ='2'
,@textProperty	 ='2'
,@templateId	 =1
,@Fieldid		 =4

GO

ALTER PROCEDURE GETTemplateProperties
@templateId     INT
,@Fieldid       INT
,@id			INT
AS
/*
ModifiedBy  : Triven D
CreatedDate :2021/06/17 
Description : This code is used to Get TemplateProperties
*/
BEGIN
  SELECT width,height,topProperty,leftProperty,fontSize,color,fontWeight,active,textProperty,templateId,Fieldid,id
  FROM dbo.TemplateProperties 
  WHERE 
  (Fieldid = CASE WHEN @Fieldid IS NULL THEN Fieldid ELSE @Fieldid END) AND
  (templateId = CASE WHEN @templateId IS NULL THEN templateId ELSE @templateId END) AND
  (id = CASE WHEN @id IS NULL THEN id ELSE @id END)
END;
GO
EXEC GETTemplateProperties
@Fieldid		= NULL 
,@templateId	= NULL
, @id = null
GO

alter PROCEDURE GetTemplateInfo
 @StoreID		    INT=103
,@Branchid		    INT
,@Band			    INT
,@LabelSizeID	    INT =1
,@templateId		INT =1

AS 

BEGIN

BEGIN TRY

;WITH CTE AS(
SELECT FieldName,templateId,b.id as TemplatePropertiesID,width,height,topProperty,leftProperty,fontSize,color,spacing,fontWeight,active,textProperty
FROM TemplateProperties b 
INNER JOIN Field c ON b.Fieldid = c.id
WHERE b.templateId = @templateId
)
SELECT 
FieldName,templateId,TemplatePropertiesID,
CASE WHEN FieldName = 'PromRecordId'     THEN PromRecordId     
     WHEN FieldName = 'PromProdCode'	 THEN PromProdCode	   
     WHEN FieldName = 'PromIsland'		 THEN PromIsland	   
     WHEN FieldName = 'PrBand'			 THEN PrBand		   
     WHEN FieldName = 'PromCode'		 THEN PromCode		   
     WHEN FieldName = 'PromDescription'	 THEN PromDescription  
     WHEN FieldName = 'PromSupp'		 THEN PromSupp		   
     WHEN FieldName = 'PromStartRetDate' THEN PromStartRetDate 
     WHEN FieldName = 'PromEndRetDate'	 THEN PromEndRetDate   
     WHEN FieldName = 'PromStartCostDate'THEN PromStartCostDate
     WHEN FieldName = 'PromEndCostDate'	 THEN PromEndCostDate  
     WHEN FieldName = 'CurrentCost'		 THEN CurrentCost	   
     WHEN FieldName = 'PromCost'		 THEN PromCost		   
     WHEN FieldName = 'PromMessage'		 THEN PromMessage	   
     WHEN FieldName = 'CurrentRetail'	 THEN CurrentRetail	   
     WHEN FieldName = 'PromRetail'		 THEN PromRetail	   
     WHEN FieldName = 'SuppProdCode'	 THEN SuppProdCode	   
     WHEN FieldName = 'HostedInd'		 THEN HostedInd		   
     WHEN FieldName = 'PromDeptDesc'	 THEN PromDeptDesc	   
     WHEN FieldName = 'proddescrip'		 THEN proddescrip	    
	 ELSE NULL END AS FieldValue,
width,height,topProperty,leftProperty,fontSize,color,spacing,fontWeight,active,textProperty,BuyersOfferID
FROM CTE
CROSS APPLY 
   ( 
   SELECT  PromRecordId,PromProdCode,PromIsland,PrBand,PromCode,PromDescription,PromSupp,PromStartRetDate,PromEndRetDate,PromStartCostDate
,PromEndCostDate,CurrentCost,PromCost,PromMessage,CurrentRetail,PromRetail,SuppProdCode,HostedInd,PromDeptDesc,proddescrip ,a.BuyersOfferID
FROM BuyerOffer a
--JOIN StoreOffer b on a.BuyersOfferID=b.BuyersOfferID
--JOIN StoreOfferAcceptance c on c.StoreOffersID=b.StoreOffersID
--WHERE b.storeid   = @storeID 
--AND b.LabelSizeID = @LabelSizeID
   ) A 

END TRY  
BEGIN CATCH  
    SELECT   
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage;  
  
    IF @@TRANCOUNT > 0  
        ROLLBACK TRANSACTION;  
END CATCH;  
END
GO

EXEC GetTemplateInfo
 @StoreID		    =103
,@Branchid		    =NULL
,@Band			    =NULL
,@LabelSizeID	     =1
,@templateId		 =1
GO

select * from store