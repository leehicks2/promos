using System;

namespace promos.Controllers.Models
{
public class FileImport
{
    public int ID { get; set; }

    public string FileName { get; set; }

    public DateTime? ImportDate { get; set; }

    public string Filesize { get; set; }

    public int UserID { get; set; }

    public int? FileRecordCount { get; set; }

    public int? ValidRecordCount { get; set; }

    }   
}
