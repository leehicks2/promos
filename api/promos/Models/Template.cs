namespace promos.Controllers.Models{
    public class Template{
        public int id {get; set;}
        public string BackgroundImage {get; set;}
        public string TemplateName {get; set;}
        public int BandId {get; set;}
        public int templateType {get; set;}
        public string Width {get; set;}
        public string Height {get; set;}
        public string SizeDescription {get; set;}
        public string Size {get; set;}
        }
}