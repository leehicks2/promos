using System;

namespace promos.Controllers.Models
{
    public class Order{
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public int TemplateId { get; set; }
        public int BuyersOfferId { get; set; }
        public int Amount { get; set; }
        public string TemplateName {get; set;}
        public string Background {get; set;}
        public string StoreName {get;set;}
        public string PromProdCode {get; set;}
        public string SizeDescription {get; set;}
        public string Size {get; set;}
        public string Width {get; set;}
        public string Height {get; set;}
    }
}