namespace promos.Controllers.Models
{
    public class Field{
        public int id { get; set;}
        public string FieldName {get; set;}
        public string Description {get; set;}
    }
}