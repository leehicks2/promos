
namespace promos.Controllers.Models
{
    public class TemoPromoFile
    {
        public string PromRecordId { get; set; }

        public string PromProdCode { get; set; }

        public string PromIsland { get; set; }

        public string PrBand { get; set; }

        public string PromCode { get; set; }

        public string PromDescription { get; set; }

        public string PromSupp { get; set; }

        public string PromStartRetDate { get; set; }

        public string PromEndRetDate { get; set; }

        public string PromStartCostDate { get; set; }

        public string PromEndCostDate { get; set; }

        public string CurrentCost { get; set; }

        public string PromCost { get; set; }

        public string PromMessage { get; set; }

        public string CurrentRetail { get; set; }

        public string PromRetail { get; set; }

        public string SuppProdCode { get; set; }

        public string HostedInd { get; set; }

        public string PromDeptDesc { get; set; }

        public string proddescrip { get; set; }

        public string Size { get; set; }
        public string Slogan { get; set; }
        public string Savings { get; set; }
        public string DeptDesc { get; set; }
        public string MultibuyPrice { get; set; }
        public string MultibuyQty { get; set; }
        public string GenericDescription { get; set; }

    }
}