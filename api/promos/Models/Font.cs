using System;

namespace promos.Controllers.Models
{
    public class Fonts{
        public int FontID { get; set; }
        public int BandId { get; set; }
        public string Font { get; set; }
    }
}