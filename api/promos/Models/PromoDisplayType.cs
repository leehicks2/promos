using System;

namespace promos.Controllers.Models
{
    public class PromoDisplayType{
        public int Id { get; set; }
        public string Description { get; set; }
        public string Size {get; set;}
    }
}