namespace promos.Controllers.Models{
    public class StoreRequestTemplates{
         public int Id {get;set;}
        public int TemplateId {get;set;}
        public int Amount {get;set;}
        public int StoreRequestId {get;set;}
    }
}