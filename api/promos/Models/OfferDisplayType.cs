using System;

namespace promos.Controllers.Models
{
    public class OfferDisplayType{
        public int Id { get; set; }
        public int Amount { get; set; }
        public string Description {get; set;}
         public int TemplateId {get; set;}
    }
}