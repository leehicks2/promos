using System;

namespace promos.Controllers.Models
{
    public class StoreOffer{
        public int StoreOffersId { get; set; }
        public int StoreID { get; set; }
        public string StoreName {get; set;}
        public int BuyersOfferId {get; set;}
        public int TemplateId {get; set;}
        public int Amount {get; set;}
        public int StatusId {get; set;}
    }
}