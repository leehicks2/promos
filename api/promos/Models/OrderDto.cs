using System;

namespace promos.Controllers.Models
{
    public class OrderDto{
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public string OrderDate { get; set; }
         public int StoreId { get; set; }
         public string StoreName { get; set; }
         public string TemplateName { get; set; }
         public int Amount { get; set; }
         public string IslandName {get; set;}
         public string PromProdCode {get; set;}
         public string PromDescription {get; set;}
         public int TemplateId {get; set;}
    }
}