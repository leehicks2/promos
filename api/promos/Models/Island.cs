using System;

namespace promos.Controllers.Models
{
    public class Island{
        public int IslandId { get; set; }
        public string IslandName { get; set; }
    }
}