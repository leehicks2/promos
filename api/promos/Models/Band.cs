namespace promos.Controllers.Models
{
    public class Band
    {
        public int id {get; set;}
        public string name {get; set;}
        public string BandLetter {get; set;}
    }
}