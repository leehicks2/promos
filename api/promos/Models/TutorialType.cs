namespace promos.Controllers.Models{
    public class TutorialType{
        public int TutorialId {get; set;}
        public string TutorialName {get; set;}
        public string Folder {get; set;}
        public int ParentId {get; set;}
        }
    }