namespace promos.Controllers.Models{
    public class TemplateProperties{
        public int id {get;set;}
        public int templateId { get;set;}
        public string width { get;set;}
        public string height { get;set;}
        public string topProperty { get;set;}
        public string leftProperty { get;set;}
        public string fontSize { get;set;}
        public string color { get;set;}
        public string spacing { get;set;}
        public string fontWeight { get;set;}
        public bool active { get;set;}
        public string textProperty { get;set;}
        public int FieldId {get; set;}
        public string FieldName { get;set;}
        public string FieldValue { get;set;}
        public string fontstyle { get;set;}
        public string TemplateSizeDescription {get; set;}
        public string BackgroundImage {get; set;}
        public string Savings {get; set;}
        public int Strikethrough {get; set;}
        public string TextAlign {get; set;}
        public int DateFormat {get; set;}
        public int Rotate {get; set;}
        public string TextBefore {get; set;}
        public string TextAfter {get; set;}
        public string VerticalAlign {get; set;}
        public string Description {get;set;}
    }
}