using System;

namespace promos.Controllers.Models
{
    public class OfferType{
        public int Id { get; set; }
        public string Name { get; set; }
        public int BandId {get; set;}
    }
}