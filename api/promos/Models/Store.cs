using System;

namespace promos.Controllers.Models
{
    public class Store{
        public int StoreID { get; set; }

        public string StoreName { get; set; }

        public DateTime DateAdded { get; set; }
        public int BandId {get; set;}
    }
}