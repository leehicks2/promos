namespace promos.Controllers.Models
{
    public class AuthResponseModel{
        public bool Authed  {get; set;}
        public User User {get; set;}
    }
}