using System;

namespace promos.Controllers.Models
{
    public class OfferDisplayTypeDb{
        public int Id { get; set; }
        public int DisplayTypeId { get; set; }
        public int OfferTypeId {get; set;}
        public int Amount {get; set;}
    }
}