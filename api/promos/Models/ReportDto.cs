namespace promos.Controllers.Models
{
    public class ReportDto
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}