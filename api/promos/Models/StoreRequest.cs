using System.Collections.Generic;

namespace promos.Controllers.Models
{
    public class StoreRequests
    {
        public int StoreRequestsId {get; set;}
       public int BuyersOfferId {get; set;}
       public int UserId {get; set;}
       public int SizeId {get; set;}
       public int Quantity {get; set;}
        public int TemplateId {get; set;}
        public int StatusId {get; set;}
        public string Orientation {get; set;}
       public List<StoreRequestTemplates> storeRequestTemplates {get; set;}
       public int StoreId {get; set;}
    }
}