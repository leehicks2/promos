using System;

namespace promos.Controllers.Models
{
    public class AccessLevel
    {
        public int AccessLevelId {get; set;}
        public int AccessLevelName {get; set;}
        public DateTime? DateAdded { get; set; }
    }
}