namespace promos.Controllers.Models
{
    public class StoreRequestAction
    {
        public int StoreRequestsId {get; set;}
         public int BuyersOfferId {get; set;}
         public int StatusId {get; set;}
        public string PromProdCode {get; set;}
        public string PromDescription {get; set;}
        public string TemplateName {get; set;}
        public int TemplateId {get; set;}
        public int StoreID {get; set;}
        public int Amount {get; set;}
        public string UserName {get; set;}
        public string StoreName {get; set;}
        public string SizeDescription {get; set;}
        public string BandLetter {get; set;}
        public int BandId {get; set;}
        public int TemplateMapId {get;set;}
    }
}