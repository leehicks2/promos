
namespace promos.Controllers.Models
{
    public class StoreOfferTypeDisplayTypesDb{
        public int id { get; set; }
        public int StoreID { get; set; }
        public string StoreName {get; set;}
        public int OfferTypeDisplayTypeId {get; set;}
        public int UserId {get; set;}
        public int Amount {get; set;}
        public string TemplateName {get; set;}
    }
}