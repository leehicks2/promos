using Microsoft.Extensions.Configuration;

namespace promos.Controllers.Models {
    public class DataSourceConnection{
        public string DataSource { get; set;}
        public string UserId { get; set; }
        public string Password { get; set; }
        public string InitialCatalog { get; set; }


        public DataSourceConnection(IConfiguration config){
            DataSource = config.GetValue<string>("SqlDataSource");
            UserId = config.GetValue<string>("SqlUserId");
            Password = config.GetValue<string>("SqlPassword");
            InitialCatalog = config.GetValue<string>("SqlInitialCatalog");
        }
    }
}
