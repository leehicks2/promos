using System;
using System.Collections.Generic;

namespace promos.Controllers.Models
{
    public class User
{
    public int Userid { get; set; }

    public string UserName { get; set; }

    public DateTime DateAdded { get; set; }

    public string UserPassword { get; set; }

    public int AccessLevelID { get; set; }

    public int? StoreId { get; set; }

    public string UserEmail { get; set; }

}
}