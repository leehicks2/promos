namespace promos.Controllers.Models
{
    public class StoreOfferDisplayOverride{
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int FinalAmount {get; set; }
    }
}