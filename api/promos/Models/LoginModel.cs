namespace promos.Controllers.Models
{
    public class LoginModel{
        public string Username {get; set;}
        public string Password  {get; set;}
        public string Authed  {get; set;}
    }
}