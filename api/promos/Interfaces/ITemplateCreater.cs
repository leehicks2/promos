using System.Collections.Generic;
using iText.Kernel.Pdf;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface ITemplateCreater{
        List<Order> GetOrder(int id);
        CreatedModel ProcessOrder(int id, int printerId);
        void CreatePreview(int buyerOffer, int template);
    }
}