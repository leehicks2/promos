using System.Collections.Generic;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface IStoreOffer{
     int Insert(int bandId, int userId);
     StoreOffer GetById(int id);
     List<StoreOffer> GetAll();
     List<StoreOffer> UpsertMultiple(List<StoreOffer> storeOffers);
     List<StoreOfferStatus> GetStoreOfferStatusAll();
     List<PromoDisplayType> GetPromoDisplayType();
     List<OfferType> GetOfferType();
     OfferType InsertOfferType(OfferType storeOffer);
     List<OfferDisplayType> GetOfferDisplayTypesByOfferId(int id);
     OfferDisplayTypeDb InsertOfferDisplayType(OfferDisplayTypeDb storeOffer);
     OfferDisplayType UpdateOfferDisplayType(OfferDisplayType storeOffer);
     OfferDisplayType DeleteOfferDisplayType(OfferDisplayType storeOffer);
     List<OfferType> GetOfferTypeByBandId(int id);
     List<StoreOfferTypeDisplayTypesDb> GetOfferTypeDisplayTypesDb(int offerId, int storeId);
     List<StoreOfferTypeDisplayTypesDb> GetOfferTypeDisplayTypeStoreNameByOfferId(int offerId);
    List<StoreOfferTypeDisplayTypesDb> InsertOfferTypeDisplayType(List<StoreOfferTypeDisplayTypesDb> storeOffers);
    StoreOfferTypeDisplayTypesDb UpdateOfferTypeDisplayType(StoreOfferTypeDisplayTypesDb storeOffers);
    List<StoreOffer> GetStoreOffersByBandId(int bandId);
    int DeleteStoreOffer(int buyerOfferId);
    int DeleteStoreOfferById(int id);
    List<StoreOfferDisplayOverride> GetOfferDisplayTypesByOfferIdForStore(int id);
    int InsertOrder(int bandId, int userId, int offerTypeId);
    List<Order> GetOrders();
    List<OrderDto> GetOrdersById(int id);
    List<Order> GetOrdersComplated();
    List<PrinterDto> GetPrinters();
    List<ReportDto> GetReports();
    };
}  