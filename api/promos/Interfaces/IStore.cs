using System.Collections.Generic;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface IStore{
    List<Store> GetAllStores();
     Store GetStoreById(int id);
     List<Island> GetAllIslands();
     List<Store> GetStoreBandIdId(int id);
     List<Island> GetIslandsByBandId(int id);
     List<Store> GetStoreBandAndIsland(int id, int islandId);
    };
}  