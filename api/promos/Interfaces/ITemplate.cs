using System.Collections.Generic;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface ITemplate{
     Template GetTemplateById(int id);
     List<Template> GetAllTemplates();
     Template Insert(Template template);
     Template Update(Template template);
     List<Template> GetTemplatesByBand(int id);
     List<TemplatePack> GetTemplatePacks();
     TemplatePack InsertTemplatePack(TemplatePack template);
     List<TemplateDisplay> GettemplateDisplay(int storeId, int bandId, int templateId);
     List<TemplateDisplay> GettemplateDisplayByBuyerAndTemplate(int buyerOfferId, int templateId);
     List<Template> GetTemplateByBandAndSize(string bandLetter, int sizeId);
    };
}   