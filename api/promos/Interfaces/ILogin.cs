using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface ILogin{
     AuthResponseModel login(LoginModel loginModel);
    };
}   