using System.Collections.Generic;
using System.Data;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface IBuyerOffers{
    //  bool Import(DataTable temoPromo);
    //  bool MapWorkSheetToTemoPromo(string filePath);
    //  bool CallProc(string filename, string fileSize, int userId);
    List<BuyerOffer> GetBuyerOffers();
    List<BuyerOffer> GetBuyerOffersByBand(string BandId, int userId);
    List<BuyerOffer> GetBuyerOffersByBandWithOrder(string BandId, int userId, string orderBy);
    List<BuyerOffer> GetBuyerOffersByBandAndStore(string BandId, string StoreId);
    List<BuyerOffer> GetBuyerOffersByUser(int userId);
    List<BuyerOffer> GetBuyerOffersByBandAndIsland(string BandId, string islandName);
    List<BuyerOffer> GetBuyerOffersByBandAndIslandAndOrder(string BandId, string islandName, string orderby);
    List<BuyerOffer> GetBuyerOffersByBandAndIslandAndOrderFilter(string BandId, string islandName, string orderby, string filter);
    List<BuyerOffer> GetBuyerOffersByBandWithOrderFilter(string BandId, int userId, string orderBy, string filterBy);
    List<BuyerOffer> InsertMultipeSoreOrders(List<int> buyerOffers);
    List<BuyerOffer> DeleteMultipeSoreOrders(List<int> buyerOffers);
   };
} 