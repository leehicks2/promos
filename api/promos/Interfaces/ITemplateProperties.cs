using System.Collections.Generic;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface ITemplateProperties{
     List<TemplateProperties> GetTemplatePropertyByTemplateId(int id);
    TemplateProperties GetById(int id);
     TemplateProperties Insert(TemplateProperties templateProperties);
     TemplateProperties Update(TemplateProperties templateProperties);
    List<TemplateProperties> GetByTemplateId(int id);
    List<Field> GetFields();
    bool DeleteTemplateProperties(int id);
    List<Fonts> GetFontsByBand(int id);
    };
}   