using System.Collections.Generic;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface IBand{
     Band GetBandById(int id);
     List<Band> GetAllBands();
     Band Insert(Band obj);
     Band Update(Band obj);
    };
}  