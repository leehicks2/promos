using System.Collections.Generic;
using System.Data;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface IImportPromo{
     bool Import(DataTable temoPromo, string fileName);
     bool MapWorkSheetToTemoPromo(string filePath, string fileName);
     bool CallProc(string filename, string fileSize, int userId);
     bool UpdatePromos(List<BuyerOffer> promos);
    };
}   