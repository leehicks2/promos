using System.Collections.Generic;
using promos.Controllers.Models;

namespace promos.Controllers.Interfaces{
    public interface IStoreRequests{

        List<StoreRequests> GetAllStoreRequests();
        StoreRequests InsertStoreRequest(StoreRequests request);
        List<StoreRequestAction> GetAllStoreRequestsByBand(string bandLetter);
        StoreRequestAction ConfirmStoreRequest(StoreRequestAction request);
        bool UpdateTemplateMap(int id, int amount);
        List<StoreRequestAction> GetAllActiveRequestsByStore(int storeId);
        bool ConfirmStoreRequestByUser(int storeId);
        List<StoreRequestOrder> GetActiveStoreRequestOrdersByBand(string bandLetter);

        List<StoreRequestAction> GetAllStoreRequestsByOrderId(int orderId);
        StoreRequestAction ConfirmStoreRequest(int orderIdReq);
        List<Tutorial> GetTutorials();
        List<TutorialType> GetTutorialTypes(int parentId);
    }
}
