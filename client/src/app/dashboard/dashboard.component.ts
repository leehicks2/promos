import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccessService } from '../service/access.service';
import { StoreOfferService } from '../service/store-offer.service';
import { ReportDto } from '../shared/models/ReportDto';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public reportList: ReportDto[] = [];
  constructor(private storeOfferService: StoreOfferService, private router:Router,  private accessService: AccessService) {
    this.gotoHome();
   }

   gotoHome(){
    if(!this.accessService.isAdmin()){
      this.router.navigate(['/home']);  // define your component where you want to go
     }
    }

  ngOnInit() {
    this.getPrinters();
  }


  getPrinters(){
    this.storeOfferService.Getreports().then((data: ReportDto[])=>{
      if(data){
        this.reportList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   goToLink(link){
    window.open(link, "_blank");
   }


}
