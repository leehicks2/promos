import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccessService } from '../service/access.service';
import { BandService } from '../service/band.service';
import { IslandService } from '../service/island.service';
import { StoreOfferService } from '../service/store-offer.service';
import { StoreService } from '../service/store.service';
import { TemplateService } from '../service/template.service';
import { Band } from '../shared/models/BandModel';
import { Island } from '../shared/models/Island';
import { OfferDisplayType } from '../shared/models/OfferDisplayType';
import { OfferDisplayTypeDB } from '../shared/models/OfferDisplayTypeDb';
import { OfferType } from '../shared/models/OfferType';
import { PosTemplate } from '../shared/models/PosTemplate';
import { PromoDisplaytype } from '../shared/models/PromoDisplayType';
import { Store } from '../shared/models/StoreModel';
import { StoreOfferTypeDisplayTypes } from '../shared/models/StoreOfferTypeDisplayTypes';
import { TemplatePack } from '../shared/models/TemnplatePack';

@Component({
  selector: 'app-templatepack',
  templateUrl: './templatepack.component.html',
  styleUrls: ['./templatepack.component.css']
})
export class TemplatepackComponent implements OnInit {

  public templatePackList: TemplatePack[] = []
  public bandList : Band [] = [];
  public islandList: Island [] = [];
  public storeList: Store[] = [];
  public promoDisplayTypeList: PromoDisplaytype[] = [];
  public offerTypeList: OfferType[] = [];
  public currentOfferType: OfferType = new OfferType();
  public offerDisplayTypes: OfferDisplayType[] = [];
  public offerDisplayTypeDb: OfferDisplayTypeDB;
  public templateList: PosTemplate[] = [];
  public storeOfferTypeDisplayTypes: StoreOfferTypeDisplayTypes[];
  public storeOfferTypeDisplayTypesStores: StoreOfferTypeDisplayTypes[];

  public newStoreOfferTypeDisplayTypesStores: StoreOfferTypeDisplayTypes[];

  public displayAddType: boolean = false;
  public displayAddStoreOfferType: boolean = false;
  public defaultOffersView: boolean = true;
  public headerdeafualtOffersView = true;

  public band: Band = new Band();


  public displayTemplatePackBox: boolean = false;

  constructor(private templateService: TemplateService, private bandService: BandService,
    private storeOfferService: StoreOfferService, private islandService: IslandService, private storeService: StoreService,
    private router: Router,  private accessService: AccessService) {
      this.gotoHome();
      this.setNewOfferDisplayType();
     }


     gotoHome(){
      if(!this.accessService.isAdmin()){
       this.router.navigate(['/home']);  // define your component where you want to go
      }
     }

  gettemplatePacks(){
    this.templateService.getByTemplatePacks().then((data: TemplatePack[])=>{
      if(data){
        this.templatePackList = data;
        console.log(JSON.stringify(this.templatePackList));
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   openCreateTemplatePackBox(){
    if(this.displayTemplatePackBox){
      this.displayTemplatePackBox = false;
    }else{
      this.displayTemplatePackBox = true;
    }
 }

 getTemplatesByBand(){
  this.templateService.getByBandId(this.band.id).then((data: PosTemplate[])=>{
    if(data){
      this.templateList = data;
    }
    }).catch((error: any[])=>{
      console.log(error);
    });
 }

   getBands(){
    this.bandService.GetBands().then((data: Band[])=>{
      if(data){
        this.bandList = data;
        this.band = this.bandList[0];
        this.getOfferTypeByband(this.band.id);
        this.getTemplatesByBand();
        this.getIslands();
        this.GetStores();
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getIslands(){
    this.islandService.GetIslandsByBand(this.band.id).then((data: Island[])=>{
      if(data){
        this.islandList = data;
        // this.band = this.bandList[0];
        // this.getOfferTypeByband(this.band.id);
        // this.getTemplatesByBand();
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   GetStores(){
    this.storeService.GetStoresByBand(this.band.id).then((data: Store[])=>{
      if(data){
        this.storeList = data;
        //alert(JSON.stringify(this.storeList))
        // this.band = this.bandList[0];
        // this.getOfferTypeByband(this.band.id);
        // this.getTemplatesByBand();
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   GetStoresByBandAndIsland(id){
    this.storeService.GetStoresByBandAndIsland(this.band.id,id).then((data: Store[])=>{
      if(data){
        this.storeList = data;

        //alert(JSON.stringify(this.storeList))
        // this.band = this.bandList[0];
        // this.getOfferTypeByband(this.band.id);
        // this.getTemplatesByBand();
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getPromoDisplayType(){
    this.storeOfferService.GetPromoDisplayType().then((data: PromoDisplaytype[])=>{
      if(data){
        this.promoDisplayTypeList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getOfferType(){
    this.storeOfferService.GetOfferTypes().then((data: OfferType[])=>{
      if(data){
        this.offerTypeList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getOfferTypeByband(id: number){
    this.storeOfferService.GetOfferTypesByBand(id).then((data: OfferType[])=>{
      if(data){
        this.offerTypeList = data;
        this.displayTemplatePackBox = false;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getOfferDisplayTypes(id){
    this.storeOfferService.GetOfferDisplayTypes(id).then((data: OfferDisplayType[])=>{
      if(data){
        this.offerDisplayTypes = data;
        this.offerDisplayTypeDb.offerTypeId = id;
        this.GetOfferTypeDisplayTypesStores(id);
        if(id == 0){
          this.displayAddType = false;
         }else{
          this.displayAddType = true;
         }
         this.defaultOffersView = true;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   insertOfferType(){
    this.currentOfferType.bandId = this.band.id;
    this.storeOfferService.InsertOfferType(this.currentOfferType).then((data: OfferType)=>{
      if(data){
        this.currentOfferType = data;
        this.getOfferTypeByband(this.band.id);
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   insertOfferDisplayType(){
     if(this.offerDisplayTypeDb.displayTypeId == 0){
      alert("Please check that you have selected a display type");
      return;
     }

    this.storeOfferService.InsertOfferDisplayType(this.offerDisplayTypeDb).then((data: OfferDisplayTypeDB)=>{
      if(data){
        this.addNewOfferDisplaytypeToList(data);
        //this.setNewOfferDisplayType()
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   updateOfferDisplayType(id){
    if(this.offerDisplayTypeDb.amount <= 0){
     alert("Amount cant be less than zero");
     return;
    }
    let saveObj: OfferDisplayType;
    for(let x = 0; x < this.offerDisplayTypes.length; x++){
      if(this.offerDisplayTypes[x].id == id){
        saveObj = this.offerDisplayTypes[x];
      }
    }

   this.storeOfferService.UpdateOfferDisplayType(saveObj).then((data: OfferDisplayType)=>{
     if(data){

     }
     }).catch((error: any[])=>{
       console.log(error);
       alert("Error Updating")
     });;
  }

  deleteOfferDisplayType(id){
    let saveObj: OfferDisplayType;
    let theId = 0;
    for(let x = 0; x < this.offerDisplayTypes.length; x++){
      if(this.offerDisplayTypes[x].id == id){
        saveObj = this.offerDisplayTypes[x];
        theId = saveObj.id;
        this.storeOfferService.deleteOfferDisplayType(saveObj).then((data: OfferDisplayType)=>{
          if(data){
           this.offerDisplayTypes.splice(x, 1);
          }
          }).catch((error: any[])=>{
            console.log(error);
            alert("Error Updating")
          });;
      }
    }

    //alert(this.storeOfferTypeDisplayTypes.length);

    for(let y = 0; y < this.storeOfferTypeDisplayTypes.length; y++){
      // alert(this.storeOfferTypeDisplayTypes[y].offerTypeDisplayTypeId);
      // alert(theId)
      if(this.storeOfferTypeDisplayTypes[y].offerTypeDisplayTypeId == theId){
        this.storeOfferTypeDisplayTypes.splice(y, 1);
      }
    }
  }

   addNewOfferDisplaytypeToList(offerdb: OfferDisplayTypeDB){
    let offer = new OfferDisplayType();
    offer.id = offerdb.id;
    offer.amount = offerdb.amount;
    offer.description = this.getdisplayTypeDescription(offerdb.displayTypeId);
    this.offerDisplayTypes.push(offer);
   }

   getdisplayTypeDescription(id){
     let description = "";
     this.templateList.forEach(element => {
      if(element.id == id){
        description = element.templateName;
      }
    });
    return description;
   }

   setNewOfferDisplayType(){
    this.offerDisplayTypeDb = new OfferDisplayTypeDB();
    this.offerDisplayTypeDb.amount = 1;
    this.offerDisplayTypeDb.displayTypeId = 0;
    this.offerDisplayTypeDb.offerTypeId = 0;
   }

   //UI Events
   setCurrentBand(id){
    this.defaultOffersView = true;
    this.bandList.forEach(element => {
      if(element.id == id){
        this.band = element;
        this.getOfferTypeByband(this.band.id);
        this.getTemplatesByBand();
        this.offerDisplayTypes = [];
        this.displayAddType = false;

        this.getIslands();
        this.GetStores();
      }
    });
   }

   setDisplayType(id){
    this.offerDisplayTypeDb.displayTypeId = id;
   }

   checkAmount(){
     if(this.offerDisplayTypeDb.amount <= 0){
      this.offerDisplayTypeDb.amount = 1;
     }
   }

   checkEditedAmount(i){
    for(let x = 0; x < this.offerDisplayTypes.length; x++){
      if(this.offerDisplayTypes[x].amount <= 0){
        this.offerDisplayTypes[x].amount = 1;
      }
    }
   }

   GetOfferTypeDisplayTypes(storeId){
    this.displayAddStoreOfferType = false;
    this.storeOfferService.GetOfferTypeDisplayTypes(this.offerDisplayTypeDb.offerTypeId, storeId).then((data: StoreOfferTypeDisplayTypes[])=>{
      if(data){
        this.storeOfferTypeDisplayTypes = data;
        for(let x = 0; x < this.storeOfferTypeDisplayTypesStores.length; x++){
          this.storeOfferTypeDisplayTypesStores[x].Active = false;
          if(this.storeOfferTypeDisplayTypesStores[x].storeID == storeId){
            this.storeOfferTypeDisplayTypesStores[x].Active = true;
          }
        }
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   GetOfferTypeDisplayTypesStores(offerId){
     this.storeOfferTypeDisplayTypes = [];
    this.storeOfferService.GetOfferTypeDisplayTypesStores(offerId).then((data: StoreOfferTypeDisplayTypes[])=>{
      if(data){
        this.storeOfferTypeDisplayTypesStores = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   addNewOfferTypeDisplayTypes(){
     this.storeOfferTypeDisplayTypes = [];
     this.displayAddStoreOfferType = true;
     for(let x = 0; x < this.offerDisplayTypes.length; x++){
      let newOne: StoreOfferTypeDisplayTypes = new StoreOfferTypeDisplayTypes();
      newOne.offerTypeDisplayTypeId = this.offerDisplayTypes[x].id;
      newOne.amount = this.offerDisplayTypes[x].amount;
      newOne.templateName = this.offerDisplayTypes[x].description;
      this.storeOfferTypeDisplayTypes.push(newOne);
     }
   }

   SetStoresOnStoreDisplayOfferType(id){

    for(let x = 0; x < this.storeOfferTypeDisplayTypesStores.length; x++){
      if(this.storeOfferTypeDisplayTypesStores[x].storeID == id){
        alert("That Store already exists");
        return;
      }
    }

    for(let x = 0; x < this.storeOfferTypeDisplayTypes.length; x++){
      this.storeOfferTypeDisplayTypes[x].storeID = id;
    }

   }

   insertStoreDisplayOfferType(){
    if(!this.storeOfferTypeDisplayTypes[0].storeID){
      alert("Please select a Store");
      return;
    }

    // alert(JSON.stringify(this.storeOfferTypeDisplayTypes))
    // return;
      this.storeOfferService.InsertStoreOfferTypeDisplayTypes(this.storeOfferTypeDisplayTypes).then((data: StoreOfferTypeDisplayTypes[])=>{
      if(data){
        this.storeOfferTypeDisplayTypes = data;
        this.GetOfferTypeDisplayTypesStores(this.offerDisplayTypeDb.offerTypeId);
        this.displayAddStoreOfferType = false;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   updateStoreDisplayOfferType(id){

      this.storeOfferService.UpdateStoreOfferTypeDisplayTypes(this.storeOfferTypeDisplayTypes[id]).then((data: StoreOfferTypeDisplayTypes)=>{
      if(data){
       alert("Updated");
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   setCustomStoreTypeView(){
     this.defaultOffersView = false;
   }

   setDefaultStoreTypeView(){
    this.defaultOffersView = true;
    this.storeOfferTypeDisplayTypes = [];
  }

  ngOnInit() {
    this.getBands();
    this.getPromoDisplayType();
  }

}
