import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { ResizeEvent } from 'angular-resizable-element';
import { TemplateProperties } from '../shared/models/ElementProperties';
import { PosTemplate } from '../shared/models/PosTemplate';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { TemplateService } from '../service/template.service';
import { TemplatepropertiesService } from '../service/templateproperties.service';
import { PositionAudit } from '../shared/models/PositionAudit';
import { Field } from '../shared/models/Field';
import { BandService } from '../service/band.service';
import { Band } from '../shared/models/BandModel';
import { StoreService } from '../service/store.service';
import { Store } from '../shared/models/StoreModel';
import { TemplatePack } from '../shared/models/TemnplatePack';
import { StoreOfferService } from '../service/store-offer.service';
import { PromoDisplaytype } from '../shared/models/PromoDisplayType';
import { TemplateDisplay } from '../shared/models/TemplateDisplay';
import { TempDisplayText } from '../shared/models/TempDisplayText';
import { PromosService } from '../service/promos.service';
import { Promo } from '../shared/models/PromoModel';
import { Fonts } from '../shared/models/Fonts';
import { DOCUMENT, getLocaleFirstDayOfWeek } from '@angular/common'
import { Router } from '@angular/router';
import { AccessService } from '../service/access.service';
import { GridDisplay } from '../shared/models/GridDisplay';


@Component({
  selector: 'app-templating',
  templateUrl: './templating.component.html',
  styleUrls: ['./templating.component.css']
})
export class TemplatingComponent implements OnInit {

  public templateList: PosTemplate[] = [];
  public currentTemplate: PosTemplate = new PosTemplate();
  public positionAuditList: PositionAudit[] = []

  public newTemplate: PosTemplate = new PosTemplate()

  public currentTemplateHeight: string = "1122px";
  public currentTemplateWidth: string = "794px";
  public currentTemplateLeft: string = "";
  public currentTemplateRight: string = "";

  public currentTemplateDefaultHeight: number = 0;
  public currentTemplateDefaultWidth: number = 0;


  public inputOptionsRightPosition: string;
  public inputOptionsLeftPosition: string;
  public inputOptionsTopPosition: string = "0px";

  public promotions: Promo[] = [];

  // List of fields to link to promo objects
  public FieldList : Field [] = [];
  public FontList : Fonts [] = [];
  // List of all bands to select from
  public bandList : Band [] = [];
  public currentBandLtter: string = "";
  //List of Stores
  public storeList : Store [] = [];
  //
  public templatePackList: TemplatePack[] = [];

  public pageHeaderHeight: number = 100;
  public zoomCount: number = 0;

  public fontSizeList: string[] = [];
  public textSpacingSizeList: string[] = [];

  public promoDisplayTypeList: PromoDisplaytype[] = [];

  public tempDisplayText: TempDisplayText[] = [];
  public displayGrid: boolean = false;
  public displayBorder: boolean = false;

  public progress: number;
  public message: string;

  //display variable
  public displayTemplateBox: boolean = false;
  public displayTemplatePackBox: boolean = false;
  public textView: boolean = false;

  public currentPromoId: number;

  public spaceDivWidth = "";
  public spaceDivHeight = "";
  public spaceDivLeft = "";
  public spaceDivTop = "";


  constructor(private httpClient: HttpClient, private templateService: TemplateService,
    private templatePropertiesService: TemplatepropertiesService, private bandService: BandService,
    private storeService: StoreService, private storeOfferService: StoreOfferService, private promosService: PromosService,
    private _elementRef : ElementRef, @Inject(DOCUMENT) private _document: HTMLDocument,private router: Router,
    private accessService: AccessService) {
    this.gotoHome();
    this.currentTemplate.templateProperties = [];
    this.getTemplates();
    this.getFields();
    this.getBands();
    this.getPromoDisplayType();
    this.fontSizeList = this.generatePxDropdownList(100, 2);
    this.textSpacingSizeList = this.generatePxDropdownList(50, 1);

    this.templateBandSelect(1);
    this.gettemplatePacks();

   }

   gotoHome(){
     if(!this.accessService.isAdmin()){
      this.router.navigate(['/home']);  // define your component where you want to go
     }
    }

   ngAfterViewInit() {
    let x = this._document.querySelectorAll('.display-text');
    console.log(x);
}



   @ViewChild('myIdentifier', {static: false})elementView: ElementRef;

   generatePxDropdownList(numberOfItems, increment){
     let response:string[] = [];
    for(let x = 0; x <= numberOfItems; x++){
      let value = (x + increment) + "px";
      response.push(value);
    }
    return response;
   }

   //Creating Templates
   createTemplate(){
        this.templateService.insertTemplate(this.newTemplate).then((data: PosTemplate)=>{
        if(data){
          // this.currentTemplate = data;
          // this.currentTemplate.templateProperties = [];
          // this.displayTemplateBox = false;
          // this.newTemplate = new PosTemplate();
          // this.templateList.push(this.currentTemplate);
          this.templateSelectOnChange(data.id);
          this.displayTemplateBox = false;
        }
      }).catch((error: any[])=>{
        console.log(error);
      });
   }

   openCreateTemplateBox(){
    this.displayTemplatePackBox = false;
      if(this.displayTemplateBox){
        this.displayTemplateBox = false;
      }else{
        this.displayTemplateBox = true;
      }
   }

   openCreateTemplatePackBox(){
    this.displayTemplateBox = false;
    if(this.displayTemplatePackBox){
      this.displayTemplatePackBox = false;
    }else{
      this.displayTemplatePackBox = true;
    }
 }


   templateSelectOnChange(id){

    this.templateService.getById(id).then((data: PosTemplate)=>{
      this.verticalDivs = [];
      this.horizontalDivs = [];
      if(data){
        this.currentTemplate = data;
        this.currentTemplateDefaultWidth =  parseInt(this.currentTemplate.width);
        this.currentTemplateDefaultHeight = parseInt(this.currentTemplate.height);
        this.currentTemplate.backgroundImage = 'url("https://localhost:5001/StaticFiles/Resources/Images/' + this.currentTemplate.backgroundImage + '")';
        this.getContainerDimensionsFromSize(this.currentTemplate);
        this.calculateContainerPosition();
        this.getTemplateProperties();
        this.positionAuditList = [];
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }



   templateBandSelect(id){
    this.templateService.getByBandId(id).then((data: PosTemplate[])=>{
      if(data){
        this.templateList = data;
        this.templateSelectOnChange(this.templateList[0].id);
        this.getFontsByBand(id);
        this.bandList.forEach(element => {
          if(element.id == id){
           // alert(JSON.stringify(element))
            this.currentBandLtter = element.bandLetter
          }
        });
        this.getPromotions(this.currentBandLtter);
      }
      }).catch((error: any[])=>{
        console.log(error);
      });
   }

   zoom(){
    this.zoomCount += 0.1;
    let actualWidth = parseInt(this.currentTemplateWidth);
    let actualHeight = parseInt(this.currentTemplateHeight);

    let newWidth = actualWidth + (actualWidth * (this.zoomCount));
    let newHeight = actualHeight + (actualHeight * (this.zoomCount));

    let newLeftPos = (this.convertIntFromPixels(this.currentTemplateRight) - (this.convertIntFromPixels(this.currentTemplateWidth) / 2)) - (newWidth / 2);

    this.currentTemplateLeft = this.convertIntToPixels(newLeftPos);
    this.currentTemplateRight = this.convertIntToPixels((this.convertIntFromPixels(this.currentTemplateRight) - (this.convertIntFromPixels(this.currentTemplateWidth) / 2)) + (newWidth / 2));
    this.currentTemplateHeight = newHeight + "px";
    this.currentTemplateWidth = newWidth + "px";
   }

   setViewSize(width){
    let actualWidth = parseInt(this.currentTemplate.width);
    let actualHeight = parseInt(this.currentTemplate.height);

    let newWidth = actualWidth * 5;
    let newHeight = actualHeight * 5;

    let newLeftPos = (this.convertIntFromPixels(this.currentTemplateRight) - (this.convertIntFromPixels(this.currentTemplateWidth) / 2)) - (newWidth / 2);
    this.currentTemplateLeft = this.convertIntToPixels(newLeftPos);
    this.currentTemplateHeight = newHeight + "px";
    this.currentTemplateWidth = newWidth + "px";
   }


   changeTemplateSize(value){


    this.currentTemplate.height = value;
    this.setViewSize(800);

    // this.templateService.getById(id).then((data: PosTemplate)=>{
    //   if(data){
    //     this.currentTemplate = data;
    //     this.currentTemplate.backgroundImage = 'url("https://localhost:5001/StaticFiles/Resources/Images/' + this.currentTemplate.backgroundImage + '")';
    //     this.currentTemplate.templateProperties = [];
    //   }
    //   }).catch((error: any[])=>{
    //     console.log(error);
    //   });;
   }

   isOverFlow(textHeight, parentHieght){
    //  console.log(clientHeight);
    //  console.log(scrollHeight);
    if(textHeight == 0){
     return true;
    }
    return textHeight > parentHieght;
  }

  calculateFontSize(){
    let x = this._document.querySelectorAll('.display-text');
      const isOverflown = ({ clientHeight, scrollHeight }) => scrollHeight > clientHeight;

      const resizeText = ({ element, minSize = 1, maxSize = 500, step = 0.8, unit = 'px' }) => {
       // element.style.fontSize = "5px";
       // alert(element.clientWidth);
       const parent = element.parentNode;
          let i = minSize
          element.style.fontSize = i + "px";
          let overflow = false

          let textHeight = element.getBoundingClientRect().height;
          let parentHeight = parent.getBoundingClientRect().height;

          // if(textHeight > parentHeight){
          //   let diff = textHeight / parentHeight;

          //   alert(diff)
          // }

            //   alert(parent.style.width)
            // alert(parent.style.height)

          while (!overflow && i < maxSize && element.clientWidth < parent.clientWidth) {
            element.style.fontSize = `${i}${unit}`;
            let textHeight = element.clientHeight;
            // let parentHeight = parent.getBoundingClientRect().height;
            console.log(textHeight)
            console.log(parentHeight)
            overflow = this.isOverFlow(textHeight, parentHeight);
            console.log(overflow)
            if (!overflow) i += step;
          }

         // element.style.fontSize = `${i - step}${unit}`;
          // revert to last state where no overflow happened
         // element.style.fontSize = `${i - step}${unit}`
      }

      x.forEach(element => {
        resizeText(
          {
            element: element,
            step: 1
          }
        );
      });

  }

  calculateFontSizeMinus(){
    let x = this._document.querySelectorAll('.display-text');
      const isOverflown = ({ clientHeight, scrollHeight }) => scrollHeight > clientHeight;

      const resizeText = ({ element, minSize = 10, maxSize = 200, step = 1, unit = 'px' }) => {
       // element.style.fontSize = "5px";

          let i = element.clientWidth;
          const parent = element.parentNode;

            //  alert(parent.style.width)
            // alert(parent.style.height)

          while (element.clientWidth > parent.clientWidth) {
            element.style.fontSize = `${i}${unit}`;
            i = i - 1;
          }

          alert(parent.clientWidth);

          // revert to last state where no overflow happened
         //element.style.fontSize = `${i - step}${unit}`
      }

      resizeText(
        {
          element: x[0],
          step: 1
        }
      );
  }

  setDisplayText(){

    let finalDisplay = "";
    for(let x = 0; x <  this.currentTemplate.templateProperties.length; x++){
      if(this.currentTemplate.templateProperties[x].textProperty != "Placeholder"){
        finalDisplay = this.currentTemplate.templateProperties[x].textProperty;
      }else{

        if(this.currentTemplate.templateProperties[x].textBefore != "Placeholder"){
          finalDisplay = this.currentTemplate.templateProperties[x].textBefore;
        }
         finalDisplay = finalDisplay + this.currentTemplate.templateProperties[x].fieldName;
      }

      this.currentTemplate.templateProperties[x].displayText = finalDisplay;

    }
  }

   getTemplateProperties(){
    this.templatePropertiesService.getByTemplateId(this.currentTemplate.id).then((data: TemplateProperties[])=>{
      if(data){

        this.currentTemplate.templateProperties = data;
        //alert(JSON.stringify(this.currentTemplate.templateProperties))
      for(let x = 0; x <  this.currentTemplate.templateProperties.length; x++){
        // if(this.currentTemplate.templateProperties[x].textBefore != "Placeholder"){
        //   this.currentTemplate.templateProperties[x].displayText = this.currentTemplate.templateProperties[x].textBefore;
        // }else{
        //   this.currentTemplate.templateProperties[x].displayText = this.currentTemplate.templateProperties[x].fieldName;
        // }
        // if(this.currentTemplate.templateProperties[x].textBefore != "Placeholder"){
        //   this.currentTemplate.templateProperties[x].displayText = this.currentTemplate.templateProperties[x].textBefore;
        // }else{
        //   this.currentTemplate.templateProperties[x].displayText = this.currentTemplate.templateProperties[x].fieldName;
        // }

        if(this.currentTemplate.templateProperties[x].textProperty != "Placeholder"){
          this.currentTemplate.templateProperties[x].displayText = this.currentTemplate.templateProperties[x].textProperty;
        }else{
          this.currentTemplate.templateProperties[x].displayText = this.currentTemplate.templateProperties[x].description;
        }
      }
        console.log(this.currentTemplate.templateProperties);

    setTimeout(()=>{                           //<<<---using ()=> syntax
       this.calculateFontSize()
    //   x.forEach(element => {
    //   // element.setAttribute("font-size", "500px");
    //     //alert(isOverflown(element));
    //     // console.log(element.clientWidth);
    //     // console.log(element.clientHeight);
    //   });
     }, 100);
        //this.getTemplateDisplay();
      }
      }).catch((error: any[])=>{
        console.log(error);
      });
   }

   getTemplates(){
     this.templateService.get().then((data: PosTemplate[])=>{
      if(data){
        this.templateList = data;
        // this.currentTemplate = this.templateList[0];
        // this.getContainerDimensionsFromSize(this.currentTemplate);
        // this.currentTemplate.backgroundImage = 'url("https://localhost:5001/StaticFiles/Resources/Images/' + this.currentTemplate.backgroundImage + '")';
        // this.calculateContainerPosition();
        // this.getTemplateProperties();

      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }



   openPreviewWindow() {
    var url = "https://localhost:5001/StaticFiles/Resources/Images/preview.pdf";
    var target = "blank"
 if(localStorage.getItem(target) == "true")
  {
    window.open(url,target);
  }
  else
    {
      localStorage.setItem(target,"true");
      window.open(url,target);
    }
  }

   getTemplateDisplay(value){
     this.currentPromoId = value;
     this.templateService.createTemplatePreview(value, this.currentTemplate.id).then((data: any)=>{
      this.openPreviewWindow();
     });

    // this.templateService.getTemplateDisplays(value, this.currentTemplate.id).then((data: TemplateDisplay[])=>{
    //  if(data){
    //   this.tempDisplayText = [];
    //    for(let i = 0; i < this.currentTemplate.templateProperties.length; i++){
    //     let tempDisplay = new TempDisplayText();
    //     tempDisplay.id = this.currentTemplate.templateProperties[i].id;
    //     tempDisplay.text = this.currentTemplate.templateProperties[i].fieldName;
    //     this.tempDisplayText.push(tempDisplay);
    //     for(let y = 0; y < data.length; y++){
    //       if(this.currentTemplate.templateProperties[i].id == data[y].templatePropertiesId){
    //         this.currentTemplate.templateProperties[i].fieldName = data[y].displayText;
    //       }
    //     }
    //    }
    //    this.textView = true;
    //    this.calculateFontSize();
    //  }
    //  }).catch((error: any[])=>{
    //    console.log(error);
    //  });;
  }

  getTemplateDisplaySet(){
    if(this.currentPromoId == 0){
      return;
    }
   this.templateService.getTemplateDisplays(this.currentPromoId, this.currentTemplate.id).then((data: TemplateDisplay[])=>{
    if(data){
     this.tempDisplayText = [];
      for(let i = 0; i < this.currentTemplate.templateProperties.length; i++){
       let tempDisplay = new TempDisplayText();
       tempDisplay.id = this.currentTemplate.templateProperties[i].id;
       tempDisplay.text = this.currentTemplate.templateProperties[i].fieldName;
       this.tempDisplayText.push(tempDisplay);
       for(let y = 0; y < data.length; y++){
         if(this.currentTemplate.templateProperties[i].id == data[y].templatePropertiesId){
           this.currentTemplate.templateProperties[i].fieldName = data[y].displayText;
         }
       }
      }
      this.textView = true;
    }
    }).catch((error: any[])=>{
      console.log(error);
    });;
 }

  setDisplayFields(){
    for(let i = 0; i < this.currentTemplate.templateProperties.length; i++){
      for(let x = 0; x < this.tempDisplayText.length; x++){
        if(this.currentTemplate.templateProperties[i].id == this.tempDisplayText[x].id){
          this.currentTemplate.templateProperties[i].fieldName = this.tempDisplayText[x].text;
        }
      }
    }
    this.textView = false;
    this.tempDisplayText = [];
  }

   getContainerDimensionsFromSize(template: PosTemplate){

    let actualWidth = parseInt(template.width);
    let actualHeight = parseInt(template.height);

    this.currentTemplateWidth =  this.convertIntToPixels(actualWidth);
    this.currentTemplateHeight = this.convertIntToPixels(actualHeight);

    this.spaceDivHeight =  this.convertIntToPixels(this.calculateInnerHeight(template.sizeDescription, actualHeight));;
    this.spaceDivWidth = this.convertIntToPixels(this.calculateInnerWidth(template.sizeDescription, actualWidth));
    this.spaceDivLeft = this.convertIntToPixels(this.calculateInnerLeft(template.sizeDescription, actualWidth));
    this.spaceDivTop = this.convertIntToPixels(this.calculateInnerTop(template.sizeDescription, actualHeight));
   }

   calculateInnerWidth(size: string, actualWidth){
     let sizeToClaculateBy = 5;
      let cleanSize = size.replace("mm", "");
      let sizeList = cleanSize.split("x");
      let width = parseInt(sizeList[0]);
      let height = sizeList[1];
      let widthFinalPerc = (sizeToClaculateBy / width);
      let widthFinal = actualWidth - (actualWidth * widthFinalPerc);
      //alert(actualWidth * widthFinalPerc);
      return widthFinal;
   };

   calculateInnerHeight(size: string, actualWidth){
    let sizeToClaculateBy = 5;
      let cleanSize = size.replace("mm", "");
      let sizeList = cleanSize.split("x");
      let width = parseInt(sizeList[1]);
      let widthFinalPerc = (sizeToClaculateBy / width);
      let widthFinal = actualWidth - (actualWidth * widthFinalPerc);
      //alert(actualWidth * widthFinalPerc);
      return widthFinal;
  };

  calculateInnerLeft(size: string, actualWidth){
    let sizeToClaculateBy = 5;
      let cleanSize = size.replace("mm", "");
      let sizeList = cleanSize.split("x");
      let width = parseInt(sizeList[0]);
      let widthFinalPerc = (sizeToClaculateBy / width);
      let widthFinal = actualWidth - (actualWidth * widthFinalPerc);

      let leftPos = (actualWidth - widthFinal) / 2;
      //alert(actualWidth * widthFinalPerc);
      return leftPos;
  };

  calculateInnerTop(size: string, actualWidth){
    let sizeToClaculateBy = 5;
      let cleanSize = size.replace("mm", "");
      let sizeList = cleanSize.split("x");
      let width = parseInt(sizeList[1]);
      let widthFinalPerc = (sizeToClaculateBy / width);
      let widthFinal = actualWidth - (actualWidth * widthFinalPerc);

      let leftPos = (actualWidth - widthFinal) / 2;
      //alert(actualWidth * widthFinalPerc);
      return leftPos;
  };


   setNewTemplateName(value){
    this.newTemplate.templateName = value;
   }

   displayTemplateCreateBox(){
     this.displayTemplateBox = true;
   }


   getFields(){
    this.templatePropertiesService.getFields().then((data: Field[])=>{
      if(data){
        this.FieldList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });
   }

   getBands(){
    this.bandService.GetBands().then((data: Band[])=>{
      if(data){
        this.bandList = data;
        this.currentBandLtter = this.bandList[0].bandLetter;
        this.getPromotions(this.currentBandLtter);
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   gettemplatePacks(){
    this.templateService.getByTemplatePacks().then((data: TemplatePack[])=>{
      if(data){

        this.templatePackList = data;
        console.log(JSON.stringify(this.templatePackList));
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getStores(){
    this.storeService.GetStores().then((data: Store[])=>{
      if(data){
        this.storeList = data;
        console.log(JSON.stringify(this.storeList));
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getStoresBand(value){
    this.storeService.GetStoresByBand(value).then((data: Store[])=>{
      if(data){
        this.storeList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   setNewTemplateSizeId(value){
    this.newTemplate.templateType = value;
   }

   setNewTemplateBandId(value){
    this.newTemplate.bandId = value;
    this.storeService.GetStoresByBand(value).then((data: Store[])=>{
      if(data){
        this.storeList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   onResizeEnd(event: ResizeEvent, num): void {
     let temp = this.currentTemplate.templateProperties[num];
     let currentWidth = parseInt(event.rectangle.width.toString()) + "px";
     let currentHeight = (parseInt(event.rectangle.height.toString())) + "px";
     temp.width = currentWidth;
     temp.height = currentHeight;


     this.templatePropertiesService.getById(temp.id).then((data: TemplateProperties)=>{

      let props = data;
      props.width = currentWidth;
      props.height = currentHeight;
      props = this.calculatePropertiesBackToDefaultSize(props, true);

      // alert(props.width)
      // alert(currentWidth)
      this.updateTemplateProprty(props, currentWidth, currentHeight);
     });

     //alert(temp.topProperty);
    // alert(JSON.stringify(temp.topProperty))
     //temp = this.calculateBackToViewSize(temp);
    // temp = this.calculatePropertiesBackToDefault(temp, true);

    // alert(temp.topProperty);
    // alert(JSON.stringify(temp.topProperty))
    //temp = this.updateTemplateProprty(temp, currentWidth, currentHeight);
  }

  getPromotions(bandLetter: string = "A"){
    this.promosService.getByBand(bandLetter).then((data: Promo[])=>{
     this.promotions = data;
   }).catch((error: any[])=>{
     console.log(error);
   });
  }

  deleteTemplateObject(num){
    delete this.currentTemplate.templateProperties[num];
    alert(this.currentTemplate.templateProperties.length)
    let xxx = this.currentTemplate.templateProperties.filter(x => x !== null)
    alert(this.currentTemplate.templateProperties.length)
    let t = localStorage.getItem('template');

    if(t != null){
     let x: TemplateProperties[] = JSON.parse(t);
     delete x[num];
     localStorage.setItem('template', JSON.stringify(x));
     this.currentTemplate.templateProperties = x;
    }
  }

  closeOptions(num){
    this.currentTemplate.templateProperties[num].active = false;
  }


  setColor(value, num){
    this.currentTemplate.templateProperties[num].color = this.hexToRgbA(value);
    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.color = this.hexToRgbA(value);
      this.updateTemplateProprty(templateProperty);
    });
  }

  setFontWeight(num){
    let fontWeight = this.currentTemplate.templateProperties[num].fontWeight == "bold" ? "normal" : "bold";
    this.currentTemplate.templateProperties[num].fontWeight = fontWeight;
    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.fontWeight = fontWeight;
      this.updateTemplateProprty(templateProperty);
    });
  }

  setField(value, num){
    this.currentTemplate.templateProperties[num].fieldId = value;
    this.currentTemplate.templateProperties[num].textProperty = "Placeholder";


    for(let x = 0; x < this.FieldList.length; x++){
      if(this.FieldList[x].id == value){
        this.currentTemplate.templateProperties[num].fieldName = this.FieldList[x].description;
        this.currentTemplate.templateProperties[num].displayText = this.FieldList[x].description;
      }
    }

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.fieldId = value;
      templateProperty.textProperty = "Placeholder";
      this.updateTemplateProprty(templateProperty);
    });

    //this.updateTemplateProprty(this.currentTemplate.templateProperties[num]);
  }

  setText(num){
    this.updateTemplateProprty(this.currentTemplate.templateProperties[num]);
  }

  setFontSize(value, num){
    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.fontSize = this.currentTemplate.templateProperties[num].fontSize;
      this.updateTemplateProprty(templateProperty);
    });
  }

  setFontStyle(value, num){
    let fStyle = this.currentTemplate.templateProperties[num].fontstyle;
    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.fontstyle = fStyle;
      this.updateTemplateProprty(templateProperty);
    });
  }

  addObject(){
    const newItem = new TemplateProperties();
    newItem.id = 0;
    newItem.templateId = this.currentTemplate.id;
    newItem.height = "50px";
    newItem.width = "100px";
    newItem.color = "rgba(0,0,0,1)";
    newItem.leftProperty = "0px";
    newItem.topProperty = "0px";
    newItem.spacing = "1px";
    newItem.fontSize = "12px";
    newItem.fontWeight = "normal";
    newItem.textProperty = "Placeholder";
    newItem.textBefore = "Placeholder";
    newItem.textAfter = "Placeholder";
    newItem.fontstyle = "Arial";
    newItem.fieldId = 1;
    newItem.fieldName = this.FieldList[0].fieldName;
    newItem.strikethrough = 0;
    newItem.textAlign = "center";
    newItem.rotate = 0;
    newItem.dateFormat = 0;
    newItem.verticalAlign = "Center";

    this.templatePropertiesService.insertTemplateProperties(newItem).then((data: TemplateProperties)=>{
      if(data){
        this.templateSelectOnChange(this.currentTemplate.id)
        //this.currentTemplate.templateProperties.push(data);
      }
    }).catch((error: any[])=>{
      console.log(error);
    });
  }

  setFontSpacing(value, num){
    this.currentTemplate.templateProperties[num].spacing = value;

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.spacing = value;
      this.updateTemplateProprty(templateProperty);
    });
  }

  setPropertyText(value, num){
    if(value == ""){
      value = "Placeholder";
      this.currentTemplate.templateProperties[num].displayText = this.currentTemplate.templateProperties[num].fieldName;
    }else{
      this.currentTemplate.templateProperties[num].displayText = value;
    }

    this.currentTemplate.templateProperties[num].textProperty = value;

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.textProperty = value;
      this.updateTemplateProprty(templateProperty);
    });
  }

  setBeforeText(value, num){

    if(value.trim() == ""){
      value = "Placeholder";
      this.currentTemplate.templateProperties[num].displayText = this.currentTemplate.templateProperties[num].fieldName;
    }else{
      this.currentTemplate.templateProperties[num].displayText = value;
    }

    this.currentTemplate.templateProperties[num].textBefore = value;

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.textBefore = value;
      this.updateTemplateProprty(templateProperty);
    });
  }

  setAfterText(value, num){
    if(value.trim() == ""){
      value = "Placeholder";
      this.currentTemplate.templateProperties[num].displayText = this.currentTemplate.templateProperties[num].fieldName;
    }else{
      this.currentTemplate.templateProperties[num].displayText = value;
    }

    this.currentTemplate.templateProperties[num].textAfter = value;

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.textAfter = value;
      this.updateTemplateProprty(templateProperty);
    });
  }

  setFontStrikeThrough(value, num){
    this.currentTemplate.templateProperties[num].strikethrough = parseInt(value);

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.strikethrough = parseInt(value);
      this.updateTemplateProprty(templateProperty);
    });
  }

  setFontTextAlign(value, num){
    this.currentTemplate.templateProperties[num].textAlign = value;

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.textAlign =value;
      this.updateTemplateProprty(templateProperty);
    });
  }

  setFontVerticalAlign(value, num){
    this.currentTemplate.templateProperties[num].verticalAlign = value;

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.verticalAlign =value;
      this.updateTemplateProprty(templateProperty);
    });
  }

  setFontRotate(value, num){
    this.currentTemplate.templateProperties[num].rotate = parseInt(value);

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.rotate = parseInt(value);
      this.updateTemplateProprty(templateProperty);
    });
  }

  setFontDateFormat(value, num){
    this.currentTemplate.templateProperties[num].dateFormat = parseInt(value);

    this.templatePropertiesService.getById(this.currentTemplate.templateProperties[num].id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.dateFormat = parseInt(value);
      this.updateTemplateProprty(templateProperty);
    });
  }

  onDragStart(num){
    this.currentTemplate.templateProperties[num].active = false;
  }

  onDragEnded(event, num) {
      let id = this.currentTemplate.templateProperties[num].id;

      let element = event.source.getRootElement();
      let boundingClientRect = element.getBoundingClientRect();

      this.templatePropertiesService.getById(id).then((data: TemplateProperties)=>{
      let templateProperty = data;
      templateProperty.topProperty = this.convertIntToPixels(((boundingClientRect.y - this.pageHeaderHeight) + window.pageYOffset) + 3);
      templateProperty.leftProperty = this.convertIntToPixels((boundingClientRect.x - this.convertIntFromPixels(this.currentTemplateLeft)) - 13);

      let filteredList = [];
      if(this.positionAuditList.length > 0){
        filteredList = this.positionAuditList.filter(function (el) {
        return el.id == id;
      });
    }

      if(filteredList.length > 0){
        for(let x = 0; x < this.positionAuditList.length; x++){
          if(this.positionAuditList[x].id == id){
            this.positionAuditList[x].topProperty = templateProperty.topProperty;
            this.positionAuditList[x].leftProperty = templateProperty.leftProperty
          }
        }
      }else{
        let positionAudit = new PositionAudit();
        positionAudit.id = this.currentTemplate.templateProperties[num].id;
        positionAudit.leftProperty = templateProperty.leftProperty;
        positionAudit.topProperty = templateProperty.topProperty;
        this.positionAuditList.push(positionAudit);
      }

      let temp = templateProperty;
      temp = this.calculatePropertiesBackToDefault(temp, true);

      this.updateTemplateProprty(templateProperty);
    }).catch((error: any[])=>{
      console.log(error);
    });

  }

  getPosition(el) {
    let x = 0;
    let y = 0;
    while(el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
      x += el.offsetLeft - el.scrollLeft;
      y += el.offsetTop - el.scrollTop;
      el = el.offsetParent;
    }
    return { top: y, left: x };
  }

  calculateContainerPosition(){
    let windowWidth = window.innerWidth;
    let containerWidth = this.convertIntFromPixels(this.currentTemplateWidth);

    let leftPosition = (windowWidth * 0.5) - (containerWidth * 0.5);
    let rightPosition = (windowWidth * 0.5) + (containerWidth * 0.5);
    this.currentTemplateLeft = this.convertIntToPixels(leftPosition);
    this.currentTemplateRight = this.convertIntToPixels(rightPosition);

    this.createGridDisplay(this.convertIntFromPixels(this.currentTemplate.height), containerWidth);
  }

  styleOptions(event,num){

    if(this.currentTemplate.templateProperties[num].active){
      this.currentTemplate.templateProperties[num].active = false;
      return;
    }

    let id = this.currentTemplate.templateProperties[num].id;
    let leftPosition:string = "";
    let rightPosition:string = "";
    //check if exists in audit
    let filteredList = this.positionAuditList.filter(function (el) {
      return el.id == id;
    });

    let startLeftPos = 0;
    if(filteredList.length > 0){
      startLeftPos = this.convertIntFromPixels(filteredList[0].leftProperty);
    }else{
      startLeftPos = this.convertIntFromPixels(this.currentTemplate.templateProperties[num].leftProperty);
    }
    let startWidth = this.convertIntFromPixels(this.currentTemplate.templateProperties[num].width);
    let rightStart = this.convertIntFromPixels(this.currentTemplateLeft) + startLeftPos + startWidth;
    let calculateRight = this.convertIntFromPixels(this.currentTemplateRight) - rightStart;

    if(filteredList.length > 0){
      leftPosition  = (-this.convertIntFromPixels(filteredList[0].leftProperty) - 204) + "px";
      rightPosition = -calculateRight - 202 + "px";
    }else{
      leftPosition = this.convertIntToPixels((-this.convertIntFromPixels(this.currentTemplate.templateProperties[num].leftProperty) - 204));
      rightPosition = this.convertIntToPixels((-calculateRight - 202));
    }

    let windowWidth = window.innerWidth;
    let xPosition = event.clientX;
    let positionPercent = ((xPosition / windowWidth) * 100);

    if(positionPercent >= 50){
      this.inputOptionsLeftPosition = "";
      this.inputOptionsRightPosition = rightPosition;
    }else{
      this.inputOptionsRightPosition = "";
      this.inputOptionsLeftPosition = leftPosition;
    }

    for(let x = 0; x < this.currentTemplate.templateProperties.length; x++){
      this.currentTemplate.templateProperties[x].active = false;
    }

    let accountForScroll = 0;
    if(window.scrollY > 0){
      if(window.scrollY > 100){
        accountForScroll = window.scrollY - 100;
      }

    }
   this.inputOptionsTopPosition = this.convertIntToPixels((-(this.convertIntFromPixels(this.currentTemplate.templateProperties[num].topProperty)) + accountForScroll));
    // this.inputOptionsLeftPosition = "";
    // this.inputOptionsRightPosition = "20px";
    this.currentTemplate.templateProperties[num].active = !this.currentTemplate.templateProperties[num].active;

    this.setActiveDiv(this.currentTemplate.templateProperties[num].active, num);
  }

  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    this.httpClient.post('https://localhost:5001/UploadTemplateImage/', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)

          this.progress = Math.round(100 * event.loaded / event.total);

          else if (event.type === HttpEventType.Response) {

          this.currentTemplate.backgroundImage = fileToUpload.name;
          this.updateTemplate();
        }
      });
  }

  updateTemplateProprty(templateProperty:TemplateProperties, currentWidth = "", currentHeight = ""): TemplateProperties{

    this.templatePropertiesService.updateTemplateProperties(templateProperty).then((data: TemplateProperties)=>{
    //  if(currentWidth == ""){
    //   templateProperty.width = currentWidth;
    //   templateProperty.height = currentHeight;
    //  }

    //  return templateProperty;
    }).catch((error: any[])=>{

      console.log(error);
    });
    return templateProperty;
  }

  updateTemplate(){
    this.templateService.updateTemplate(this.currentTemplate).then((data: PosTemplate)=>{
    this.currentTemplate = data;
    this.currentTemplate.backgroundImage = 'url("https://localhost:5001/StaticFiles/Resources/Images/' + this.currentTemplate.backgroundImage + '")';
    this.getTemplateProperties();
    //alert(JSON.stringify(this.currentTemplate));
  }).catch((error: any[])=>{
    console.log(error);
  });
  }

  hideAllFields(){
    for(let x=0; x < this.currentTemplate.templateProperties.length; x++){
      this.currentTemplate.templateProperties[x].active = false;
    }
  }

  convertIntFromPixels(string: string): number{
    return parseInt(string.replace("px",""));
  }

  convertIntToPixels(int): string{
    let value = parseInt(int);
    return value + "px";
  }


  deleteTemplateProperty(id:number, index:number){
    this.templatePropertiesService.delete(id).then((data: boolean)=>{
      for(let x=0; x <this.currentTemplate.templateProperties.length; x++){
        if(this.currentTemplate.templateProperties[x].id == id){
          this.currentTemplate.templateProperties.splice(x, 1);
        }
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
  }

  getPromoDisplayType(){
    this.storeOfferService.GetPromoDisplayType().then((data: PromoDisplaytype[])=>{
      if(data){
        this.promoDisplayTypeList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }


   getFontsByBand(bandId:number){
    this.templatePropertiesService.getFontsByBand(bandId).then((data: Fonts[])=>{
      if(data){
        this.FontList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
  }

  calculateNegativeZoom(){


    // let parentHeight = document.getElementById('two').style.height.replace("px","");
    // let  parentWidth = document.getElementById('two').style.width.replace("px","");

    // let childHeight = document.getElementById('two-child').style.height.replace("px","");
    // let  childWidth = document.getElementById('two-child').style.width.replace("px","");
    // var testDiv = document.getElementById("two-child").offsetTop;;
    // let editWidth = 100;
    // let editHeight = 100;
    // let display = parseInt(parentWidth.replace("px",""));
    // let displayHeight = parseInt(parentHeight.replace("px",""));

    // let widthDiff: any = (((editWidth / display)/ 100) * 100);
    // let heightDiff = (((editHeight / displayHeight)/ 100) * 100);

    // let parentWidthDiff = (parentWidth * widthDiff);
    // let childWidthDiff = (childWidth * widthDiff);

    // let parentHeightDiff = (parentHeight * heightDiff);
    // let childHeighthDiff = (childHeight * heightDiff);
    // let childTopDiff = (testDiv * heightDiff);

    // document.getElementById('two').style.width = parentWidthDiff + "px";
    // document.getElementById('two-child').style.width = childWidthDiff + "px";

    // document.getElementById('two').style.height = parentHeightDiff + "px";
    // document.getElementById('two-child').style.height = childHeighthDiff + "px";
    // document.getElementById('two-child').style.top = childTopDiff + "px";
    // alert(childTopDiff)
  }

  public currentZoom = 0;

  calculateZoomAdd(){
  //   if(this.currentTemplateDefaultWidth > 500){
  //     if(this.currentZoom < 3){
  //       this.currentZoom +=1;
  //     }else if(this.currentTemplateDefaultWidth < 500){
  //       if(this.currentZoom < 5){
  //         this.currentZoom +=1;
  //       }else {
  //       return;
  //     }
  //   }
  // }
    let parentWidth = this.convertIntFromPixels(this.currentTemplate.width);
    let parentHeight = this.convertIntFromPixels(this.currentTemplate.height);

    let editWidth = parentWidth * 1.2;
    let editHeight = parentHeight * 1.2;

    let display = parentWidth;
    let displayHeight = parentHeight;

   let widthDiff: number = (((editWidth / display)/ 100) * 100);
   let heightDiff: number = (((editHeight / displayHeight)/ 100) * 100);

   let parentWidthDiff = (parentWidth * widthDiff);
   let parentHeightDiff = (parentHeight * heightDiff);
//let childWidthDiff = (childWidth * widthDiff);

   this.currentTemplate.width = parentWidthDiff + "px";
   this.currentTemplate.height = parentHeightDiff + "px";



    this.getContainerDimensionsFromSize(this.currentTemplate);
       this.calculateContainerPosition();

       for(let x = 0; x < this.currentTemplate.templateProperties.length; x++){
        let childWidthDiff: number = (this.convertIntFromPixels(this.currentTemplate.templateProperties[x].width) * widthDiff);
        let childHeighthDiff: number = (this.convertIntFromPixels(this.currentTemplate.templateProperties[x].height) * heightDiff);

        let childLefthDiff: number = (this.convertIntFromPixels(this.currentTemplate.templateProperties[x].leftProperty) * widthDiff);
        let childTophDiff: number = (this.convertIntFromPixels(this.currentTemplate.templateProperties[x].topProperty) * heightDiff);

        this.currentTemplate.templateProperties[x].width = childWidthDiff + "px";
        this.currentTemplate.templateProperties[x].height = childHeighthDiff + "px";
        this.currentTemplate.templateProperties[x].topProperty = childTophDiff + "px";
        this.currentTemplate.templateProperties[x].leftProperty = childLefthDiff + "px";

        //alert(this.currentTemplate.templateProperties[x].width);
       }

       this.calculateFontSize();
  // alert(this.currentTemplate.width);
   // this.getContainerDimensionsFromSize(this.currentTemplate);
   // this.calculateContainerPosition();
   // this.getTemplateProperties();
  }

  toggleGrid(){
    this.displayGrid = !this.displayGrid;
    this.displayBorder = false;
  }

  toggleBorder(){
    this.displayBorder = !this.displayBorder;
    this.displayGrid = false;
  }

  calculateZoomSubtract(){

    this.templateSelectOnChange(this.currentTemplate.id);

    return;
    let  parentWidth = this.convertIntFromPixels(this.currentTemplate.width);
    let parentHeight = this.convertIntFromPixels(this.currentTemplate.height);

    let editWidth = parentWidth * 0.8;
    let editHeight = parentHeight * 0.8;

    if(editWidth < this.currentTemplateDefaultWidth){
      editWidth = this.currentTemplateDefaultWidth;
      editHeight = this.currentTemplateDefaultHeight;
    }

    let display = parentWidth;
    let displayHeight = parentHeight;

   let widthDiff: number = (((editWidth / display)/ 100) * 100);
   let heightDiff: number = (((editHeight / displayHeight)/ 100) * 100);

   let parentWidthDiff = (parentWidth * widthDiff);
   let parentHeightDiff = (parentHeight * heightDiff);

   this.currentTemplate.width = parentWidthDiff + "px";
   this.currentTemplate.height = parentHeightDiff + "px";

    this.getContainerDimensionsFromSize(this.currentTemplate);
    this.calculateContainerPosition();

       for(let x = 0; x < this.currentTemplate.templateProperties.length; x++){
        let childWidthDiff: number = (this.convertIntFromPixels(this.currentTemplate.templateProperties[x].width) * widthDiff);
        let childHeighthDiff: number = (this.convertIntFromPixels(this.currentTemplate.templateProperties[x].height) * heightDiff);

        let childLefthDiff: number = (this.convertIntFromPixels(this.currentTemplate.templateProperties[x].leftProperty) * widthDiff);
        let childTophDiff: number = (this.convertIntFromPixels(this.currentTemplate.templateProperties[x].topProperty) * heightDiff);

        this.currentTemplate.templateProperties[x].width = childWidthDiff + "px";
        this.currentTemplate.templateProperties[x].height = childHeighthDiff + "px";
        this.currentTemplate.templateProperties[x].topProperty = childTophDiff + "px";
        this.currentTemplate.templateProperties[x].leftProperty = childLefthDiff + "px";
        //alert(JSON.stringify(this.currentTemplate.templateProperties[x]))
       }

       //this.calculateFontSizeMinus()
  }

  calculatePropertiesBackToDefault(templatePropert: TemplateProperties, shouldCalc: boolean): TemplateProperties{
    if(shouldCalc){
      if(this.convertIntFromPixels(this.currentTemplateHeight) > this.currentTemplateDefaultHeight){
        templatePropert = this.calculateBackToDefaultSubtact(templatePropert);
      }else{
        templatePropert = this.calculateBackToDefaultAdd(templatePropert);
      }
    }

    return templatePropert;
  }

  calculatePropertiesBackToDefaultSize(templatePropert: TemplateProperties, shouldCalc: boolean): TemplateProperties{
    if(shouldCalc){
      if(this.convertIntFromPixels(this.currentTemplateHeight) > this.currentTemplateDefaultHeight){
        templatePropert = this.calculateBackToDefaultSubtactSize(templatePropert);
      }else{
        templatePropert = this.calculateBackToDefaultAdd(templatePropert);
      }
    }

    return templatePropert;
  }

  calculateBackToDefaultSubtact(templatePropert: TemplateProperties): TemplateProperties{
    let  parentWidth = this.convertIntFromPixels(this.currentTemplateWidth);
    let parentHeight = this.convertIntFromPixels(this.currentTemplateHeight);
    //alert(parentWidth);
    let diffWidth = (this.currentTemplateDefaultWidth / parentWidth);
    let diffHeight = (this.currentTemplateDefaultHeight / parentHeight);
    //let rounded = Math.round(diffWidth);

    let propLeft = this.convertIntFromPixels(templatePropert.leftProperty);
    let newLeft = Math.round((propLeft * diffWidth));

    let propTop = this.convertIntFromPixels(templatePropert.topProperty);
    let newTop = Math.round((propTop * diffHeight));

    templatePropert.leftProperty = this.convertIntToPixels(newLeft);
    templatePropert.topProperty = this.convertIntToPixels(newTop);
    return templatePropert;
  }

  calculateBackToDefaultSubtactSize(templatePropert: TemplateProperties): TemplateProperties{
    let  parentWidth = this.convertIntFromPixels(this.currentTemplateWidth);
    let parentHeight = this.convertIntFromPixels(this.currentTemplateHeight);
    //alert(parentWidth);
    let diffWidth = (this.currentTemplateDefaultWidth / parentWidth);
    let diffHeight = (this.currentTemplateDefaultHeight / parentHeight);
    //let rounded = Math.round(diffWidth);

    let width = this.convertIntFromPixels(templatePropert.width);
    let newWidth = Math.round((width * diffWidth));

    let height = this.convertIntFromPixels(templatePropert.height);
    let newHeight = Math.round((height * diffHeight));

    templatePropert.width = this.convertIntToPixels(newWidth);
    templatePropert.height = this.convertIntToPixels(newHeight);
    return templatePropert;
  }

  calculateBackToDefaultAdd(templatePropert: TemplateProperties): TemplateProperties{
    let  parentWidth = this.convertIntFromPixels(this.currentTemplateWidth);
    let parentHeight = this.convertIntFromPixels(this.currentTemplateHeight);
    //alert(parentWidth);
    let diffWidth = (this.currentTemplateDefaultWidth / parentWidth);
    let diffHeight = (this.currentTemplateDefaultHeight / parentHeight);
    //let rounded = Math.round(diffWidth);

    let propLeft = this.convertIntFromPixels(templatePropert.leftProperty);
    let newLeft = Math.round((propLeft * diffWidth));

    let propTop = this.convertIntFromPixels(templatePropert.topProperty);
    let newTop = Math.round((propTop * diffHeight));

    templatePropert.leftProperty = this.convertIntToPixels(newLeft);
    templatePropert.topProperty = this.convertIntToPixels(newTop);
    return templatePropert;
  }


  calculateBackToViewSize(templatePropert: TemplateProperties): TemplateProperties{
    let  parentWidth = this.convertIntFromPixels(this.currentTemplateWidth);
    let parentHeight = this.convertIntFromPixels(this.currentTemplateHeight);
    //alert(parentWidth);
    let diffWidth = (this.currentTemplateDefaultWidth / parentWidth);
    let diffHeight = (this.currentTemplateDefaultHeight / parentHeight);

    let curentWidth = this.convertIntFromPixels(templatePropert.width);
    let currentHeight = this.convertIntFromPixels(templatePropert.height);
    //alert(curentWidth);

    let rounded = Math.round(diffWidth);

    //let propLeft = this.convertIntFromPixels(templatePropert.leftProperty);
    let newWidth = Math.round((curentWidth * diffWidth));
    //let propTop = this.convertIntFromPixels(templatePropert.topProperty);
    let newHeight = Math.round((currentHeight * diffHeight));

    templatePropert.width = this.convertIntToPixels(newWidth);
    templatePropert.height = this.convertIntToPixels(newHeight);
    return templatePropert;
  }

  hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
    }
    throw new Error('Bad Hex');
}

setActiveDiv(value, num){
  for(let x= 0; x < this.currentTemplate.templateProperties.length; x++){
    this.currentTemplate.templateProperties[x].zindex = 100;
  }
  if(value){
    this.currentTemplate.templateProperties[num].zindex = 9999;
    return;
  }
  this.currentTemplate.templateProperties[num].zindex = 100;
}

setActiveDivNew(value, num){
  for(let x= 0; x < this.currentTemplate.templateProperties.length; x++){
    this.currentTemplate.templateProperties[x].zindex = 100;
  }
  if(value){
    this.currentTemplate.templateProperties[num].zindex = 9999;
    return;
  }
  this.currentTemplate.templateProperties[num].zindex = 100;
}

public verticalDivs: GridDisplay[] = [];
    public horizontalDivs: GridDisplay[] = [];
    createGridDisplay(maxHeight:number, maxWidth:number){
        let maxVertical = maxHeight / 20;
        let maxHorizontal = maxWidth / 20;
        //alert(maxHorizontal)
        let countVertical = 0;
        for(let x = 0; x < maxHorizontal;x++){
          let newDiv = new GridDisplay();
          newDiv.height = maxHeight + "px";
          newDiv.width = "1px";
          newDiv.left = countVertical + "px";
          newDiv.top = "0px";
          this.verticalDivs.push(newDiv);
          countVertical += 20;
        }

        countVertical = 0;
        for(let x = 0; x < maxVertical;x++){
          let newDiv = new GridDisplay();
          newDiv.height = "1px";
          newDiv.width = maxWidth + "px";
          newDiv.top = countVertical + "px";
          newDiv.left = "0px";
          this.horizontalDivs.push(newDiv);
          countVertical += 20;
        }

    }


  ngOnInit() {
  }


}
