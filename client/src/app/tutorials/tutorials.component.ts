import { Component, ElementRef, OnInit } from '@angular/core';
import { AccessService } from '../service/access.service';
import { StorerequestsService } from '../service/storerequests.service';
import { StoreRequest } from '../shared/models/StoreRequest';
import { Tutorial } from '../shared/models/Tutorial';
import { TutorialType } from '../shared/models/TutorialType';

@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorials.component.html',
  styleUrls: ['./tutorials.component.css']
})
export class TutorialsComponent implements OnInit {
  public defaultOffersView: boolean;
  public tutorialList: Tutorial[] = [];
  public tutorialTypeList: TutorialType[] = [];
  public tutorialListDisplay: Tutorial[] = [];
  public selectedVideo: string = "";
  public videoSelected: boolean = false;
  public isAdmin:boolean = false;
  public videoView: boolean = false;

  constructor(private storeRequestService: StorerequestsService, private elRef: ElementRef,
    private accessService: AccessService) {
    this.getTutorials();
    this.gotoHome();
   }

   gotoHome(){
    this.isAdmin = this.accessService.isAdmin();
   }

   removeOptionsBasedOnUser(){

      for(let x = 0; x < this.tutorialList.length; x++){
        if(!this.isAdmin){
          if(this.tutorialList[x].tutorialName == "Store Requests"){
            this.tutorialListDisplay.push(this.tutorialList[x]);
          }
        }else{
          this.tutorialListDisplay.push(this.tutorialList[x]);
        }

      }

   }

   getTutorials(){
    this.storeRequestService.GetTutorials().then((data: Tutorial[])=>{
     this.tutorialList = data;
     this.removeOptionsBasedOnUser();
		}).catch((error: any[])=>{
			console.log(error);
		});
   }

   getTutorialTypes(value: number){
    this.videoSelected = false;
    for(let x= 0; x < this.tutorialList.length; x++){
      this.tutorialList[x].active = false;
      if(this.tutorialList[x].tutorialId == value){
        this.tutorialList[x].active = true;
      }
    }

    this.storeRequestService.GetTutorialTypes(value).then((data: TutorialType[])=>{
     this.tutorialTypeList = data;
     this.videoView = true;

		}).catch((error: any[])=>{
			console.log(error);
		});
   }

   setSelectVideo(value){
    for(let x= 0; x < this.tutorialTypeList.length; x++){
      this.tutorialTypeList[x].active = false;
      if(this.tutorialTypeList[x].tutorialId == value){
       this.tutorialTypeList[x].active = true;
       this.selectedVideo = this.tutorialTypeList[x].folder;
       this.videoSelected = true;
      }
    }
    const player = this.elRef.nativeElement.querySelector('video');
    player.load();
   }

  ngOnInit() {
  }

}
