import { AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';

import {UserServiceService} from '../service/user-service.service';
import { Band } from '../shared/models/BandModel';
import { DepartmentModel } from '../shared/models/DepartmentModel';
import { ItemType } from '../shared/models/ItemType';
import { Promo } from '../shared/models/PromoModel';
import {PromosService} from '../service/promos.service';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { Subject } from 'rxjs/internal/Subject';
import { BandService } from '../service/band.service';
import { Store } from '../shared/models/StoreModel';
import { PromotionSorting } from '../shared/models/PromotionSorting';
import { StoreService } from '../service/store.service';
import { StoreOffer } from '../shared/models/StoreOffer';
import { StoreOfferService } from '../service/store-offer.service';
import { StoreOfferDb } from '../shared/models/StoreOfferDb';
import { identifierModuleUrl } from '@angular/compiler';
import { StoreOfferStatus } from '../shared/models/StoreOfferStatus';
import { PromoDisplaytype } from '../shared/models/PromoDisplayType';
import { OfferType } from '../shared/models/OfferType';
import { OfferDisplayType } from '../shared/models/OfferDisplayType';
import { OfferTypeDisplayTypeOverride } from '../shared/models/OfferTypeDisplayTypeOverride';
import { Order } from '../shared/models/Order';
import { OrderDto } from '../shared/models/OrderDto';
import { OrderDtoDisplay } from '../shared/models/OrderDtoDisplay';
import { TemplateService } from '../service/template.service';
import { DataTableDirective } from 'angular-datatables';
import { PrinterDto } from '../shared/models/PrinterDto';
import { Router } from '@angular/router';
import { AccessService } from '../service/access.service';
import { StoreRequest } from '../shared/models/StoreRequest';
import { StoreRequestDisplay } from '../shared/models/StoreRequestDisplay';
import { StorerequestsService } from '../service/storerequests.service';
import { StoreRequestTemplates } from '../shared/models/StoreRequestTemplates';
import { StoreRequestAction } from '../shared/models/StoreRequestAction';
import { StoreRequestActionDisplay } from '../shared/models/StoreRequestActionDisplay';
import { StoreRequestActionDisplayProduct } from '../shared/models/StoreRequestActionDisplayProduct';
import { RequestOrder } from '../shared/models/RequestOrders';
import { Island } from '../shared/models/Island';
import { GridDisplay } from '../shared/models/GridDisplay';
import { TemplatepropertiesService } from '../service/templateproperties.service';
import { Field } from '../shared/models/Field';
import { ReactiveFormsModule } from '@angular/forms';
import { strictEqual } from 'assert';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit, OnDestroy, OnInit {

  //We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: any = {};
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  isDtInitialized:boolean = false;


  public promotions: Promo[] = [];
  public updatedPromotions: Promo[] = new Array();
  public products = [];
  public bands: Band[] = [];
  public storeList: Store[];
  public islandList:Island[];
  public storeListAll: Store[];
  public storeOffersList: StoreOfferDb[];
  public storeOfferStatusList: StoreOfferStatus[] = [];
  public promoDisplayType: PromoDisplaytype[] = [];
  public departments: DepartmentModel[] = [];
  public itemTypes:ItemType[] = [];
  public selectedValue:Band;
  public selectedDepartment:Store;
  public selectedItemType:ItemType;
  public fileToUpload: File | null = null;
  public promotionSorting = new PromotionSorting();
  public offerTypeList: OfferType[] = [];
  public selectedOfferTypeId: number = 0;
  public orderList: Order[] = [];
  public orderDtoList: OrderDto[] = [];
  public orderDtoListDisplay: OrderDtoDisplay[] = [];
  public printerList: PrinterDto[] = [];
  public currentPrinterId: number = 0;
  public currentOrderId: number = 0;
  public currentcurrentRequestOrderId: number = 0;
  public filterList: string[] = [];

  public bandLetter = "";

  public cartView = false;
  public promoView = true;
  public cartViewStore = false;
  public orderView = false;
  public newOrderView: boolean = false;
  public activeOffers: boolean = false;
  public activeOrders: boolean = false;
  public activeStoreOffers: boolean = false;
  orderByCurrentOrPast: boolean = true;
  public isAdmin: boolean = false;

  public displayRequestOption = false;
  public currentStoreRequest: StoreRequest = new StoreRequest();
  public storeRequestDisplay: StoreRequestDisplay = new StoreRequestDisplay();
  public promoDisplayTypeList: PromoDisplaytype[] = [];
  public storeRequestTemplateList: StoreRequestTemplates[] = [];
  public currentStoreRequetsTemplate: StoreRequestTemplates = new StoreRequestTemplates();
  public storeRequestActionList: StoreRequestAction[] = [];
  public storeRequestActionDisplayList: StoreRequestActionDisplay[] = [];
  public storeRequestActionDisplayListProduct: StoreRequestActionDisplayProduct[] = [];
  public storeRequestOrderList: RequestOrder[] = [];
  public FieldList : Field [] = [];
  public currentIsland = "";
  public searchPhrase: string = "";
  public orderField: string = "";

  public progress: number;
  public message: string;
  @Output() public onUploadFinished = new EventEmitter();

  constructor(private userService: UserServiceService, private promosService: PromosService, private httpClient: HttpClient,
    private bandService: BandService, private storeService: StoreService, private storeOfferService: StoreOfferService,
    private templateService: TemplateService, private router: Router, private accessService: AccessService,
    private storeRequestService: StorerequestsService,  private templatePropertiesService: TemplatepropertiesService) {
      this.gotoHome();
      this.getBands();
      this.getDepartments();
      this.getItemTypes();
      this.getAllStores();
      this.getPromoDisplayType();
      this.getOrders();
      this.getPrinters();
      this.getPromoDisplayTypeSize();
      this.getFields();
      this.storeRequestDisplay.buyerOffer = new Promo();
      this.storeRequestDisplay.stroreRequest = new StoreRequest();
      this.currentIsland = "0";
   }

   gotoHome(){
   this.isAdmin = this.accessService.isAdmin();
  }

  getPromoDisplayTypeSize(){
    this.storeOfferService.GetPromoDisplayType().then((data: PromoDisplaytype[])=>{
      if(data){
        this.promoDisplayTypeList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   filterPromotionsByDate(){
     let tempPromos: Promo[] = [];
    for(let x = 0; x < this.promotions.length; x++){
      const promoendDate = new Date(this.promotions[x].promEndRetDate.replace(" 00:00:00", ""));
      const today = new Date();
      const difference = today.getTime() - promoendDate.getTime()
      if(difference < 0){
        this.promotions[x].promStartRetDate = this.promotions[x].promStartRetDate.replace(" 00:00:00", "");
        this.promotions[x].promEndRetDate = this.promotions[x].promEndRetDate.replace(" 00:00:00", "");
        tempPromos.push(this.promotions[x]);
      }
    }
    this.promotions = tempPromos;
   }

   sortPromotions(value:string){
     if(value == this.promotionSorting.MultiBuyPrice){
      this.promotions.sort((a, b) => (a.multibuyPrice > b.multibuyPrice) ? 1 : -1)
     }

     if(value == this.promotionSorting.Slogan){
      this.promotions.sort((a, b) => (a.slogan < b.slogan) ? 1 : -1)
     }
   }

   getPromotions(){
     this.promosService.get().then((data: Promo[])=>{
      this.promotions = data;
      this.filterPromotionsByDate();
      //this.getStoreOffers();
		}).catch((error: any[])=>{
      console.log(error);
		});
   }

   getPromotionsByBand(band:string){
    this.promosService.getByBand(band).then((data: Promo[])=>{
      this.promotions = data;
     // alert(JSON.stringify(data))
      this.filterPromotionsByDate();
      //this.rerender();
      this.getStoreOffersByBand();
    }).catch((error: any[])=>{
      console.log(error);
    });
  }

  getPromotionsByBandWithOrder(orderby:string){
    let band = this.selectedValue;
    this.orderField = orderby;

   //alert(this.currentIsland);
// return;
if(this.orderField == ""){
  this.filterList = [];
  this.searchPhrase = "";
  if(this.currentIsland != "0"){
    this.getPromotionsByBandAndIslnad(this.currentIsland);
  }else{
    this.getPromotionsByBand(this.selectedValue.bandLetter);
  }
 }else{
  this.getFilteredPromotions();
 }

  }

  setFilteredList(){
       if(this.orderField != ""){
        this.FieldList.forEach(element => {
          if(element.description == this.orderField){
            let x = element.fieldName.charAt(0).toLowerCase() + element.fieldName.slice(1);
              this.filterList = [...new Set(this.promotions.map(item => item[x]))];
          }
        });
    }
  }


  getFilteredPromotions(){
    if(this.orderField == ""){
      alert("Please select an Order Field");
      return;
     }

  if(this.currentIsland != "0" && this.searchPhrase == "" && this.orderField == ""){
    this.promosService.getByBandAndIsland(this.selectedValue.bandLetter, this.currentIsland).then((data: Promo[])=>{
      this.promotions = data;

      this.filterPromotionsByDate();
      //this.rerender();
      this.getStoreOffersByBand();

      this.setFilteredList();
    }).catch((error: any[])=>{
      console.log(error);
    });
  }else if(this.currentIsland == "0" && this.orderField != "" && this.searchPhrase == ""){
    this.promosService.getByBandWithOrder(this.selectedValue.bandLetter,this.orderField).then((data: Promo[])=>{
      this.promotions = data;
      this.filterPromotionsByDate();
      //this.rerender();
      this.getStoreOffersByBand();
      this.setFilteredList();
    }).catch((error: any[])=>{
      console.log(error);
    });
  }
  else if(this.currentIsland == "0" && this.orderField !="" && this.searchPhrase != ""){
    this.promosService.getByBandWithOrderFilter(this.selectedValue.bandLetter,this.orderField, this.searchPhrase).then((data: Promo[])=>{
      this.promotions = data;
      this.filterPromotionsByDate();
      //this.rerender();
      this.getStoreOffersByBand();
      this.setFilteredList();
    }).catch((error: any[])=>{
      console.log(error);
    });
  }else if(this.currentIsland != "0" && this.orderField !="" && this.searchPhrase != ""){
    this.promosService.getByBandAndIslandWithOrderFilter(this.selectedValue.bandLetter,this.currentIsland, this.orderField, this.searchPhrase).then((data: Promo[])=>{
      this.promotions = data;
      this.filterPromotionsByDate();
      //this.rerender();
      this.getStoreOffersByBand();
      this.setFilteredList();
    }).catch((error: any[])=>{
      console.log(error);
    });
  }
  else if(this.currentIsland != "0" && this.orderField !="" && this.searchPhrase == ""){
    this.promosService.getByBandAndIslandWithOrder(this.selectedValue.bandLetter,this.currentIsland, this.orderField).then((data: Promo[])=>{
      this.promotions = data;
      this.filterPromotionsByDate();
      //this.rerender();
      this.getStoreOffersByBand();
      this.setFilteredList();
    }).catch((error: any[])=>{
      console.log(error);
    });
  }
  }

  getPromotionsByBandWithOrderFilter(filterBy:string){

    this.searchPhrase = filterBy;

   if(this.orderField == ""){
    alert("Please select an Order Field");
    return;
   }

if(this.currentIsland != "0" && this.searchPhrase == "" && this.orderField == ""){
  this.promosService.getByBandAndIsland(this.selectedValue.bandLetter, this.currentIsland).then((data: Promo[])=>{
    this.promotions = data;
    this.filterPromotionsByDate();
    //this.rerender();
    this.getStoreOffersByBand();
  }).catch((error: any[])=>{
    console.log(error);
  });
}else if(this.currentIsland == "0" && this.orderField != "" && this.searchPhrase == ""){
  this.promosService.getByBandWithOrder(this.selectedValue.bandLetter,this.orderField).then((data: Promo[])=>{
    this.promotions = data;
    this.filterPromotionsByDate();
    //this.rerender();
    this.getStoreOffersByBand();
  }).catch((error: any[])=>{
    console.log(error);
  });
}
else if(this.currentIsland == "0" && this.orderField !="" && this.searchPhrase != ""){
  this.promosService.getByBandWithOrderFilter(this.selectedValue.bandLetter,this.orderField, this.searchPhrase).then((data: Promo[])=>{
    this.promotions = data;
    this.filterPromotionsByDate();
    //this.rerender();
    this.getStoreOffersByBand();
  }).catch((error: any[])=>{
    console.log(error);
  });
}else if(this.currentIsland != "0" && this.orderField !="" && this.searchPhrase != ""){
  this.promosService.getByBandAndIslandWithOrderFilter(this.selectedValue.bandLetter,this.currentIsland, this.orderField, this.searchPhrase).then((data: Promo[])=>{
    this.promotions = data;
    this.filterPromotionsByDate();
    //this.rerender();
    this.getStoreOffersByBand();
  }).catch((error: any[])=>{
    console.log(error);
  });
}
else if(this.currentIsland != "0" && this.orderField !="" && this.searchPhrase == ""){
  this.promosService.getByBandAndIslandWithOrder(this.selectedValue.bandLetter,this.currentIsland, this.orderField).then((data: Promo[])=>{
    this.promotions = data;
    this.filterPromotionsByDate();
    //this.rerender();
    this.getStoreOffersByBand();
  }).catch((error: any[])=>{
    console.log(error);
  });
}

  }

  insertMultipleStoreOrders(){
    let list: number[] = [];

    this.promotions.forEach(element => {
      list.push(element.buyersOfferID);
    });
    this.promosService.insertMultipleOrders(list).then((data: Promo[])=>{
      alert("complete");
      this.getStoreOffersByBand();
    });
  }

  deleteMultipleStoreOrders(){
    let list: number[] = [];

    this.promotions.forEach(element => {
      list.push(element.buyersOfferID);
    });
    this.promosService.deleteMultipleOrders(list).then((data: Promo[])=>{
      alert("complete");
      this.getStoreOffersByBand();
    });
  }

  getPromotionsByBandAndIslnad(island:string){
    this.currentIsland = island;

    if(this.currentIsland == "0"){
      this.getPromotionsByBand(this.bandLetter);
    }else{
      this.promosService.getByBandAndIsland(this.bandLetter, island).then((data: Promo[])=>{
        this.currentIsland = island;
       this.promotions = data;
       this.filterPromotionsByDate();
       //this.rerender();
       this.getStoreOffersByBand();
     }).catch((error: any[])=>{
       console.log(error);
     });
    }
  }

  getPromotionsByUser(){
    let user = this.accessService.getUser();

    this.promosService.getByUser(user.userid).then((data: Promo[])=>{
     this.promotions = data;
     this.filterPromotionsByDate();
     this.rerender();
     this.getStoreOffersByBand();
   }).catch((error: any[])=>{
     console.log(error);
   });
  }

  getPromotionsByBandAndStore(band:string, storeid: string){
    this.promosService.getByBandAndStore(band, storeid).then((data: Promo[])=>{
     this.promotions = data;
     this.filterPromotionsByDate();
     this.setPromoCheckedFlags();
   }).catch((error: any[])=>{
     console.log(error);
   });
  }

  getPromosByBandAndStore(value){
    this.getPromotionsByBandAndStore(this.bandLetter,value);
  }

   updatePromotions(){

    this.promosService.put(this.promotions).then((data: any)=>{
      this.updatedPromotions = [];
     alert("Update Successful");
		}).catch((error: any[])=>{
			console.log(error);
		});
    }


   getBands(){
    this.bandService.GetBands().then((data: Band[])=>{
      if(data){
        this.bands = data;
        this.selectedValue = this.bands[0];
        this.getStoresByBandId(this.selectedValue.id);
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;

     this.selectedValue = this.bands[0];
   }

   getStoresByBandId(value){
     this.bands.forEach(element => {
       if(element.id == value){
        this.selectedValue = element;
       }
     });

    if(value == "0"){
      this.getPromotions();
    }else{
      for(let x=0; x < this.bands.length; x++){
        if(this.bands[x].id == value){
          this.bandLetter = this.bands[x].bandLetter;
          this.selectedValue = this.bands[x];
        }
      }

      if(this.isAdmin){
        this.getPromotionsByBand(this.bandLetter);
       // this.getStoreRequestsByBand(this.bandLetter);
        this.getActiveStoreRequestsByBand(this.bandLetter);
        this.getOfferTypeByband(this.selectedValue.id);
      }else{
        this.getPromotionsByUser();
        this.getActiveStoreRequestsByStore(this.accessService.getUser().storeId);
      }
    }



    this.storeService.GetIslandsByBand(value).then((data: Island[])=>{
      if(data){
        this.islandList = data;
      //  alert(JSON.stringify(this.islandList))
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getAllStores(){
    this.storeService.GetStores().then((data: Store[])=>{
      if(data){
        this.storeListAll = data;
        //this.selectedDepartment = this.storeList[0];
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getDepartments(){
    let selectvalue = new DepartmentModel();
    selectvalue.id = 0;
    selectvalue.name = "Select Department";
    this.departments.push(selectvalue);
    let department = new DepartmentModel();
    department.id = 1;
    department.name = "Chilled";
    this.departments.push(department);
    let department2 = new DepartmentModel();
    department2.id = 1;
    department2.name = "Crisps";
    this.departments.push(department2);

    ///this.selectedDepartment = this.departments[0];
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    alert(this.fileToUpload.name);
}

  getItemTypes(){
    let type3 = new ItemType();
    type3.id = 1;
    type3.name = "Select Item Type";
    this.itemTypes.push(type3);
    let type = new ItemType();
    type.id = 1;
    type.name = "Single";
    this.itemTypes.push(type);
    let type2 = new ItemType();
    type2.id = 1;
    type2.name = "Bulk";
    this.itemTypes.push(type2);
    this.selectedItemType = this.itemTypes[0];


  }

  search(){
    alert(this.selectedValue.name);
    //alert(this.selectedDepartment.name);
  }

   submit(){
     alert(JSON.stringify(this.promotions))
   }

   getData(){
    this.userService.get().then((data: any[])=>{
			console.log(data);
			this.products = data;
		}).catch((error: any[])=>{
			console.log(error);
		});
   }

  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);

    this.httpClient.post('https://localhost:5001/upload/', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
         // alert("File Uploaded")
          this.message = 'Upload success.';
          this.onUploadFinished.emit(event.body);
          this.getPromotions();
        }
      });
  }

  onChangePromotion(promo:Promo){
    for(let x = 0; x < this.promotions.length; x++){
      if(this.promotions[x].buyerUserID == promo.buyerUserID){
        this.updatedPromotions.push(this.promotions[x]);
      }
    }
  }

  getPromoDisplayType(){
    this.storeOfferService.GetPromoDisplayType().then((data: PromoDisplaytype[])=>{
      if(data){
        this.promoDisplayType = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

  ngOnInit() {

  }

  //Shopping cart
  cartItems: StoreOffer[] = [];
  cartItemsStore: StoreOffer[] = [];
  cartLength: number = 0;
  cartLengthStore:number = 0;
  orderLength: number = 0;

  addToCart(id){
    if(localStorage.getItem("cartItems")){
      this.cartItems = JSON.parse(localStorage.getItem("cartItems"));
    }

    let itemRemoved = false;

    for(let x = 0; x < this.cartItems.length; x++){
      if(this.cartItems[x].Promo.buyersOfferID == id){
        this.cartItems.splice(x, 1);
        localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
        itemRemoved = true;
      }
    }

    if(!itemRemoved){
      for(let x = 0; x < this.promotions.length; x++){
        if(this.promotions[x].buyersOfferID == id){
          let newCartItem = new StoreOffer();
          newCartItem.storeOffersId = 0;
          newCartItem.Promo = this.promotions[x];
          newCartItem.amount = 30;
          this.cartItems.push(newCartItem);
          this.promotions[x].inCart = true;
          localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
        }
      }
    }

    this.cartLength = this.cartItems.length;
    if(this.cartLength > 0){
      this.activeStoreOffers = true;
    }else{
      this.activeStoreOffers = false;
    }
  }

  displayCart(value, showOrder = false){

    if(showOrder){
      this.newOrderView = false;
    }
    if(value == "cartView"){
      this.promoView = false;
      this.cartViewStore = false;
      this.orderView = false;
      this.cartView = true;
    }

    if(value == "cartViewStore"){
      this.promoView = false;
      this.cartView = false;
      this.orderView = false;
      this.cartViewStore = true;
    }

    if(value == "promoView"){
      this.cartView = false;
      this.cartViewStore = false;
      this.orderView = false;
      this.promoView = true;
    }

    if(value == "orderView"){
      this.cartView = false;
      this.cartViewStore = false;
      this.promoView = false;
      this.orderView = true;
    }

  }

  displayPromos(){
    if(this.cartView){
      this.cartView = false;
    }else{
      this.cartView = true;
    }
    this.setPromoCheckedFlags();
  }

  setPromoCheckedFlags(){
    if(this.cartItems.length <= 0){
      for(let y = 0; y < this.promotions.length; y++){
          this.promotions[y].inCart = false;
      }
    }
      for(let x = 0; x < this.cartItems.length; x++){
        for(let y = 0; y < this.promotions.length; y++){
          if(this.cartItems[x].Promo.buyersOfferID == this.promotions[y].buyersOfferID){
            this.promotions[y].inCart = true;
          }
        }
      }
  }

  removeFromCart(id){
    this.storeOfferService.DeleteStoreOfferById(id).then((data: number)=>{
      for(var i = this.cartItems.length -1; i >= 0; i--){
        if(this.cartItems[i].storeOffersId == id){
          this.cartItems.splice(i, 1);
        }
      }
      this.cartLength = this.cartItems.length;
      this.setPromoCheckedFlags();
    });
  }

  removeFromCartStore(value){
    for(let x = 0; x < this.cartItemsStore.length; x++){
      if(this.cartItemsStore[x].Promo.buyersOfferID == value){
        this.cartItemsStore.splice(x, 1);
        localStorage.setItem("cartItemsStore", JSON.stringify(this.cartItems));
      }
    }

    this.cartLengthStore = this.cartItemsStore.length;
    if(this.cartLengthStore > 0){
      this.activeOffers = true;
    }else{
      this.activeOffers = false;
    }
  }

  incrementCartAmount(id){
    for(let x = 0; x < this.cartItems.length; x++){
      if(this.cartItems[x].Promo.buyersOfferID == id){
        this.cartItems[x].amount += 1;
        localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
      }
    }
  }

  decrementCartAmount(id){
    for(let x = 0; x < this.cartItems.length; x++){
      if(this.cartItems[x].Promo.buyersOfferID == id){
        this.cartItems[x].amount -= 1;
        localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
      }
    }
  }

  insertStoreOffer(buyersOfferID){
    let id = 0;
   this.storeOffersList.forEach(element => {
     if(element.buyersOfferId == buyersOfferID){
        id = buyersOfferID;
     }
   });

  if(id > 0){

    this.storeOfferService.DeleteStoreOffer(buyersOfferID).then((data: number)=>{
      for(let x = 0; x < this.cartItems.length; x++){
        if(this.cartItems[x].storeOffersId == buyersOfferID){
          this.cartItems.splice(x,1);
        }
      }
      this.getStoreOffersByBand();
    });
  }else{
    this.storeOfferService.InsertStoreOffer(buyersOfferID, 1).then((data: number)=>{
      this.getStoreOffersByBand();
    });
  }
  }

  displayStoreRequest(value){
    this.storeRequestDisplay.stroreRequest.buyersOfferID = value;
    this.storeRequestDisplay.stroreRequest.quantity = 1;
    for(let x = 0; x < this.promotions.length; x++){
      if(this.promotions[x].buyersOfferID == value){
        this.storeRequestDisplay.buyerOffer = this.promotions[x];
      }
    }
    this.displayRequestOption = true;
  }

  setReqAmount(value){
    if(value <= 0){
      alert("Amount must be greater than 0");
      return;
    }
    this.currentStoreRequetsTemplate.amount = value;
  }

  setReqSize(value){
    this.getTemplatesForRequests(value);
  }

  setReqTemplate(value){
    if(value == 0){
      return;
    }
    for(let x = 0; x < this.storeRequestTemplateList.length; x++){
      if(this.storeRequestTemplateList[x].id == value){
        this.currentStoreRequetsTemplate.templateId = this.storeRequestTemplateList[x].id;
        this.currentStoreRequetsTemplate.templateName = this.storeRequestTemplateList[x].templateName;
        //this.storeRequestDisplay.template.push(newTemp);
      }
    }
  }

  addRequestTemplateToRequest(){
    let exists = false;

    if(this.currentStoreRequetsTemplate.templateId == 0 || this.currentStoreRequetsTemplate.amount == 0){
      alert("Please select a template and set an amount");
      return;
    }
    for(let x = 0; x < this.storeRequestDisplay.template.length; x++){
      if(this.storeRequestDisplay.template[x].templateId == this.currentStoreRequetsTemplate.templateId){
        exists = true;
      }
    }

    if(exists){
      alert("The template exists")
      return;
    }

    this.storeRequestDisplay.template.push(this.currentStoreRequetsTemplate);

    this.currentStoreRequetsTemplate = new StoreRequestTemplates()
  }

  closeStoreDisplay(){
    this.displayRequestOption = false;
    this.storeRequestDisplay = new StoreRequestDisplay();
    this.storeRequestDisplay.buyerOffer = new Promo();
    this.storeRequestDisplay.stroreRequest = new StoreRequest();
  }

  insertStoreRequest(){
    if(this.storeRequestDisplay.template.length < 1){
      alert("Please select a template");
      return;
    }
    this.currentStoreRequest = this.storeRequestDisplay.stroreRequest;
    this.currentStoreRequest.statusId = 1006;
    this.currentStoreRequest.buyersOfferID = this.storeRequestDisplay.buyerOffer.buyersOfferID;
    this.currentStoreRequest.storeRequestTemplates = this.storeRequestDisplay.template;
    this.currentStoreRequest.userId = this.accessService.getUser().userid;
    this.currentStoreRequest.storeId = this.accessService.getUser().storeId;

    this.storeRequestService.InsertStoreRequest(this.currentStoreRequest).then((data: any)=>{
      this.displayRequestOption = false;
      this.storeRequestDisplay = new StoreRequestDisplay();
      this.storeRequestDisplay.buyerOffer = new Promo();
      this.storeRequestDisplay.stroreRequest = new StoreRequest();
      this.getActiveStoreRequestsByStore(this.accessService.getUser().storeId);

		}).catch((error: any[])=>{
			console.log(error);
		});

  }

  confirmStoreRequestByUser(){
    this.storeRequestService.UpdateStoreRequestByUser(this.accessService.getUser().storeId).then((data: any)=>{
     this.getActiveStoreRequestsByStore(this.accessService.getUser().storeId);
		}).catch((error: any[])=>{
			console.log(error);
		});
  }

  confirmStoreRequestAdmin(){

    this.storeRequestService.ConfirmStoreRequest(this.currentcurrentRequestOrderId).then((data: any)=>{
      this.currentcurrentRequestOrderId = 0;
      this.getActiveStoreRequestsByBand(this.bandLetter);
      this.getOrdersAfterInsert();
		}).catch((error: any[])=>{
			console.log(error);
		});
  }

  confirmStoreRequest(value, i, j, k){
    let am = this.storeRequestActionDisplayList[i].storeRequestActionDisplayListProduct[j].storeRequestAction[k].amount;
    let id = this.storeRequestActionDisplayList[i].storeRequestActionDisplayListProduct[j].storeRequestAction[k].templateMapId;
    // alert(am)
    // return;

    this.storeRequestService.UpdateMapping(id, am).then((data: any)=>{
      alert();
		}).catch((error: any[])=>{
			console.log(error);
		});
  }

  updateStoreRequestAmount(value, i, j, k){

    let am = this.storeRequestActionDisplayList[i].storeRequestActionDisplayListProduct[j].storeRequestAction[k].amount;
    let id = this.storeRequestActionDisplayList[i].storeRequestActionDisplayListProduct[j].storeRequestAction[k].templateMapId;

    this.storeRequestService.UpdateMapping(id, am).then((data: any)=>{
      alert("Pos Amount Updated");
		}).catch((error: any[])=>{
			console.log(error);
		});
  }


  getActiveStoreRequestsByBand(band: string){
    this.storeRequestService.GetRequestOrdersBandId(band).then((data: RequestOrder[])=>{
      this.storeRequestOrderList = data;
      this.cartLengthStore = this.storeRequestOrderList.length;
      if(this.cartLengthStore > 0){
        this.activeOffers = true;
      }else{
        this.activeOffers = false;
      }
    });
  }

  getStoreRequestsByBand(band:string){
    this.storeRequestActionDisplayList = [];
    this.storeRequestActionDisplayListProduct = []
    this.storeRequestService.GetByBandId(band).then((data: any)=>{
      this.storeRequestActionList = data;
      this.cartLengthStore = this.storeRequestActionList.length;
      if(this.cartLengthStore > 0){
        this.activeOffers = true;
      }else{
        this.activeOffers = false;
      }
      let groups = this.storeRequestActionList.reduce((groups, item) => {
        const group = (groups[item.storeName] || []);
        group.push(item);
        groups[item.storeName] = group;
        return groups;
      }, {});

      for (const key in groups) {
        let orderDtoDisplay: StoreRequestActionDisplay = new StoreRequestActionDisplay();
        if (groups.hasOwnProperty(key)) {
          orderDtoDisplay.store = key;
          orderDtoDisplay.storeRequestAction = groups[key];
          this.storeRequestActionDisplayList.push(orderDtoDisplay);
        }
      }

      for(let x = 0; x < this.storeRequestActionDisplayList.length; x++){
        this.storeRequestActionDisplayListProduct = [];
        let groupsProduct = this.storeRequestActionDisplayList[x].storeRequestAction.reduce((groupsProduct, item) => {
          const group = (groupsProduct[item.promProdCode] || []);
          group.push(item);
          groupsProduct[item.promProdCode] = group;
          return groupsProduct;
        }, {});

        for (const key in groupsProduct) {
          let orderDtoDisplay: StoreRequestActionDisplayProduct = new StoreRequestActionDisplayProduct();
          if (groupsProduct.hasOwnProperty(key)) {
            orderDtoDisplay.product = key;
            orderDtoDisplay.storeRequestAction = groupsProduct[key];
            this.storeRequestActionDisplayListProduct.push(orderDtoDisplay);
          }
        }

        this.storeRequestActionDisplayList[x].storeRequestActionDisplayListProduct = this.storeRequestActionDisplayListProduct;

      }

      console.log(JSON.stringify(this.storeRequestActionDisplayList));
     // console.log(JSON.stringify(this.storeRequestActionDisplayListProduct));


		}).catch((error: any[])=>{
			console.log(error);
		});
  }

  getStoreRequestsByOrderId(orderId:number){
    this.currentcurrentRequestOrderId = orderId;

    this.storeRequestActionDisplayList = [];
    this.storeRequestActionDisplayListProduct = []
    this.storeRequestService.GetByOrderId(orderId).then((data: any)=>{
      this.storeRequestActionList = data;

      let groups = this.storeRequestActionList.reduce((groups, item) => {
        const group = (groups[item.storeName] || []);
        group.push(item);
        groups[item.storeName] = group;
        return groups;
      }, {});

      for (const key in groups) {
        let orderDtoDisplay: StoreRequestActionDisplay = new StoreRequestActionDisplay();
        if (groups.hasOwnProperty(key)) {
          orderDtoDisplay.store = key;
          orderDtoDisplay.storeRequestAction = groups[key];
          this.storeRequestActionDisplayList.push(orderDtoDisplay);
        }
      }

      for(let x = 0; x < this.storeRequestActionDisplayList.length; x++){
        this.storeRequestActionDisplayListProduct = [];
        let groupsProduct = this.storeRequestActionDisplayList[x].storeRequestAction.reduce((groupsProduct, item) => {
          const group = (groupsProduct[item.promProdCode] || []);
          group.push(item);
          groupsProduct[item.promProdCode] = group;
          return groupsProduct;
        }, {});

        for (const key in groupsProduct) {
          let orderDtoDisplay: StoreRequestActionDisplayProduct = new StoreRequestActionDisplayProduct();
          if (groupsProduct.hasOwnProperty(key)) {
            orderDtoDisplay.product = key;
            orderDtoDisplay.storeRequestAction = groupsProduct[key];
            this.storeRequestActionDisplayListProduct.push(orderDtoDisplay);
          }
        }

        this.storeRequestActionDisplayList[x].storeRequestActionDisplayListProduct = this.storeRequestActionDisplayListProduct;

      }

      console.log(JSON.stringify(this.storeRequestActionDisplayList));
     // console.log(JSON.stringify(this.storeRequestActionDisplayListProduct));


		}).catch((error: any[])=>{
			console.log(error);
		});
  }

  getActiveStoreRequestsByStore(bandId:number){
    this.storeRequestActionDisplayList = [];
    this.storeRequestActionDisplayListProduct = []
    this.storeRequestService.GetActiveRequestForStore(this.accessService.getUser().storeId).then((data: any)=>{
      this.storeRequestActionList = data;
      this.cartLengthStore = this.storeRequestActionList.length;
      if(this.cartLengthStore > 0){
        this.activeOffers = true;
      }else{
        this.activeOffers = false;
      }
      let groups = this.storeRequestActionList.reduce((groups, item) => {
        const group = (groups[item.storeName] || []);
        group.push(item);
        groups[item.storeName] = group;
        return groups;
      }, {});

      for (const key in groups) {
        let orderDtoDisplay: StoreRequestActionDisplay = new StoreRequestActionDisplay();
        if (groups.hasOwnProperty(key)) {
          orderDtoDisplay.store = key;
          orderDtoDisplay.storeRequestAction = groups[key];
          this.storeRequestActionDisplayList.push(orderDtoDisplay);
        }
      }

      for(let x = 0; x < this.storeRequestActionDisplayList.length; x++){
        this.storeRequestActionDisplayListProduct = [];
        let groupsProduct = this.storeRequestActionDisplayList[x].storeRequestAction.reduce((groupsProduct, item) => {
          const group = (groupsProduct[item.promProdCode] || []);
          group.push(item);
          groupsProduct[item.promProdCode] = group;
          return groupsProduct;
        }, {});

        for (const key in groupsProduct) {
          let orderDtoDisplay: StoreRequestActionDisplayProduct = new StoreRequestActionDisplayProduct();
          if (groupsProduct.hasOwnProperty(key)) {
            orderDtoDisplay.product = key;
            orderDtoDisplay.storeRequestAction = groupsProduct[key];
            this.storeRequestActionDisplayListProduct.push(orderDtoDisplay);
          }
        }

        this.storeRequestActionDisplayList[x].storeRequestActionDisplayListProduct = this.storeRequestActionDisplayListProduct;

      }

      console.log(JSON.stringify(this.storeRequestActionDisplayList));
     // console.log(JSON.stringify(this.storeRequestActionDisplayListProduct));


		}).catch((error: any[])=>{
			console.log(error);
		});
  }

  getTemplatesForRequests(sizeid:number){
    this.templateService.getByBandAndSize(this.storeRequestDisplay.buyerOffer.prBand, sizeid).then((data: StoreRequestTemplates[])=>{
      if(data){
       this.storeRequestTemplateList = data;

      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
  }

  getStoreOffers(){
    this.storeOfferService.GetAll().then((data: StoreOfferDb[])=>{
      this.cartLength = 0;
      if(data){
        this.cartItems = [];
        this.storeOffersList = data;
        this.storeOffersList.forEach(storeOffersDb => {
          this.promotions.forEach(buyersOffer => {
            if(buyersOffer.buyersOfferID == storeOffersDb.buyersOfferId){
              let cartItem = new StoreOffer();
              cartItem.Promo = buyersOffer;
              cartItem.storeOffersId = storeOffersDb.storeOffersId;
              cartItem.amount = storeOffersDb.amount;
              cartItem.Staus = new StoreOfferStatus();
              this.cartItems.push(cartItem);
              this.cartLength = this.cartItems.length;
              if(this.cartLength > 0){
                this.activeStoreOffers = true;
              }else{
                this.activeStoreOffers = false;
              }
            }
          });
        });

        this.setStoreOfferStatus()
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;

  }

  getStoreOffersByBand(){
    this.storeOfferService.GetByBandId(this.selectedValue.id).then((data: StoreOfferDb[])=>{
      this.cartLength = 0;
      if(data){
        this.cartItems = [];
        this.storeOffersList = data;
        this.storeOffersList.forEach(storeOffersDb => {
          this.promotions.forEach(buyersOffer => {
            if(buyersOffer.buyersOfferID == storeOffersDb.buyersOfferId){

              let cartItem = new StoreOffer();
              cartItem.Promo = buyersOffer;
              cartItem.storeOffersId = storeOffersDb.storeOffersId;
              cartItem.amount = storeOffersDb.amount;
              cartItem.storeId = storeOffersDb.storeID;
              cartItem.storeName = storeOffersDb.storeName

              cartItem.Staus = new StoreOfferStatus();
              this.cartItems.push(cartItem);
              this.cartLength = this.cartItems.length;
            }
          });
        });
        if(this.cartLength > 0){
          this.activeStoreOffers = true;
        }else{
          this.activeStoreOffers = false;
        }
        this.setPromoCheckedFlags();
        // this.setStoreOfferStatus()
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;

  }

  getStoreOfferStatus(){
    this.storeOfferService.GetAllStoreOfferStatus().then((data: StoreOfferStatus[])=>{
      if(data){
        this.storeOfferStatusList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   setStoreOfferStatus(){
    let storeOfferStatus: StoreOfferStatus = new StoreOfferStatus();
    this.cartLengthStore = 0;
    if(this.storeOfferStatusList.length <= 0){
      this.storeOfferService.GetAllStoreOfferStatus().then((data: StoreOfferStatus[])=>{
        if(data){
          this.storeOfferStatusList = data;
          for(let x= 0; x < this.storeOffersList.length; x++){

            let statusToFind = this.storeOffersList[x].statusId;

            let staus: StoreOfferStatus;

            for(let y= 0; y < this.storeOfferStatusList.length; y++){
              if(this.storeOfferStatusList[y].id == statusToFind){
                staus = this.storeOfferStatusList[y];
              }
            }

            for(let z = 0; z < this.cartItems.length; z++){
              if(this.cartItems[z].storeOffersId == this.storeOffersList[x].storeOffersId){
                this.cartItems[z].Staus = staus;
                if(staus.name == "CartStore"){
                  this.cartItemsStore.push(this.cartItems[z])
                  this.cartItems.splice(z, 1);
                  this.cartLength = this.cartItems.length;
                  this.cartLengthStore = this.cartItemsStore.length;
                  if(this.cartLengthStore > 0){
                    this.activeOffers = true;
                  }else{
                    this.activeOffers = false;
                  }
                }
              }
            }
          }
        }
        }).catch((error: any[])=>{
          console.log(error);
        });;
    }else{
      // for(let x= 0; x < this.storeOfferStatusList.length; x++){
      //   if(storeOfferDb.statusId == this.storeOfferStatusList[x].id){
      //     storeOfferStatus = this.storeOfferStatusList[x];
      //     }
      //   }
    }
     return storeOfferStatus;
   }

  upsertStoreOffers(){

    let storeOfferDbList : StoreOfferDb[] = [];

    this.cartItems.forEach(element => {
      let storeOffer = new StoreOfferDb();
      storeOffer.storeOffersId = element.storeOffersId;
      storeOffer.buyersOfferId = element.Promo.buyersOfferID;
      storeOffer.storeID = this.GetStoreId(element);
      storeOffer.templateId = 1;
      storeOffer.amount = element.amount;
      storeOfferDbList.push(storeOffer);
    });

    this.storeOfferService.Upsert(storeOfferDbList).then((data: StoreOfferDb[])=>{
      if(data){
        this.storeOffersList = data;
       // alert(JSON.stringify(this.storeOffersList))
      };
      }).catch((error: any[])=>{
        console.log(error);
      });;
  }

  acceptStoreOffer(index: number){
    this.cartItems.push(this.cartItemsStore[index]);
    this.cartItemsStore.splice(index, 1);

    this.cartLength = this.cartItems.length;
    this.cartLengthStore = this.cartItemsStore.length;

    if(this.cartLengthStore > 0){
      this.activeOffers = true;
    }else{
      this.activeOffers = false;
    }
  }

  GetStoreId(storeOffer: StoreOffer){
      let searchStoreName = storeOffer.Promo.promIsland;
      let searchBandName = storeOffer.Promo.prBand;
      let storeId = 0;
      this.storeListAll.forEach(store => {


        let storeId2 = store.storeID;

        if(store.storeName == searchStoreName){

          for(let x = 0; x <this.bands.length; x++){

            if(this.bands[x].id == storeId2){
              //alert(storeId2)
              alert(store.storeID)
              storeId = storeId2;
            }
          }

        }
      });
      alert(storeId);
      return storeId;
  }

  getOfferTypeByband(id: number){
    this.storeOfferService.GetOfferTypesByBand(id).then((data: OfferType[])=>{
      if(data){
        this.offerTypeList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getOfferDisplayTypes(id){
    this.storeOfferService.GetOfferDisplayTypes(id).then((data: OfferDisplayType[])=>{
      if(data){
        this.selectedOfferTypeId = id;
        for(let x=0; x < this.cartItems.length; x++){
          this.cartItems[x].offerDisplayType = data;
        }
        this.getOfferDisplayTypesOverride(id);
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getOfferDisplayTypesOverride(id){
    this.storeOfferService.GetOfferDisplayTypesOverride(id).then((data: OfferTypeDisplayTypeOverride[])=>{
      console.log(JSON.stringify(data))
      console.log(JSON.stringify(this.cartItems))

      if(data){
        for(let z = 0; z < data.length; z++){
          let store = data[z].storeId;
          let oId = data[z].id;
          let am = data[z].finalAmount;

          for(let x=0; x < this.cartItems.length; x++){
            if(this.cartItems[x].storeId == store){
              // alert(this.cartItems[x].storeId);
              // alert(store);
              for(let y=0; y < this.cartItems[x].offerDisplayType.length; y++){
                if(this.cartItems[x].offerDisplayType[y].id == oId && this.cartItems[x].storeId == store){
                  //this.cartItems[x].offerDisplayType[y].amount = am;
                }
              }
            }else{
            }
          }
        }

        // for(let x=0; x < this.cartItems.length; x++){
        //   let storeId = this.cartItems[x].storeId;
        //   for(let y=0; y < this.cartItems[x].offerDisplayType.length; y++){
        //    let offerId = this.cartItems[x].offerDisplayType[y].id;
        //     for(let z = 0; z < data.length; z++){
        //       if(data[z].id == offerId && data[z].storeId == storeId){
        //         this.cartItems[x].offerDisplayType[y].amount = data[z].finalAmount;
        //       }
        //     }
        //   }
        //   console.log(JSON.stringify(this.cartItems));

          //alert(storeId);
          // for(let z = 0; z < data.length; z++){
          //   let famount = 0;
          //   let id = data[z].id;
          //   if(data[z].storeId == storeId){
          //     famount = data[z].finalAmount;
          //   }
          //   if(famount > 0){
          //     for(let y=0; y < this.cartItems[x].offerDisplayType.length; y++){
          //       if(this.cartItems[x].offerDisplayType[y].id == id && data[z].storeId == this.cartItems[x].storeId){
          //         this.cartItems[x].offerDisplayType[y].amount = famount
          //       }else{
          //         this.cartItems[x].offerDisplayType[y].amount = this.cartItems[x].offerDisplayType[y].amount;
          //       }
          //     }
          //   }
          // }
          //console.log(this.cartItems[x].offerDisplayType);
        //}


        // for(let z = 0; z < data.length; z++){
        //   for(let x=0; x < this.cartItems.length; x++){
        //       for(let y=0; y < this.cartItems[x].offerDisplayType.length; y++){
        //         if(data[z].storeId == this.cartItems[x].storeId){
        //           if(data[z].id == this.cartItems[x].offerDisplayType[y].id){
        //             this.cartItems[x].offerDisplayType[y].amount = data[z].finalAmount;
        //           }
        //         }
        //       }
        //   }
        // }
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   onSelectOfferType(id){
      this.getOfferDisplayTypes(id);
   }

   //Orders

   getOrders(){
    this.storeOfferService.GetOrders().then((data: Order[])=>{
      if(data){
        this.orderList = data;
        this.orderDtoListDisplay = [];
        if(this.orderList.length > 0){
          this.activeOrders = true;
          this.orderLength = this.orderList.length;
        }

      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getPrinters(){
    this.storeOfferService.GetPrinters().then((data: PrinterDto[])=>{
      if(data){
        this.printerList = data;
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getOrdersCompleted(){
    this.storeOfferService.GetOrdersCompleted().then((data: Order[])=>{
      if(data){
        this.orderList = data;
        if(this.orderList.length > 0){
          // this.activeOrders = true;
          //   this.orderLength = this.orderList.length;
            this.orderDtoListDisplay = [];
        }

      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }


   getOrdersAfterInsert(){
    this.storeOfferService.GetOrders().then((data: Order[])=>{
      if(data){
        this.orderList = data;
        var last = this.orderList[this.orderList.length - 1];

        this.getOrdersById(last.orderId);
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   getOrdersById(id:number){
    this.currentOrderId = id;
    this.storeOfferService.GetOrdersById(id).then((data: OrderDto[])=>{
      if(data){
        // this.orderDtoList = [];
        this.orderDtoListDisplay = [];

        this.orderDtoList = data;

        let groups = this.orderDtoList.reduce((groups, item) => {
          const group = (groups[item.storeName] || []);
          group.push(item);
          groups[item.storeName] = group;
          return groups;
        }, {});

        for (const key in groups) {
          let orderDtoDisplay: OrderDtoDisplay = new OrderDtoDisplay();
          if (groups.hasOwnProperty(key)) {
            orderDtoDisplay.Store = key;
            orderDtoDisplay.oderDto = groups[key];
            this.orderDtoListDisplay.push(orderDtoDisplay);
          }

          this.displayCart('orderView');
          this.cartItems = [];
          this.setPromoCheckedFlags();
        }
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   processOrder(){
     if(this.currentPrinterId == 0){
      alert("Please select a printe house");
      return;
     }

    this.templateService.createTemplatePdf(this.currentOrderId, this.currentPrinterId).then((data: any)=>{
      if(data){
        alert("Order Complete, please check your email for confirmation.");
        this.cartView = false;
        this.cartViewStore = false;
        this.promoView = true;
        this.orderView = false;
        this.currentPrinterId = 0;
        this.orderDtoListDisplay = [];
        this.getOrders();
        this.getStoreOffersByBand();
        this.setPromoCheckedFlags();
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   previewTemplate(value, id){
    this.templateService.createTemplatePreview(value, id).then((data: any)=>{
      this.openPreviewWindow();
     });
   }

   openPreviewWindow() {
    var url = "https://localhost:5001/StaticFiles/Resources/Images/preview.pdf";
    var target = "blank"
 if(localStorage.getItem(target) == "true")
  {
    window.open(url,target);
  }
  else
    {
      localStorage.setItem(target,"true");
      window.open(url,target);
    }
  }

   insertOrder(){
    this.storeOfferService.InsertOrder(this.selectedValue.id, this.selectedOfferTypeId, 1).then((data: number)=>{
      if(data){
        this.newOrderView = true;
        this.cartItems = [];
        this.cartLength = 0;
        this.getOrdersAfterInsert();

        //alert("New Order Created");
      }
      }).catch((error: any[])=>{
        console.log(error);
      });;
   }

   onSelectOrderTypeView(entry): void {
      this.orderByCurrentOrPast = entry;

      if(!entry){
        this.getOrdersCompleted();
      }else{
        this.getOrders();
      }
    }

    setCurrentPrinter(value){
      this.currentPrinterId = value;
    }

    ngAfterViewInit(): void {
      this.dtTrigger.next();
    }

    ngOnDestroy(): void {
      // Do not forget to unsubscribe the event
      this.dtTrigger.unsubscribe();
    }

    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        this.dtTrigger.next();
      });
    }

    getFields(){
      this.templatePropertiesService.getFields().then((data: Field[])=>{
        if(data){
          this.FieldList = data;
        }
        }).catch((error: any[])=>{
          console.log(error);
        });
     }
}

