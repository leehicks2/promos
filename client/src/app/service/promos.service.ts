import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Promo } from '../shared/models/PromoModel';

@Injectable({
  providedIn: 'root'
})
export class PromosService {
  private SERVER_URL = "https://localhost:5001/buyerOffers";

  constructor(private httpClient: HttpClient) { }

  public get(){
		return this.httpClient.get(this.SERVER_URL).toPromise();
	}

  public getByBand(bandLetter:string){
		return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandLetter).toPromise();
	}

  public getByBandFilter(bandLetter:string, filter:string){
		return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandLetter + + "/filter/" + filter).toPromise();
	}


  public getByBandWithOrder(bandLetter:string, orderBy: string){
		return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandLetter + "/orderby/" + orderBy).toPromise();
	}

  public getByBandWithOrderFilter(bandLetter:string, orderBy: string, filterBy: string){
		return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandLetter + "/orderby/" + orderBy + "/filter/" + filterBy).toPromise();
	}

  public getByBandAndIslandWithOrder(bandLetter:string, island:string, orderBy: string){
		return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandLetter + "/island/" + island + "/orderby/" + orderBy).toPromise();
	}

  public getByBandAndIslandWithOrderFilter(bandLetter:string, island:string, orderBy: string, filter: string){
		return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandLetter + "/island/" + island + "/orderby/" + orderBy + "/filter/" + filter).toPromise();
	}

  public getByBandAndIsland(bandLetter:string, islandName: string){
		return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandLetter + "/island/" + islandName).toPromise();
	}

  public getByUser(userId){
		return this.httpClient.get(this.SERVER_URL + "/userid/" + userId).toPromise();
	}

  public getByBandAndStore(bandLetter:string, storeId:string){
		return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandLetter + "/storeid/" + storeId).toPromise();
	}

  public put(promos: Promo[]){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    const httpOptions = {
      headers: headers
    }

    return this.httpClient.post(this.SERVER_URL + "/update",JSON.stringify(promos), {headers:headers}).toPromise();
    //return this.httpClient.get(this.SERVER_URL).toPromise();
  }

  public insertMultipleOrders(promos: number[]){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    const httpOptions = {
      headers: headers
    }

    return this.httpClient.post(this.SERVER_URL + "/Insertmultiplestoreorders",JSON.stringify(promos), {headers:headers}).toPromise();
    //return this.httpClient.get(this.SERVER_URL).toPromise();
  }

  public deleteMultipleOrders(promos: number[]){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    const httpOptions = {
      headers: headers
    }

    return this.httpClient.post(this.SERVER_URL + "/deletemultiplestoreorders",JSON.stringify(promos), {headers:headers}).toPromise();
    //return this.httpClient.get(this.SERVER_URL).toPromise();
  }

}



