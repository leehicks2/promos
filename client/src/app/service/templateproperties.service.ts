import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TemplateProperties } from '../shared/models/ElementProperties';

@Injectable({
  providedIn: 'root'
})
export class TemplatepropertiesService {
  private SERVER_URL = "https://localhost:5001/templateproperties";

  constructor(private httpClient: HttpClient) { }

  insertTemplateProperties(template: TemplateProperties){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL,JSON.stringify(template), {headers:headers}).toPromise();
  }

  updateTemplateProperties(template: TemplateProperties){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    console.log(JSON.stringify(template));
    return this.httpClient.post(this.SERVER_URL + "/update",JSON.stringify(template), {headers:headers}).toPromise();
  }

  public getByTemplateId(id:number){
    const url = this.SERVER_URL + "/template/" + id;
		return this.httpClient.get(url).toPromise();
	}

  public getById(id:number){
    const url = this.SERVER_URL + "/" + id;
		return this.httpClient.get(url).toPromise();
	}

  public getFields(){
    const url = this.SERVER_URL + "/" + "field";
		return this.httpClient.get(url).toPromise();
	}

  public delete(id: number){
    const url = this.SERVER_URL + "/" + "templateproperty/delete/" + id;
		return this.httpClient.get(url).toPromise();
	}

  public getFontsByBand(id: number){
    const url = this.SERVER_URL + "/fonts/band/" + id;
		return this.httpClient.get(url).toPromise();
	}

}
