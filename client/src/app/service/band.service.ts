import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BandService {
  private SERVER_URL = "https://localhost:5001/band";

  constructor(private httpClient: HttpClient) { }

  GetBands(){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL, {headers:headers}).toPromise();
  }
}
