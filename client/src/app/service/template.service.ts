import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PosTemplate } from '../shared/models/PosTemplate';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {
  private SERVER_URL = "https://localhost:5001/template";

  constructor(private httpClient:HttpClient) { }

  insertTemplate(template: PosTemplate){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL,JSON.stringify(template), {headers:headers}).toPromise();
  }

  updateTemplate(template: PosTemplate){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL + "/update",JSON.stringify(template), {headers:headers}).toPromise();
  }

  public get(){
		return this.httpClient.get(this.SERVER_URL).toPromise();
	}

  public getById(id:number){
    const url = this.SERVER_URL + "/" + id;
		return this.httpClient.get(url).toPromise();
	}

  public getByBandAndSize(bandLetter:string, sizeId: number){
    const url = this.SERVER_URL + "/bandid/" + bandLetter + "/sizeid/" + sizeId;
		return this.httpClient.get(url).toPromise();
	}

  public getByBandId(id:number){
    const url = this.SERVER_URL + "/band/" + id;
		return this.httpClient.get(url).toPromise();
	}

  public getByTemplatePacks(){
    const url = this.SERVER_URL + "/packs/";
		return this.httpClient.get(url).toPromise();
	}

  public getTemplateDisplays(buyeroffer:number, template:number){

    const url = this.SERVER_URL + "/display/buyeroffer/" + buyeroffer + "/template/" + template;
		return this.httpClient.get(url).toPromise();
	}

  public createTemplatePdf(orderId: number, printerId: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    const url = "https://localhost:5001" + "/TemplateCreater/" + orderId + "/printer/" + printerId;
		return this.httpClient.get(url).toPromise();
	}

  public createTemplatePreview(buyerOffer: number, template: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    const url = "https://localhost:5001" + "/TemplateCreater/preview/buyeroffer/" + buyerOffer + "/template/" + template;
		return this.httpClient.get(url).toPromise();
	}


}
