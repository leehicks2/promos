import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {UserServiceService} from '../service/user-service.service';
import { LoginModel } from '../shared/models/LoginModel';
import { User } from '../shared/models/User';
import { AuthResponseModel } from '../shared/models/AuthResponseModel';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private SERVER_URL = "https://localhost:5001/login";

  constructor(private httpClient: HttpClient) { }

  login(loginModel: LoginModel){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL,JSON.stringify(loginModel), {headers:headers}).toPromise();
    //return this.httpClient.get(this.SERVER_URL).toPromise();
  }

}
