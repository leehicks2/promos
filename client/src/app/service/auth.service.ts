import { ANALYZE_FOR_ENTRY_COMPONENTS, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  isAuthenticated(){
    let authed: string = localStorage.getItem("authed");
    if(authed && authed == "true"){
      return true
    }
    return false;
  }
}
