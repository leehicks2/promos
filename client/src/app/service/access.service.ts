import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../shared/models/User';

@Injectable({
  providedIn: 'root'
})
export class AccessService {

  constructor(private router: Router) { }

  public isAdmin(): boolean{
    let isAdmin = false;
    let user = this.getUser();
    if(user){
      if(user.accessLevelID != "0"){
        if(user.accessLevelID == "2"){
          isAdmin = true;
        }else{
          isAdmin = false;
        }
      }
    }else{
      this.router.navigate(['/login']);
    }

    return isAdmin;
  }

  public getUser(): User{
    let user = localStorage.getItem("User");

    if(user){
      return JSON.parse(user);
    }else{
      this.router.navigate(['/login']);
    }
  }
}
