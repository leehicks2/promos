import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { StoreRequest } from '../shared/models/StoreRequest';
import { StoreRequestAction } from '../shared/models/StoreRequestAction';

@Injectable({
  providedIn: 'root'
})
export class StorerequestsService {
  private SERVER_URL = "https://localhost:5001/storerequests";

  constructor(private httpClient: HttpClient) { }

  GetAll(){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL, {headers:headers}).toPromise();
  }

  GetByBandId(bandId: string){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL +"/band/" + bandId, {headers:headers}).toPromise();
  }

  GetByOrderId(orderId: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL +"/order/" + orderId, {headers:headers}).toPromise();
  }

  GetRequestOrdersBandId(band: string){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL +"/activerequestorders/" + band, {headers:headers}).toPromise();
  }

  GetByStoreId(bandId: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL + "/storeid" + bandId, {headers:headers}).toPromise();
  }

  GetActiveRequestForStore(storeId: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL + "/storepending/store/" + storeId, {headers:headers}).toPromise();
  }

  UpdateStoreRequestByUser(storeId: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL + "/confirmstorerequest/" + storeId, {headers:headers}).toPromise();
  }

  UpdateMapping(id: number, amount: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL + "/updatemap/mapId/" + id + "/amount/" + amount, {headers:headers}).toPromise();
  }

  InsertStoreRequest(storeRequest: StoreRequest){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.post(this.SERVER_URL,JSON.stringify(storeRequest), {headers:headers}).toPromise();
  }

  ConfirmStoreRequest(orderId: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL + "/confirmrequestorder/" + orderId, {headers:headers}).toPromise();
  }

  DeleteStoreRequest(bandId: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL + "/storeid" + bandId, {headers:headers}).toPromise();
  }

  GetTutorials(){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL +"/tutorials", {headers:headers}).toPromise();
  }

  GetTutorialTypes(parent){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.get(this.SERVER_URL +"/tutorialtypes/" + parent, {headers:headers}).toPromise();
  }



}


