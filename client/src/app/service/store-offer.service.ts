import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as internal from 'assert';
import { OfferDisplayType } from '../shared/models/OfferDisplayType';
import { OfferDisplayTypeDB } from '../shared/models/OfferDisplayTypeDb';
import { OfferType } from '../shared/models/OfferType';
import { StoreOffer } from '../shared/models/StoreOffer';
import { StoreOfferDb } from '../shared/models/StoreOfferDb';
import { StoreOfferTypeDisplayTypes } from '../shared/models/StoreOfferTypeDisplayTypes';

@Injectable({
  providedIn: 'root'
})
export class StoreOfferService {

  private SERVER_URL = "https://localhost:5001/storeoffer";

  constructor(private httpClient: HttpClient) { }

  GetAll(){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL, {headers:headers}).toPromise();
  }

  GetByBandId(bandId: number){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/bandid/" + bandId, {headers:headers}).toPromise();
  }


  InsertStoreOffer(buyerOffreId: number, userId){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    return this.httpClient.post(this.SERVER_URL + "/buyeroffer/" + buyerOffreId +"/user/" + userId,JSON.stringify({}), {headers:headers}).toPromise();
  }

  DeleteStoreOffer(buyerOffreId: number){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/buyerofferid/delete/" + buyerOffreId, {headers:headers}).toPromise();
  }

  DeleteStoreOfferById(id: number){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/delete/offerid/" + id, {headers:headers}).toPromise();
  }

  Upsert(storeOffers: StoreOfferDb[]){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL,JSON.stringify(storeOffers), {headers:headers}).toPromise();
    //return this.httpClient.get(this.SERVER_URL).toPromise();
  }

  GetAllStoreOfferStatus(){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/status", {headers:headers}).toPromise();
  }

  GetPromoDisplayType(){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/promodisplaytype", {headers:headers}).toPromise();
  }

  GetOfferTypes(){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/offertype", {headers:headers}).toPromise();
  }

  GetOfferTypesByBand(id:number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/offertype/band/" + id, {headers:headers}).toPromise();
  }

  InsertOfferType(storeOffers: OfferType){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL + "/offertype",JSON.stringify(storeOffers), {headers:headers}).toPromise();
  }

  GetOfferDisplayTypes(id: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/offerdisplayType/offerid/" + id, {headers:headers}).toPromise();
  }

  GetOfferDisplayTypesOverride(id: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/offerdisplayTypeoverride/offerid/" + id, {headers:headers}).toPromise();
  }

  InsertOfferDisplayType(storeOffers: OfferDisplayTypeDB){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL + "/offerdisplayType",JSON.stringify(storeOffers), {headers:headers}).toPromise();
  }

  UpdateOfferDisplayType(storeOffers: OfferDisplayType){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL + "/offerdisplayType/update",JSON.stringify(storeOffers), {headers:headers}).toPromise();
  }

  deleteOfferDisplayType(storeOffers: OfferDisplayType){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL + "/offerdisplayType/delete",JSON.stringify(storeOffers), {headers:headers}).toPromise();
  }


  GetOfferTypeDisplayTypes(offerId: number, storeId){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/storeOfferDisplaytypes/offerid/" + offerId + "/storeid/" + storeId, {headers:headers}).toPromise();
  }

  GetOfferTypeDisplayTypesStores(offerId: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/storeOfferDisplaytypesstores/offerid/" + offerId, {headers:headers}).toPromise();
  }

  InsertStoreOfferTypeDisplayTypes(storeOffers: StoreOfferTypeDisplayTypes[]){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL + "/storeOfferDisplaytypesstores",JSON.stringify(storeOffers), {headers:headers}).toPromise();
  }

  UpdateStoreOfferTypeDisplayTypes(storeOffers: StoreOfferTypeDisplayTypes){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL + "/storeOfferDisplaytypesstores/update",JSON.stringify(storeOffers), {headers:headers}).toPromise();
  }

  GetOrders(){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/order", {headers:headers}).toPromise();
  }

  GetOrdersCompleted(){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/order/completed", {headers:headers}).toPromise();
  }

  GetOrdersById(id: number){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/order/orderid/" + id, {headers:headers}).toPromise();
  }

  //Orders
  InsertOrder(bandId, offerTpeId, userId){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.post(this.SERVER_URL + "/order/bandid/" + bandId + "/offertypeid/" + offerTpeId + "/userid/" + userId,JSON.stringify({}), {headers:headers}).toPromise();
  }

  GetPrinters(){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/printer", {headers:headers}).toPromise();
  }

  Getreports(){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL + "/reports", {headers:headers}).toPromise();
  }

}
