import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  private SERVER_URL = "https://localhost:5001/store";

  constructor(private httpClient: HttpClient) { }

  GetStores(){

    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');

    return this.httpClient.get(this.SERVER_URL, {headers:headers}).toPromise();
  }

  GetStoresByBand(id){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    const url = `${this.SERVER_URL}/band/${id}`;
    return this.httpClient.get(url, {headers:headers}).toPromise();
  }


  GetStoresByBandAndIsland(id, islandId){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    const url = `${this.SERVER_URL}/band/${id}/island/${islandId}`;
    return this.httpClient.get(url, {headers:headers}).toPromise();
  }

  GetIslandsByBand(id){
    let headers = new HttpHeaders();
    headers.set('Access-Control-Allow-Origins', '*');
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Accept', 'application/json; charset=utf-8');
    const url = `https://localhost:5001/island/band/${id}`;
    return this.httpClient.get(url, {headers:headers}).toPromise();
  }

}
