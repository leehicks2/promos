import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DesignerService {

  // Designer dimentions and position
  public currentTemplateHeight: string = "300px";
  public currentTemplateWidth: string = "200px";
  public currentTemplateLeft: string = "";
  public currentTemplateRight: string = "";

  // Style options position
  public inputOptionsRightPosition: string;
  public inputOptionsLeftPosition: string;

  constructor() { }
}
