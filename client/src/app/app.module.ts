import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from "angular-datatables";
import {DragDropModule} from '@angular/cdk/drag-drop';
import { ColorPickerModule } from 'ngx-color-picker';
import {MatSelectModule, MatInputModule } from '@angular/material';
import {AngularFittextModule} from 'angular-fittext';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TemplatingComponent } from './templating/templating.component';
import { ResizableModule } from 'angular-resizable-element';
import { UploadComponent } from './upload/upload.component';
import { TemplatepackComponent } from './templatepack/templatepack.component';
import { TutorialsComponent } from './tutorials/tutorials.component';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    DashboardComponent,
    TemplatingComponent,
    UploadComponent,
    TemplatepackComponent,
    TutorialsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    DataTablesModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    DragDropModule,
    ResizableModule,
    ColorPickerModule,
    MatSelectModule,
    MatInputModule,
    AngularFittextModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
