export class OrderDto{
    orderId : number;
    orderNumber : string;
    orderDate : string;
    storeId : number;
    storeName : string;
    templateName : string;
    amount : number;
    islandName : string;
    promProdCode : string;
    promDescription : string;
}
