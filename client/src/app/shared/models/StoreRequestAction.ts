export class StoreRequestAction{
    public  storeRequestsId: number;
    public  buyersOfferID: number;
    public  statusId : number;
    public  promProdCode : string;
    public  promDescription : string;
    public  templateName : string;
    public  templateId : number;
    public  storeID : number;
    public  amount : number;
    public  userName : string;
    public  storeName : string;
    public  sizeDescription : string;
    public bandLetter: string;
    public bandId: number;
    public templateMapId: number;
}
