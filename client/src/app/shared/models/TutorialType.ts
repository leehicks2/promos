export class TutorialType{
  public tutorialId: number;
  public tutorialName: string;
  public folder: string;
  public active: boolean = false;
  public parentId: number;
}
