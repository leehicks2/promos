export class GridDisplay{
  public height:string;
  public width:string;
  public top: string;
  public left:string;
  public visible: boolean = false;
}
