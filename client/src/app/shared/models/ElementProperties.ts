export class TemplateProperties{
  public id:number;
  public templateId:number;
  public width:string;
  public height:string;
  public topProperty:string;
  public leftProperty:string;
  public fontSize: string;
  public color:string;
  public spacing: string;
  public fontWeight: string;
  public active:boolean;
  public textProperty: string;
  public fieldId: number;
  public fieldName: string;
  public fontstyle: string;
  public strikethrough: number;
  public textAlign:string;
  public displayText:string;
  public rotate: number;
  public dateFormat: number;
  public textBefore: string;
  public textAfter: string;
  public verticalAlign: string;
  public zindex: number;
  public description: string;
}
