import { StoreRequestAction } from "./StoreRequestAction";

export class StoreRequestActionDisplayProduct{
  public product:string;
  public storeRequestAction: StoreRequestAction[] = [];
}
