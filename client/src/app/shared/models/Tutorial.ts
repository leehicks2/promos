export class Tutorial{
  public tutorialId: number;
  public tutorialName: string;
  public folder: string;
  public active: boolean = false;
}
