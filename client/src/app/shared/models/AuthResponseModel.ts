import { User } from "./User";

export class AuthResponseModel{
  public authed:boolean;
  public user:User;
}
