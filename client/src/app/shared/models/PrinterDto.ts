export class PrinterDto {
  id: number;
  name: string;
  location: string;
}
