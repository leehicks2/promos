import { Template } from "@angular/compiler/src/render3/r3_ast";
import { PosTemplate } from "./PosTemplate";
import { Promo } from "./PromoModel";
import { StoreRequest } from "./StoreRequest";
import { StoreRequestTemplates } from "./StoreRequestTemplates";

export class StoreRequestDisplay {

  public stroreRequest:StoreRequest;
  public buyerOffer: Promo;
  public template: StoreRequestTemplates[] = [];
}
