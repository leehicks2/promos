import { TemplateProperties } from "./ElementProperties";

export class PosTemplate{
  id:number;
  backgroundImage:string;
  templateName: string;
  bandId: number;
  templateType: number;
  width: string;
  height: string;
  displayDescription: string;
  templateProperties: TemplateProperties[];
  sizeDescription: string;
}
