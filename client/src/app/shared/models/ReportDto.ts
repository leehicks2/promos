export class ReportDto {
  id: number;
  name: string;
  location: string;
}
