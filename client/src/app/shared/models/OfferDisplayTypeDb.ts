export class OfferDisplayTypeDB{
  public id: number;
  public displayTypeId: number;
  public offerTypeId: number;
  public amount: number;
}
