import { OfferDisplayType } from "./OfferDisplayType";
import { Promo } from "./PromoModel";
import { StoreOfferStatus } from "./StoreOfferStatus";

export class StoreOffer{
  public storeOffersId:number;
  public storeName: string;
  public storeId: number;
  public Promo: Promo = new Promo();
  public amount: number;
  public Staus: StoreOfferStatus;
  public offerDisplayType: OfferDisplayType[] = [new OfferDisplayType()];
}
