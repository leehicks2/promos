
export class StoreRequestTemplates {
  public id:number;
  public templateId:number = 0;
  public amount:number = 1;
  public storeRequestId:number;
  public templateName:string;
}
