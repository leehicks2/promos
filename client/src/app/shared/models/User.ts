import { Band } from "./BandModel";
import { DepartmentModel } from "./DepartmentModel";

export class User {
    public userid: number;

    public UserName: string;

    public DateAdded: Date;

    public UserPassword: string

    public accessLevelID: string;

    public storeId: number;

    public UserEmail: string;
}
