import { StoreRequestAction } from "./StoreRequestAction";
import { StoreRequestActionDisplayProduct } from "./StoreRequestActionDisplayProduct";

export class StoreRequestActionDisplay{
  public store:string;
  public storeRequestAction: StoreRequestAction[] = [];
  public storeRequestActionDisplayListProduct: StoreRequestActionDisplayProduct[] = [];
}
