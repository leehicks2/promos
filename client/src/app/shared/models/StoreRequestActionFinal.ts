import { StoreRequestAction } from "./StoreRequestAction";
import { StoreRequestActionDisplayProduct } from "./StoreRequestActionDisplayProduct";

export class StoreRequestActionFinal{
  public store:string;
  public productDisplay: StoreRequestActionDisplayProduct[] = [];
}
