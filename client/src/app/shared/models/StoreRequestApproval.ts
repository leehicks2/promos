export class StoreRequestApproval {
  public storeRequestid: number;
  public  buyersOfferID : number;
  public  productCode : string;
  public  description : string;
  public  userName : number;
  public  createdDate : number;
  public  size : number;
  public  quantity : number;
  public  orientation : string;
}
