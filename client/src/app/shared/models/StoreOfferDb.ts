export class StoreOfferDb{
  public storeOffersId:number;
  public buyersOfferId:number;
  public storeID:number;
  public storeName:string;
  public templateId:number;
  public amount: number;
  public statusId: number;
}
