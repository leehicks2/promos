
export class Promo{

  public buyersOfferID: number;

    public  buyerUserID : number;

    public  createdDate: Date;

    public  promRecordId:string;

    public promProdCode:string;

    public size:string;

    public promIsland: string;

    public prBand: string;

    public promCode: string;

    public promDescription: string;

    public promSupp: string;

    public promStartRetDate: string;

    public promEndRetDate: string;

    public promStartCostDate: string;

    public promEndCostDate: string;

    public currentCost: string;

    public promCost: string;

    public promMessage: string;

    public currentRetail: string;

    public promRetail: string;

    public suppProdCode: string;

    public hostedInd: string;

    public promDeptDesc: string;

    public proddescrip: string;

    public slogan: string;
    public savings: string;
    public deptDesc: string;
    public multibuyPrice: string;
    public multibuyQty: string;
    public genericDescription: string;
    public inCart: boolean = false;
    public bandName: string;
}
