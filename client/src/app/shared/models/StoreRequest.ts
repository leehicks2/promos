import { StoreRequestTemplates } from "./StoreRequestTemplates";

export class StoreRequest {
  public storeRequestid: number;
  public  buyersOfferID : number;
  public  userId : number;
  public  createdDate : number;
  public  sizeID : number;
  public  quantity : number;
   public  templateID : number;
   public  statusId : number;
   public  orientation : string;
   public storeRequestTemplates: StoreRequestTemplates[] = [];
   public storeId:number = 0;
}
