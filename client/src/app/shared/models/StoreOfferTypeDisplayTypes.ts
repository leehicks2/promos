export class StoreOfferTypeDisplayTypes{
  public id:number;
  public storeID:number;
  public storeName: string;
  public offerTypeDisplayTypeId:number;
  public amount: number;
  public userId: number;
  public templateName: string;
  public Active: boolean = false;
}
