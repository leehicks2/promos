export class Band {
  id: number;
  name: string;
  bandLetter: string;
}
