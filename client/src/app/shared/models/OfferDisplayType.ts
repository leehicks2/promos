export class OfferDisplayType{
  public id: number;
  public amount: number;
  public description: string;
  public templateId: string;
}
