import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TemplatingComponent } from './templating/templating.component';
import { AuthGuardService } from './service/auth-guard.service';
import { TemplatepackComponent } from './templatepack/templatepack.component';
import { TutorialsComponent } from './tutorials/tutorials.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: 'templates', component: TemplatingComponent, canActivate: [AuthGuardService] },
  { path: 'templatepacks', component: TemplatepackComponent, canActivate: [AuthGuardService] },
  { path: 'tutorials', component: TutorialsComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
