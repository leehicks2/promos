import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AccessService } from './service/access.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'promos';

  dashboard:boolean = false;
  homeView:boolean = true;
  templateView: boolean = false;
  templatePackView: boolean = false;
  guideView: boolean = false;
  isAdmin = false;

  constructor(private _router: Router, private accessService: AccessService){
    this.checkIsAdmin();
  }

  checkIsAdmin(){
      this.isAdmin = this.accessService.isAdmin();
   }

  setDashboardView(view:string){
    // alert(this.isAdmin)
    if(!this.isAdmin){
      return;
    }


    if(view == "dashboard"){
      this.guideView = false;
        this.homeView = false;
        this.templateView = false;
        this.templatePackView = false;
        this.dashboard = true;
    }

    if(view == "home"){
      this.guideView = false;
        this.dashboard = false;
        this.templateView = false;
        this.templatePackView = false;
        this.homeView = true;
    }

    if(view == "template"){
      this.guideView = false;
        this.homeView = false;
        this.templateView = false;
        this.templatePackView = false;
        this.templateView = true;
    }

    if(view == "templatepack"){
      this.guideView = false;
      this.homeView = false;
      this.dashboard = false;
      this.templateView = false;
      this.templatePackView = true;
  }

  if(view == "guides"){
    this.homeView = false;
    this.templateView = false;
    this.templatePackView = false;
    this.dashboard = false;
    this.guideView = true;
}
  }

  logOut(){
    localStorage.setItem('authed','false');
    this._router.navigate(['/login']);
  }
}
