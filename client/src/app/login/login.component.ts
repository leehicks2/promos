import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { AuthResponseModel } from '../shared/models/AuthResponseModel';
import { LoginModel } from '../shared/models/LoginModel';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public _loginModel: LoginModel;
  public isError: boolean;

  constructor(private _loginService: LoginService, private _router: Router) {
    this._loginModel = new LoginModel();
   }

  login(){
    this._loginService.login(this._loginModel).then((data: AuthResponseModel)=>{
      if(data.authed){
        this.isError = false;
        localStorage.setItem('User', JSON.stringify(data.user));
        localStorage.setItem('authed', JSON.stringify(data.authed));
        this._router.navigate(['/home']);
      }else{
        this.isError = true;
      }

		}).catch((error: any[])=>{
			console.log(error);
      this.isError = true;
		});
  }

  ngOnInit() {
  }

}
